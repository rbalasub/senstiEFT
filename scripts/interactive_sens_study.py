#!/bin/env python
from code import interact
import os
import json
from socketserver import ForkingMixIn
import matplotlib.pyplot as plt
from senstiEFT.matrix import load_matrix, load_covariance
from os.path import exists
import matplotlib
import numpy as np
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.colors as mcolors
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from senstiEFT.matrix import matrix
from senstiEFT.helpers import get_number_of_digits
import subprocess


####Dealing with inputs and logging
def ask_for_input(list_of_answers, inputstring, helpstring):
    while True:
        input_asked = input(inputstring)
        if input_asked == "help" or input_asked == "h":
            print(help + helpstring)
            print(help + "'help' or 'h' will give you additional information on how to give inputs to the code, you cannot use them to name stuff!")
        elif input_asked in list_of_answers or "any" in list_of_answers:
            return input_asked
        else:
            print(help + "Please choose among", ", ".join(list_of_answers), "or 'help' for more information.")

##### colors            
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    OKCYAN = '\033[96m'
warning = bcolors.WARNING + "(WARNING) " +bcolors.ENDC
info = bcolors.OKBLUE + "(INFO) " +bcolors.ENDC
inp = bcolors.OKGREEN + "(INPUT REQUIRED) " + bcolors.ENDC
error = bcolors.FAIL + "(ERROR) " + bcolors.ENDC
help = bcolors.OKCYAN + "(HELP) " + bcolors.ENDC



################# functions for the sensitivity study
def title():
    print("\n******************************************************************************************************************************")
    print("**************************************** INTERACTIVE EFT SENSITIVITY STUDY ***************************************************")
    print("******************************************************************************************************************************\n")
    print("\nCheck the README to understand the various steps of a sensitivity study!\n")
    print("\nType help at any step to get more information on the choice to make!\n")
#### initialise dictionaries 
def initialise():

    ### define the config with names and useful info to reproduce
    config = { 
            "param": args.parmat, 
            "meas":args.meas, 
            "info": f"data/information_matrix/info_{args.tag}.yaml", 
            "rotation":f"data/rotation/rotation_{args.tag}.yaml",
            "ws_name":f"run/ws_{args.tag}.root",
            "grouping_string":None, "basis":None, "mvg_ws": None, "fit_results": None
            }
    ### define step names for logging 
    step = {
        "info": bcolors.OKBLUE +  "CREATING INFO MATRIX" + bcolors.ENDC,
        "grouping": bcolors.OKBLUE + "GROUPING" + bcolors.ENDC,
        "est_cov": bcolors.OKBLUE + "ESTIMATING COVARIANCE" + bcolors.ENDC,
        "mvg": bcolors.OKBLUE + "MVG WORKSPACE CREATION AND FITTING" + bcolors.ENDC
    }
    commands=[]
    return config,step,commands



#### Create the information matrix

def information_matrix(config, step, commands):
    print("\n----------------- CREATING INFO MATRIX -------------------\n")
    # check if infomat exists
    if exists(config["info"]):
        print(warning + "Name for infomat already exists, overwriting ... ")
    command_string = f"python scripts/prepare_fisher_info.py --input Higgs {config['param']} {config['meas']} --output {config['info']} "

    # create the info and exit if fails
    try:
        subprocess.run(command_string + "> /dev/null 2>&1", shell=True, check=True)
    except subprocess.CalledProcessError as e:
        print(error+ "Infomat not updated, exiting to avoid bugs ... ")
        exit()

    # save the command
    commands.append(command_string)

    # load the infomat
    infomat = load_matrix(config['info'])
    print(step +":" +info + "Saved information matrix in: " + config['info'])
    return config, infomat, commands



#### Create the grouping of EFT operators and find the most sensitive directions

def grouping(config,infomat,step,commands):
    print("\n----------------- GROUPING EFT PARAMETERS -----------------\n")
    close = ["y","n","Y","N","no","yes","No","Yes"]

    ## ask for grouping string
    print(info + "Starting the sensitivity study! Please add one at a time the grouping or use a complete string and follow the instructions to create the fit basis!")
    print(step +":" +info + f"Parameters to use for the fit basis: {infomat.xpars}")
    infomatname, rotation_name = config["info"],config["rotation"]

    ## looking at infomat might be useful to define the grouping
    if args.plotinfomat:
        print(step +":" +info + "Printing infomat in {0}".format(config["info"].replace("yaml","pdf")))
        os.system("python scripts/plotmatrix.py -i {0} -o {1} --fontsize {2} --normalise ".format(infomatname, infomatname.replace("yaml","pdf"),str(args.fontsize) ))

    # strings to control the while loops
    somestring="continue"
    total_string="continue"
    group_string_total=""

    # ask for grouping
    if args.display:
        os.system("display  {0}".format(infomatname).replace("yaml","pdf"))
    while (somestring != "stop"):
        print(step +":" +info + "Example grouping for full ev basis: --group name=ev {0}".format(" ".join(infomat.xpars)))
        print(step +":" +info + "Previous grouping: " + group_string_total)
        print(step +":" +info + "Type reset to insert directly the full string with all groups, or type a name of group to then include one by one the EFT operators.\n")
        group_string_total=""
        groupname=ask_for_input(["any"],inp+ "Type group name or reset! \n","Type a name for a set of parameters to group or type 'reset' to you restart from a string you can decide, for example if you already have a full grouping in mind. ")
        if groupname == "reset":
                poi_par=ask_for_input(["any"],inp + "Please insert the string! \n","The string should be a grouping like --group name=first_group name par1 par2 --group name=second_group par3 par4...!")
                group_string=" " + poi_par
        else:   
            grouplist= ask_for_input(["any"],inp + "Insert the list of parameters to include under the group {0} \n".format(groupname),"Insert a string like par1 par2 par3 par4 ..., a PCA will be performed in this subgroup to find the most sensitive directions.")
            group_string="--group name={0} {1}".format(groupname,grouplist)

        group_string_total= group_string_total + group_string

        newgroup=ask_for_input(close, inp + "Do you want to add a new group? (y/n) \n","Add a new group with the missing coefficients if any or you can also reset the grouping from here if you made a mistake")
        if ((newgroup == "y") or (newgroup == "yes") or (newgroup == "Yes")):   
            somestring="continue"
        elif (newgroup == "n"):   
            somestring= "stop"
    command_sens_dir= "python scripts/get_sensitive_directions.py --input {0} {1} --output {2} ".format(config["info"], group_string_total, rotation_name)
    os.system(command_sens_dir)
    commands.append(command_sens_dir)
        
    ### save the grouping in the config for reproducibility and save the config
    config["grouping_string"]=group_string_total
    print(step +":" + info + "Saving config with param, measurements, infomat, grouping")
    with open("config_{0}.json".format(args.tag),"w") as configf:
            json.dump(config,configf,indent=5)
        
    return config,commands


#### Create the fit basis and estimate the covariance of the parameters, use threshold at 10 to decide which parameters to include in the fit basis

def estimate_covariance(config,step,commands):  
        print("\n----------------- ESTIMATING COVARIANCE -----------------\n")
        close = ["y","n","Y","N","no","yes","No","Yes"] 
        
        infomatname, rotation_name = config["info"],config["rotation"]
        somestring="continue"
        cov_list=""
        while (somestring != "stop"):
            print(step +":" +info + "Estimating covariance of pois in fit basis and checking correlation matrix")
            print(step +":" +info + "Previous pois used for covariance matrix: " + cov_list)
            cov_par=ask_for_input(["any"],inp + "Add poi(s) or type reset! \n","Add pois to the list of the ones whose expected uncertainty will be estimated [no comma, no '', insert something like ev_01 ev_02 ev_03 or type 'reset' to restart and use a complete string.")
            if cov_par == "reset":
                cov_par=ask_for_input(["any"],inp + "Please insert the string! \n", " The string should be like ev_01 ev_02 ...ev_N)")
                cov_list=cov_par
            else:
                cov_list=cov_list + f" {cov_par}"
            command_est_cov= "python scripts/estimate_covariance.py --info-matrix {0}  --transformation-matrix  {1} --pois {2}".format(infomatname,rotation_name,cov_list)
            os.system(command_est_cov)
            commands.append(command_est_cov)

            savecorr=ask_for_input(close, inp + "Do you want to save and look at the correlation now? \n","If you say yes it will produce a pdf with the expected correlation matrix of the chosen basis.")    
            if ((savecorr == "y") or (savecorr == "yes") or (savecorr == "Yes")):
                os.system("python scripts/estimate_covariance.py --info-matrix {0}  --transformation-matrix  {1} --pois {2}  --save-correlation run/correlation_{3}.pdf".format(infomatname,rotation_name,cov_list,args.tag))

            finish_sens=ask_for_input(close, inp + "Do you want to fix this basis? (y/n)\n", "if not, you can add more parameters. Otherwise, you can continue with this basis or restart the grouping.")    
            if ((finish_sens == "y") or (finish_sens == "yes") or (finish_sens == "Yes")):
                somestring="stop"
                if cov_list[-1]==" ":
                    fit_basis= cov_list
                else:
                    fit_basis=cov_list + " "
            else:
                somestring="continue"

        print(step +":" +info + "Rotation matrix in: " + rotation_name)
        print(step +":" +info + "Correlation matrix of the fit basis in: " + f"run/correlation_{args.tag}.pdf")
        print(step +":" +info + "The fit basis: " + fit_basis)
            
        config["basis"]=fit_basis.split(" ")[1:-1]
        print(step +":" +info + "Saving config with param, measurements, infomat, grouping and fit basis")
        with open("config_{0}.json".format(args.tag),"w") as configf:
                json.dump(config,configf,indent=5)

        return config,commands, fit_basis


### MVG workspace creation and fitting (do not use with higgs param, if ratio of param. Parmat is a linearisation of that)

def mvg(config,step,fit_basis,commands):  
    print("\n----------------- MVG WORKSPACE CREATION AND FITTING -----------------\n")
    close = ["y","n","Y","N","no","yes","No","Yes"]
    infomatname, rotation_name=config["info"],config["rotation"]

    ### create the EFT ws in the final fit basis using the parmat, the measurements and the rotation matrix
    mvg=ask_for_input(close, inp + "Do you want to create MVG workspace? \n", "It creates a simplified model based on a multivariate gaussian likelihood (MVG). Read more in the README.md")
    if ((mvg == "y") or (mvg == "yes") or (mvg == "Yes")):
        command_mvg="python scripts/prepare_mvg_workspace.py --rotation {0} --input Higgs {1} {2}  --output {3}".format(rotation_name,args.parmat,args.meas,config["ws_name"])
        commands.append(command_mvg)
        os.system(command_mvg)

        ## fit the pois of the chosen fit basis and print output json
        fit=ask_for_input(close,  "Do you want to fit the mvg ws (Setup RooFitUtils!)?", "Fit the pois of the chosen fit basis and print output json")
        if ((fit == "y") or (fit == "yes") or (fit == "Yes")):
            command_fit="fit.py --input {0} --poi {1} --output {2}".format(config["ws_name"],fit_basis,"run/fit_results_{0}.json".format(args.tag))
            commands.append(command_fit)
            os.system(command_fit)
            ### saving config for the last time
            config["fit_results"]="run/fit_results_{0}.json".format(args.tag)
        else: 
            print(info + "Skipping fit!")
    else:
        print(step +":" +info + "Skipping creationg of the ws!")


    print(step +":" +info + "Saving config with param, measurements, infomat, grouping, fit basis and mvg results")
    with open(f"config_{args.tag}.json","w") as configf:
                json.dump(config,configf,indent=5)

    return config,commands






################# main  

def main():

    title()

    ## decide between interactive and config
    ### config here
    if args.config:
        print("\n------------------- CONFIG OPENING: REPRODUCING PREVIOUS RUN  -------------------------\n")
        run_config = json.load(open(args.config))
        
        ### infomat and grouping should be always in the config
        command_info = f"python scripts/prepare_fisher_info.py --input Higgs {run_config['param']} {run_config['meas']} --output {run_config['info']}"
        command_sens_dir= "python scripts/get_sensitive_directions.py --input {0} {1} --output {2} ".format(run_config["info"], run_config["grouping_string"], run_config["rotation"])

        ### run estimate cov and create mvg ws only if basis is in config
        if run_config["basis"] is not None: 
            command_est_cov= "python scripts/estimate_covariance.py --info-matrix {0}  --transformation-matrix  {1} --pois {2}".format(run_config["info"],run_config["rotation"], " ".join(run_config["basis"]))
            command_mvg="python scripts/prepare_mvg_workspace.py --rotation {0} --input Higgs {1} {2}  --output {3}".format(run_config["rotation"],run_config["param"],run_config["meas"],run_config["ws_name"])

        if run_config["fit_results"] is not None: 
            command_fit = "fit.py --input {0} --poi {1} --output {2}".format(run_config["ws_name"]," ".join(run_config["basis"]),run_config["fit_results"])

        print("Reproduce sensitivity study with: ")
        print("\n" +  command_info)
        print("\n" +  command_sens_dir)
        if run_config["basis"] is not None: 
            print("\n" +  command_est_cov)
            print("\n" +  command_mvg)
        if run_config["fit_results"] is not None: 
            print("\n" +  command_fit)
        print("*******************************************************************************************************************************")


    ### run interactive here
    else:
        config,step,commands = initialise()
        ### run the sensitivity study
        config,infomat,commands = information_matrix(config,step["info"],commands)
        config,commands = grouping(config,infomat,step["grouping"],commands)
        config,commands,fit_basis = estimate_covariance(config,step["est_cov"],commands)
        config,commands = mvg(config,step["mvg"],fit_basis,commands)
        
        ## save config
        with open("config_{0}.json".format(args.tag),"w") as configf:
            json.dump(config,configf,indent=5)
        print(info + f"Saving config with param, measurements, infomat, grouping and fit basis in config_{args.tag}.json")
        ## print commands
        if args.print:
            print("-------------------------- COMMANDS USED ---------------------------")
            print(info + "Printing commands...")
            [print(i+"\n") for i in commands]

        print("*******************************************************************************************************************************")


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser("Interactive python script to do a sensitivity study")
    parser.add_argument("-p","--parmat",type=str,dest="parmat",help="parmat file")
    parser.add_argument("-m","--meas",type=str,dest="meas",help="exp yaml with cov matrix")
    parser.add_argument("--tag",type=str,dest="tag",help="tag for output files", default="created_by_script")
    parser.add_argument("--fontsize",type=str,dest="fontsize",help="fontsize for priting infomat [default 5]", default="5")
    parser.add_argument("--display",help="Open display to visualize matrixes",action="store_true")
    parser.add_argument("--plot-infomat",dest="plotinfomat",help="to avoid plotting the infomat at the beginning of the sensitivity study",action="store_true")
    parser.add_argument("--config",type=str,help="Use a config instead of the interactive version")
    parser.add_argument("--no-print-commands",dest="print",help="to avoid printing the commands used",action="store_false")
    args = parser.parse_args()

main()