#!/bin/env python

from senstiEFT.matrix import load_matrix
from senstiEFT.matrix_helpers import add_matrices
from senstiEFT.senstivity_helpers import get_subgroup_evs
from senstiEFT.helpers import get_matrix_in_newbasis


def get_sensitive_directions(args):
    infomats = [load_matrix(infofile) for infofile in args.infofiles[0]]

    if len(infomats) >= 2:
        total_info = add_matrices(infomats[0], infomats[1], sum_name="sum_infos")
        for infomat in infomats[2:]:
            total_info = add_matrices(total_info, infomat, sum_name="sum_infos")

    else:
        total_info = infomats[0]

    allpars = total_info.xpars
    pargroups = {}

    order = []
    for g in args.pargroups:
        found_name, group = False, []
        for par in g:
            if "name=" in par:
                found_name = True
                gname = par.split("=")[-1]
            else:
                group.append(par)
                order.append(par)
        if not found_name:
            if len(group) == 1:
                gname = par
            else:
                gname = "_".join(g)
        pargroups[gname] = group
            
    if not len(pargroups):
        pargroups = {"ev": allpars}
    ev_transmat, eg_vals = get_subgroup_evs(args.groupname, pargroups, allpars, total_info)
    
    additional_info = {}
    if len(order):
       additional_info = {"order": {"xpars":order}}

    additional_info["eigen values"] = {ypar:float(eval) for eval,ypar in zip(eg_vals,ev_transmat.ypars)}
    
    ev_transmat.save_matrix(args.out,additional_info=additional_info)
    
    if args.save_info != "":
        total_info.reduce_matrix(xpars=additional_info["order"]["xpars"],ypars=additional_info["order"]["xpars"])
        total_info.save_matrix(args.save_info,additional_info=additional_info)
        rotated_info = get_matrix_in_newbasis(total_info, ev_transmat)
        rotated_info.save_matrix(args.save_info.replace(".json","_rotated.json").replace(".yaml","_rotated.yaml"))


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(
        "perform EFT sensitivity study given information matrices & parameter groupings"
    )
    parser.add_argument(
        "-i",
        "--input",
        "--inputs",
        nargs="+",
        action="append",
        type=str,
        dest="infofiles",
        help="input json files with information matrix",
        required=True,
        metavar="path/to/file",
    )
    parser.add_argument(
        "--group-name",
        type=str,
        dest="groupname",
        help="title for parameter groups",
        default="group",
    )
    parser.add_argument(
        "--group",
        nargs="+",
        action="append",
        type=str,
        dest="pargroups",
        help="list of parameter groups",
        default=[],
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="out",
        help="save transformation matrix",
        required=False,
        default="out.json",
    )
    parser.add_argument(
        "--save-info",
        type=str,
        dest="save_info",
        help="save transformation matrix",
        required=False,
        default="",
    )

    args = parser.parse_args()
    get_sensitive_directions(args)
