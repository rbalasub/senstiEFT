#!/bin/env python
import numpy as np
from senstiEFT.param_helpers import prepare_expr
from senstiEFT.matrix import load_matrix, load_parameterisation, matrix

import sys

def get_parameters_from_terms(list_of_terms):
    # find all the parameters in the polynomial, (look separately in linear & quad. terms)
    # [covers cases where there are parameters which appears in quadratic but not linear]
    xpars_linear = [x for x in list_of_terms if "*" not in x]

    # check if no quad terms exists -- np.concatenate works only for non-empty lists

    if len([xpar for xpar in list_of_terms if "*" in xpar]) == 0:
        xpars_term = sorted(list(set(xpars_linear)))
        return xpars_term

    xpars_linear_from_quad = list(
        set(
            np.concatenate(
                [xpar.split("*") for xpar in list_of_terms if "*" in xpar],
            ).flat
        )
    )
    xpars_term = sorted(list(set(xpars_linear + xpars_linear_from_quad)))

    return xpars_term


def make_xml_param(f, mat, is_rotation, threshold=1e-6, exclude_cross_terms=False):
    for ypar in mat.ypars:
        f.write("\t<Item Name=\"{0}\"/>\n".format(prepare_expr(mat, ypar, is_rotation, threshold, exclude_cross_terms=exclude_cross_terms))) 


def prepare_reparam_xml(args):
    if len(args.param):
        print("loading parameterisation {0}".format(args.param))
        mat = load_parameterisation(args.param)
        print(mat.matrix)
    elif len(args.rotation):
        print("loading rotation {0}".format(args.rotation))
        rotation = load_matrix(args.rotation)
        if args.not_transpose:
            mat = matrix(rotation.name, rotation.matrix,xpars=rotation.xpars, ypars=rotation.ypars)
        else:
            mat = matrix(rotation.name, np.transpose(rotation.matrix),xpars=rotation.ypars, ypars=rotation.xpars)
    else:
        print("No inputs provided")
        sys.exit()

    excludepois = args.excludepois

    if len(args.excludepois):
        for par in excludepois:
            if par not in mat.pars:
                print(
                    "Parameter {0} not found in the matrix axes, needs to be fixed.".format(
                        par
                    )
                )
                exit()
        reduce_ypars = [par for par in mat.ypars if par not in excludepois]
        reduce_xpars = [par for par in mat.xpars if par not in excludepois]
        mat.reduce_matrix(reduce_xpars, reduce_ypars)

    xpars, ypars = mat.xpars, mat.ypars
    xpars_term = get_parameters_from_terms(xpars)
    xpars_term_wo_SM = [x for x in xpars_term if x != "SM"]

    with open(args.output, "w") as fp:
        header_inputs = [
            ("InFile", args.infile),
            ("OutFile", args.outfile),
            ("ModelName", args.model),
            ("POINames", ",".join(xpars_term_wo_SM)),
            ("WorkspaceName", args.ws),
            ("DataName", args.data),
        ]
        header_input = "\n\t".join(
            [
                part
                for part in [
                    "=".join([inp[0], '"' + inp[1] + '"']) for inp in header_inputs
                ]
            ]
        )

        fp.write("<!DOCTYPE Organization  SYSTEM 'Organization.dtd'>\n")
        fp.write(" <Organization {0}>\n\n".format(header_input))
        for par in xpars_term:
            inival = 0
            if par == "SM":
                inival = 1
            fp.write('\t<Item Name = "{0}[{1}]"/>\n'.format(par, inival))
        fp.write("\n")
        is_rotation = False
        if len(args.rotation):
            is_rotation = True
        make_xml_param(fp, mat, is_rotation, args.threshold, exclude_cross_terms=args.exclude_cross_terms)
        fp.write('\n\t<Map Name="EDIT::NEWPDF(OLDPDF,\n')
        for ini in ypars:
            fp.write("\t\t{0}={0}_new,\n".format(ini, ini))
        fp.write(')"/>\n')
        fp.write("</Organization>")
    print("Wrote output:{0}".format(args.output))


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser("make xml for workspaceCombiner")

    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="output",
        help="save transformation matrix",
        default="out.xml",
    )
    parser.add_argument(
        "--exclude-cross-terms",
        action="store_true",
        dest="exclude_cross_terms",
        help="exclude cross terms",
        default=False,
    )

    parser.add_argument(
        "--in-file",
        type=str,
        dest="infile",
        help="input file",
        required=True,
        default="out.root",
    )
    parser.add_argument(
        "--out-file",
        type=str,
        dest="outfile",
        help="output file",
        required=True,
        default="out.root",
    )
    parser.add_argument(
        "--exclude",
        type=str,
        dest="excludepois",
        help="POIs to exclude.",
        metavar="POI",
        nargs="+",
        default=[],
    )
    parser.add_argument(
        "--model",
        type=str,
        dest="model",
        help="save transformation matrix",
        default="Model",
    )
    parser.add_argument(
        "--data",
        type=str,
        dest="data",
        help="save transformation matrix",
        default="combData",
    )
    parser.add_argument(
        "--workspace",
        type=str,
        dest="ws",
        help="save transformation matrix",
        default="combWS",
    )
    parser.add_argument(
        "--parameterisation",
        type=str,
        dest="param",
        help="parameterisation",
        required=False,
        default="",
    )
    parser.add_argument(
        "--threshold",
        type=float,
        dest="threshold",
        help="threshold for the coefficients of the polynomial",
        required=False,
        default=1e-6,
    )
    parser.add_argument(
        "--rotation",
        dest="rotation",
        type=str,
        help="rotation",
        required=False,
        default="",
    )
    parser.add_argument(
        "--not-transpose",
        dest="not_transpose",
        type=bool,
        help="dont transpose amtrix if rotation",
        required=False,
        default=False,
    )
    args = parser.parse_args()
    prepare_reparam_xml(args)
