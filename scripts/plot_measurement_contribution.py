#!/bin/env python
from senstiEFT.matrix import matrix, load_matrix, listdiff, load_covariance
from senstiEFT.matrix_helpers import get_const_pars
from senstiEFT.senstivity_helpers import get_new_info, get_fractional_contribution
from senstiEFT.plotter import write_barplot
import numpy as np
import yaml
from senstiEFT.matrix import load_parameterisation

def check_and_remove_null_others(frac_contribution, group_cases):
    from copy import deepcopy
    frac_contribution_new = {}
    found_other = False
    for par in frac_contribution:
        if "others" in frac_contribution[par]:
            if frac_contribution[par]["others"] == 0.0:
                frac_contribution_new[par] = {case: frac_contribution[par][case] for case in frac_contribution[par] if case != "others"}
            else:
                found_other = True    
                
    if found_other:
        print("Found non-zero contribution to others, consider checking missing measurements")
        return frac_contribution, group_cases
    
    else:
        return frac_contribution_new, [case for case in group_cases if case != "others"]
                
def plot_measurement_contribution(args):
    with open(args.config) as f:
        mat_dict = yaml.safe_load(f)

    pars =[par for par in mat_dict["pars"] if "gap" not in par]
    if "transformation_matrix" in mat_dict.keys():
        print("Provided rotation matrix")
        transmat = load_matrix(mat_dict["transformation_matrix"])

    else:
        print("Not provided rotation matrix")
        n = len(pars)
        identity = np.zeros((n,n), int)
        np.fill_diagonal(identity, 1)
        transmat = matrix("rotation",identity, xpars=pars, ypars=pars)

    inputs = mat_dict["inputs"]
    group_cases = ["others"]

    for case in inputs:
        group_cases = group_cases + list(inputs[case]["group_meas"].keys())
        inputs[case]["covariance"] = load_covariance(inputs[case]["covariance_path"])
        inputs[case]["parameterisation"] = load_parameterisation(inputs[case]["parameterisation_path"])
    frac_contribution = {}
    for eftpar in pars: 
        newmat = get_new_info(inputs, [eftpar], transmat=transmat, debug=args.debug)
        if args.debug:
            newmat.save_matrix("newmat_{0}.json".format(eftpar))
        frac_contribution[eftpar] = get_fractional_contribution(
            newmat,
            pars,
            eftpar,
            group_cases,
            info_based=args.infobased,
            debug=args.debug,
        )

    
    frac_contribution, group_cases = check_and_remove_null_others(frac_contribution, group_cases)
    
    
    matarr = np.array(
        [[frac_contribution[par][case] for case in group_cases] for par in pars]
    )
    
    [print("The highest contribution to poi: {0} is {1:.3f} and it is coming from: {2}".format(par.ljust(10),max(frac_contribution[par].values()),max(frac_contribution[par],key=frac_contribution[par].get)).ljust(15)) for par in pars]
    cont_mat = matrix(
        "contribution_matrix",
        matarr,
        xpars=group_cases,
        ypars=pars,
        labels=transmat.labels,
    )
    if args.debug:
        cont_mat.save_matrix("contribution_matrix.json")
    zero_cont = get_const_pars(cont_mat, val=0.0, xpars=True, ypars=False)
    cont_mat.reduce_matrix(xpars=listdiff(group_cases, zero_cont), ypars=pars)
#    print(cont_mat.xpars)
#    print(cont_mat.ypars)
    padded_arr = []
    
    mat_dict["pars"].reverse()
    for par in mat_dict["pars"]:
        if par not in cont_mat.ypars:
            padded_arr.append([0.0 for _ in cont_mat.xpars])
        else:
            padded_arr.append([cont_mat.getElem(xpar,par) for xpar in cont_mat.xpars])
            
    for par in mat_dict["pars"]:
        if "gap" in par:
            cont_mat.labels[par] = ""
    
    padded_mat = matrix("padded contribution matrix", padded_arr, xpars=cont_mat.xpars, ypars=mat_dict["pars"], labels=cont_mat.labels)

    if args.save_contribution_matrix:
        transposed_mat = matrix("transposed contribution matrix", np.transpose(padded_mat.matrix), xpars=padded_mat.ypars, ypars=padded_mat.xpars, labels=cont_mat.labels)
        transposed_mat.save_matrix(args.save_contribution_matrix)
    if "colors" in mat_dict:
        print("Writing barplot with colors")
        write_barplot(args.output, padded_mat,mat_dict["colors"])
    else:
        write_barplot(args.output, padded_mat, rotate=False)
    print("Wrote contribution plot to {0}".format(args.output))


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser("estimate contribution of measurements to EFT directions")
    parser.add_argument(
        "--config",
        type=str,
        dest="config",
        help="config with reqd. inputs",
        required=True,
        default=None,
    )
    parser.add_argument(
        "--save-contribution-matrix",
        type=str,
        dest="save_contribution_matrix",
        help="save contribution matrix as a yaml file",
        required=False,
        default="",
    )

    parser.add_argument(
        "--output", type=str, dest="output", help="output texfile", default="out.tex"
    )
    parser.add_argument(
        "--info-based-metric",
        action="store_true",
        dest="infobased",
        help="alternate metric based on info.",
        default=False,
    )
    parser.add_argument(
        "--debug", action="store_true", default=False, help="write out things to debug"
    )
    args = parser.parse_args()
    plot_measurement_contribution(args)
