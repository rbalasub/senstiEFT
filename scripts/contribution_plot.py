#!/bin/env python
import json
import numpy as np

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser("Contribution plot")

    parser.add_argument(
        "-i", "--input", type=str, help=" matrix json file ", required=True
    )
    parser.add_argument(
        "-o", "--output", type=str, help=" plot name ", default="Contribution_plot.pdf"
    )
    parser.add_argument(
        "--title",
        type=str,
        help="Title of the plot [default= EFT contribution plot]",
        default="EFT contribution plot",
    )

    args = parser.parse_args()


title = args.title
out = args.output
f = open(args.input)
m = json.load(f)

print()
print("<Info>: This script creates a contribution plot using a contribution matrix")
print(m["info"])
print(
    "plotting parameters: "
    + str(m["xpars"])
    + " with these sets of measurements: "
    + str(m["ypars"])
)

par = m["xpars"]
names = m["ypars"]
matrix = m["matrix"]
import matplotlib.pyplot as plt

x = np.zeros(len(par))
width = np.zeros(len(par))
plt.figure(figsize=(5, 5))
plt.title(title, size="large", weight="bold")
plt.xlabel("Relative contribution", weight="bold")
for i in range(len(names)):
    width = [el[i] for el in matrix]
    plt.barh(np.array(par), width, left=x, label=names[i])
    x = x + width[:]
    plt.legend(
        loc="upper center",
        bbox_to_anchor=(0.5, -0.15),
        fancybox=True,
        shadow=True,
        ncol=3,
        prop={"size": 6},
    )

plt.subplots_adjust(left=0.3, bottom=0.2)
plt.savefig(out)
print("<Info>: Saving figure as ", out)
