#!/bin/env python
from senstiEFT.matrix import load_parameterisation, matrix
from senstiEFT.senstivity_helpers import make_infomat
from senstiEFT.matrix_helpers import add_list_of_matrices, get_padded_matrix
import numpy as np
import math 
import ROOT

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    MAG = "\033[1;35;40m"
warning=bcolors.WARNING + "(WARNING) " +bcolors.ENDC
info=bcolors.OKBLUE + "(INFO) " +bcolors.ENDC
inp=bcolors.OKGREEN + "(INPUT REQUIRED) " + bcolors.ENDC
error= bcolors.FAIL + "(ERROR) " + bcolors.ENDC
help=bcolors.OKCYAN + "(HELP) " + bcolors.ENDC


def compare_pars(pars1, pars2):
    for x in pars1:
        if x not in [y for y in pars2]:
           print("{0} is not common!".format(x))

def common_pars(pars1, pars2):
    return [ x for x in pars1 if x in pars2]

def compare_params(args):
    parmat1 = load_parameterisation(args.param1)
    parmat2 = load_parameterisation(args.param2)
    compare_pars(parmat1.ypars, parmat2.ypars)
    compare_pars(parmat1.xpars, parmat2.xpars)
    ypars = common_pars(parmat1.ypars, parmat2.ypars)
    xpars = common_pars(parmat1.xpars, parmat2.xpars)

    if args.savediff != "":
       diff = matrix("difference of {0}, {1}".format(parmat1.name, parmat2.name),[[ parmat1.getElem(x,y) - parmat2.getElem(x,y) for x in xpars] for y in ypars], xpars = xpars, ypars = ypars)
       parmat1.labels.update(parmat2.labels)
       diff.save_parameterisation("{0}".format(args.savediff),additional_info={"labels":parmat1.labels})

    for ypar in sorted(ypars):
        str0  = ypar.ljust(42)+" : "   
        to_print=False     
        if args.comparison:
            print("{0} : ".format(ypar))
            list1,list10,list100=[],[],[]
            for xpar in sorted(xpars):

                if ((parmat1.getElem(xpar, ypar)) and (parmat2.getElem(xpar, ypar))):
                    ratio = 100* abs(parmat1.getElem(xpar, ypar) - parmat2.getElem(xpar, ypar)) / abs(parmat1.getElem(xpar, ypar))
                else:
                    ratio=0
                if ((not parmat1.getElem(xpar, ypar)) and (parmat2.getElem(xpar, ypar))):
                    print( "    " + bcolors.FAIL + bcolors.UNDERLINE +"{0} does not have a contribution only in first parameterisation!".format(xpar) +bcolors.ENDC)
                if ((not parmat2.getElem(xpar, ypar)) and (parmat1.getElem(xpar, ypar))):
                    print( "    " + bcolors.FAIL + bcolors.UNDERLINE +"{0} does not have a contribution only in second parameterisation!".format(xpar) +bcolors.ENDC)
                if ratio > 1:
                    if ratio > 10:      
                        if ratio > 100:
                            list100.append(xpar)
                        else:
                            list10.append(xpar)
                    else:
                        list1.append(xpar)
                    to_print=True
            if to_print:
                if len(list1):
                    print(bcolors.WARNING + "    {0}  more than 1% off!".format(",".join(list1)) +bcolors.ENDC)
                if len(list10):
                    print(bcolors.FAIL + "    {0}  more than 10% off!".format(",".join(list10)) +bcolors.ENDC)
                if len(list100):
                    print(bcolors.MAG + "    {0}  more than 100% off!".format(",".join(list100)) +bcolors.ENDC)
            else:
                print(bcolors.OKGREEN +"    {0} has no sizeble difference".format(ypar)+bcolors.ENDC)
        else:    
#            print(bcolors.WARNING + "<INFO>" +bcolors.ENDC +  "You set a threshold at: {0}".format(args.threshold))

            for xpar in sorted(xpars):
                val = parmat1.getElem(xpar, ypar) - parmat2.getElem(xpar, ypar) 
                if val < 0 : sign = " - "
                else: sign = " + "
                if args.fractional and val != 0.0: 
                     val =100*val/ parmat2.getElem(xpar, ypar)
                if ((val != 0.0) and (abs(val) > args.threshold )): 
                    if args.fractional:
                        str0 = str0 + "{0} {1:.1f} {2}".format(sign, abs(val), xpar).ljust(15)
                    else:
                        str0 = str0 + "{0} {1:.3f} {2}".format(sign, abs(val), xpar).ljust(15)
                    to_print=True
            if to_print:
                print(str0)
            else:
                print(bcolors.OKGREEN +"{0} has no difference".format(str0)+bcolors.ENDC)

if __name__ == '__main__':   
    from argparse import ArgumentParser
    parser = ArgumentParser("compare difference in parameterisation")
    parser.add_argument( "--param1"    ,            type=str,  dest="param1", help="name of output workspace"     , required=True,default="")    
    parser.add_argument( "--param2"    ,            type=str,  dest="param2", help="name of output workspace"     , required=True,default="")    
    parser.add_argument( "--save-difference" ,      type=str,  dest="savediff"  , help="save file"                    , required=False,default="")    
    parser.add_argument( "--fractional",  action='store_true',  dest="fractional", help="% difference "     , required=False,default=False)    
    parser.add_argument( "--color-comparison",  action='store_true',  dest="comparison", required=False,default=False)    
    parser.add_argument( "--threshold",  type=float,  dest="threshold", required=False,default=0)    
    args = parser.parse_args()
    compare_params(args)
