from senstiEFT.matrix import load_parameterisation, matrix 
from senstiEFT.helpers import shrink_matrix, correlation_from_covariance
from RooFitUtils.pgfplotter import writematrix

def plotmatrix(mat, out, normalise, matlabels=[]):
#    mat = load_matrix(matpath)
    import math
#    matlabels=["$\\sqrt{s}=$13 TeV, 36.1-139 fb$^{\\scriptsize{-1}}$"]
    if normalise: mat.matrix = [[mat.matrix[k][j]/math.sqrt(mat.matrix[k][k]*mat.matrix[j][j]) for k in range(len(mat.matrix))] for j in range(len(mat.matrix))] 
    parnames = [x for x in mat.xpars]
    ev_names = [y for y in mat.ypars]
    ev_names.reverse()

    plotmat = [[mat.getElem(x,y) for x in parnames] for y in ev_names]
    
    writematrix("Internal",parnames,ev_names,
                plotmat,
                out,-1,1,45,plotlabels=matlabels)
    
def plot_parameterisation_matrix(parmat, out):
    isLinear, isQuad = False, False
    for term in parmat.xpars:
        if "*" in term:
            isQuad = True
        else:
            isLinear = True
            
    # plot quad term
    if isQuad:
        quad_terms = [x for x in parmat.xpars if "*" in x]
        quad_mat = shrink_matrix(parmat, quad_terms, parmat.ypars)
        print(quad_terms)
        coeff_in_terms = []
        for x in quad_terms:
            for par in x.split("*"):
                coeff_in_terms.append(par)
        
        coeff_in_terms = sorted(list(set(coeff_in_terms)))

        matrices = []        
        for par in parmat.ypars:
            mat_for_par = []
            for coeff1 in coeff_in_terms:
                tmp = []
                for coeff2 in coeff_in_terms:
                    if coeff_in_terms.index(coeff2) < coeff_in_terms.index(coeff1):
                        tmp.append(0)
                    else:
                        # check Caucy-Schwarz
                        inter = quad_mat.getElem(coeff1+"*"+coeff2,par)
                        sq1 = quad_mat.getElem(coeff1+"*"+coeff1,par)
                        sq2 = quad_mat.getElem(coeff2+"*"+coeff2,par)
                        
                        if inter > sq1 + sq2:
                            print("Caucy-Schwarz violated for", coeff1, coeff2, par)
                            
                        tmp.append(quad_mat.getElem(coeff1+"*"+coeff2,par))
                mat_for_par.append(tmp)    
        
            matpar = matrix("mat_"+par,mat_for_par,coeff_in_terms,coeff_in_terms)        
            matrices.append(matpar)

        for mat, par in zip(matrices, parmat.ypars):
            plotmatrix(mat, out.replace(".tex","_"+mat.name+".tex"), True, [par.replace("_"," ")])
            



        

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser("plot EFT parameterisation")
    parser.add_argument(
        "-i",
        "--input",
        type=str,
        dest="parmat_loc",
        help="input json file with parameterisation",
        required=True,
        metavar="path/to/file",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="out",
        help="path to save parameterisation plot",
        required=False,
        default="out.eps",
    )

    args = parser.parse_args()
    parmat = load_parameterisation(args.parmat_loc)
        
    plot_parameterisation_matrix(parmat,args.out)
