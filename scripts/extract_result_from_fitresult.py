#!/bin/env python
import ROOT
import yaml
from senstiEFT.helpers import getfitresult

def extract_fitresult(args):

    fitres = getfitresult(args.measurement_name,args.fitresult,args.fitresname,poi_expr=args.poiexpr,pois=args.pois,no_systematics=args.nosys)
    with open(args.outfile, "w") as f:
        yaml.dump(fitres, f, allow_unicode=True, default_flow_style=None, encoding="utf-8")

    print("wrote fitresult to {0}".format(args.outfile))

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(
        "extract fit results from a ROOT RooFitResult"
    )
    parser.add_argument(
        "-i",
        "--fitresult-file",
        type=str,
        dest="fitresult",
        help="input ROOT file containing fit result",
        required=True,
        metavar="path/to/file.root",
    )
    parser.add_argument(
        "--measurement-name",
        type=str,
        dest="measurement_name",
        help="name for the measurement in the yaml file",
        required=True,
        metavar="measurement name",
    )
    parser.add_argument(
        "--fitresult-name",
        type=str,
        dest="fitresname",
        help="Name of the fit result",
        required=False,
        default="fitResult",
    )
    parser.add_argument(
        "--outfile",
        type=str,
        dest="outfile",
        help="Output yaml file",
        required=False,
        default="out.yaml"
    )
    parser.add_argument(
        "--pois",
        type=str,
        dest="pois",
        help="names of the pois",
        required=False,
        nargs="+",
        default=[]
    )
    parser.add_argument(
        "--poi-expr",
        type=str,
        dest="poiexpr",
        help="regular expression to match POIs",
        required=False,
        default="^mu_",
    )
    parser.add_argument(
        "--no-systematics",
        action="store_true",
        dest="nosys",
        help="remove effect of systematics",
        required=False,
        default=False
    )

    args = parser.parse_args()
    extract_fitresult(args)
