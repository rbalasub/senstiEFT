#!/bin/env python
import sys
from senstiEFT.matrix import load_matrix, load_parameterisation
from senstiEFT.plotter import plotmatrix
from senstiEFT.helpers import correlation_from_covariance

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser("plot matrix")
    parser.add_argument(
        "-i",
        "--input",
        type=str,
        dest="input",
        help="input matrix",
        required=True,
        default="out.json",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="output",
        help="output matrix",
        default="out.pdf",
    )
    parser.add_argument(
        "--fontsize", type=int, dest="fontsize", help="font size", default=10
    )
    parser.add_argument(
        "--ticklabelsize",
        type=int,
        dest="ticklabelsize",
        help="ticklabel size",
        default=15,
    )
    parser.add_argument(
        "--logscale",
        action="store_true",
        help="logarithmic scale for colorbar [default=False]",
        default=False,
    )
    parser.add_argument(
        "--hide-zeros",
        action="store_true",
        dest="hide_zeros",
        help="do not show zeros on plot",
        default=True,
    )
    parser.add_argument(
        "--hide-values",
        action="store_true",
        dest="hide_values",
        help="do not show values on plot",
        default=False,
    )
    parser.add_argument(
        "--cmap",
        type=str,
        dest="cmap",
        help="color map name, available  (blue_white,blue_white_yellow)",
        default="blue_white_yellow",
    )
    parser.add_argument(
        "--normalise",
        dest="normalise",
        action="store_true",
        help="normalise the matrix",
        default=False,
    )
    parser.add_argument(
        "--valuetext-range",
        type=float,
        dest="vtext",
        help="value range for printing text in the bin",
        metavar="vmin vmax",
        nargs="+",
        default=[-1.0, 1.0],
    )
    parser.add_argument(
        "--value-range",
        type=float,
        dest="vrange",
        help="value range for colorbar",
        metavar="vmin vmax",
        nargs="+",
        default=[-1.0, 1.0],
    )

    args = parser.parse_args()
    vmin, vmax = args.vrange[0], args.vrange[1]
    vmintext, vmaxtext = args.vtext[0], args.vtext[1]
    if args.input.endswith(".json"):
        mat = load_matrix(args.input)
    if args.input.endswith(".yaml"):
        mat = load_matrix(args.input)

    if args.normalise:
        mat.matrix = correlation_from_covariance(mat.matrix)
    plotmatrix(
        mat,
        savepathfile=args.output,
        fontsize=args.fontsize,
        ticklabelsize=args.ticklabelsize,
        vtextmax=vmaxtext,
        vtextmin=vmintext,
        vmax=vmax,
        vmin=vmin,
        logscale=args.logscale,
        cmap=args.cmap,
        hide_zeros=args.hide_zeros,
    )
