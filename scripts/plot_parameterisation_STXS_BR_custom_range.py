#!/bin/env python
import numpy as np
import math
import pprint

from senstiEFT.matrix import load_matrix, load_parameterisation, load_covariance, matrix
from senstiEFT.helpers import get_rotated_matrix
from senstiEFT.matrix_helpers import get_padded_matrix
from senstiEFT.plotter import plotmatrix

## lines to make pyroot in batch mode
import sys

import ROOT

ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)


def parse_pargroups(pargroup_inputs):
    def get_name(par_input):
        if "," in par_input:
            return par_input.split(",")[0].split("=")[0]
        else:
            return par_input.split("=")[0]

    def get_val(par_input):
        if "," in par_input:
            return float(par_input.split(",")[0].split("=")[1])
        else:
            return float(par_input.split("=")[1])

    def get_color(par_input):
        if "," in par_input:
            return eval(str(par_input.split(",")[-1]))
        else:
            return None

    return [
        [
            (get_name(par_input), get_val(par_input), get_color(par_input))
            for par_input in pargroup
        ]
        for pargroup in pargroup_inputs
    ]


def plot_parameterisation(parmat,errmat=None,pargroups={},outfile="out.eps",square=False,
                          logscale=False,exclude_pars=[],xticks_angle=-45,setymax=False,setymin=False,
                          fractional_info=False,scale_by_measurement_uncertainty=False,scaling=1.0):
    xticks, yticks = parmat.get_parlabels()
    xticks = [label.replace("$", "") for label in xticks]
    yticks = [label.replace("$", "") for label in yticks]
    xlabels = {par: xticks[ipar] for par, ipar in zip(parmat.xpars, range(len(parmat.xpars)))}
    ylabels = {bin0: yticks[ibin] for bin0, ibin in zip(parmat.ypars, range(len(parmat.ypars)))}

    colors = [
        ROOT.kAzure - 4,
        ROOT.kRed,
        ROOT.kGreen + 1,
        ROOT.kGray + 2,
        ROOT.kOrange + 1,
        ROOT.kMagenta - 6,
        ROOT.kCyan + 1,
        ROOT.kViolet - 6,
        ROOT.kOrange - 6,
        ROOT.kYellow - 3,
        ROOT.kGray,
        ROOT.kYellow + 2,
        ROOT.kOrange + 3,
    ]

    bins = [ypar for ypar in parmat.ypars if ypar not in exclude_pars]
    pars = [xpar for xpar in parmat.xpars if xpar not in exclude_pars]
    nbins, ngroups = len(bins), len(pargroups)
    
    if "HTOT" in bins:
        nbins = nbins - 1
        bins.remove("HTOT")

    if errors != None:
        nplots, ypads_scale = ngroups + 1, ngroups + 1.25
    else:
        nplots, ypads_scale = ngroups, ngroups + 1
    print(
        "number of bin : {0}\nnumber of parameters {1}\nnumber of parameter groups {2}".format(
            nbins, len(pars), ngroups
        )
    )

    l_canv, b_canv = 4 * int(6 * nbins + 10), 4 * int(
        ypads_scale * 60
    )  
    
    l_canv,b_canv = 1500, 1400
    b_top, b_bottom = b_canv - 45, 200
    l_left, l_right = 12, l_canv - 12
    l_pad, b_pad = l_canv, (b_top - b_bottom) / (1.0 * ypads_scale)

    linewidth = 4
    dashedlinewidth = 2

    print(" (l,b) of canvas : ({0},{1}) ".format(l_canv, b_canv))
    print(" (top,bottom) of canvas : ({0},{1}) ".format(b_top, b_bottom))
    print(" (left,right) of canvas : ({0},{1}) ".format(l_left, l_right))
    print(" (length of pad, b_pad) of canvas : ({0},{1}) ".format(l_pad, b_pad))

    if logscale:
        ymin, ymax = 1e-5, 0.9
    else:
        ymin, ymax = -1.0, 1.0
    if setymax:
        ymax = setymax
    if setymin:
        ymin = setymin

    print(ymax, ymin)
    import sys
    ROOT.gStyle.SetLineScalePS(0.5)
#    ROOT.gStyle.SetPaperSize(l_canv * 0.1, b_canv * 0.1)
    cst0 = ROOT.TCanvas("cst0", "cst0", l_canv, b_canv)
#    cst0.SetCanvasSize(l_canv, b_canv)

    ytitlesize = 0.12  # * y_modifier
    ticklabelsize = 0.1  # * y_modifier
    ticklabeloffset = -0.009
    ytitleoffset = 0.2  # * y_modifier
    canvas_margin = {"r": 0.0, "l": 0.0, "b": 100.0, "t": -5.0}
    pad_margin = {"r": -5.0 / l_canv, "l": 100.0 / l_canv, "b": 25.0 / b_canv, "t": 0.0}
    l, l1 = {}, {}
    h_frame = {}
    histos = {}
    legend = {}
    graybox = {}
    spac = max([len(group) for group in pargroups]) + 2
    # stack histogram per group
    
    scales = {"group_0" : 1,
             "group_1" : 20,
             "group_2" : 2,
             "group_3" : 4000,
             "group_4" : 400,
             "group_5" : 1,
             "group_6" : 1}
    
    ranges = {}
    for group, group_tag in zip(pargroups, range(len(pargroups))):
        group_name = "{0}_{1}".format("group", group_tag)
        ranges[group_name] = {"lo": 0, "hi": 0}
        # if len(group) > 9 : spac = len(group)
        h_frame[group_name] = ROOT.TH1D(
            "hs_{0}".format(group_tag), "", int(nbins), 0, int(spac * nbins)
        )
        histos[group_name], legend[group_name] = {}, {}
            
        for par_info, ipar in zip(group, range(len(group))):
            par = par_info[0]
            if par_info[2] == None:
                if ipar >= len(colors):
                    parcolor = ROOT.kGray + 2
                else:
                    parcolor = colors[ipar]
            else:
                parcolor = par_info[2]

            if par in xlabels.keys():
                label = xlabels[par]
            else:
                label = par

            par_sq = "*".join([par,par])
            histos[group_name][par] = ROOT.TH1F(par, par, nbins * spac, 0, nbins * spac)
            histos[group_name][par_sq] = ROOT.TH1F(par_sq, par_sq, nbins * spac, 0, nbins * spac)
            histos[group_name][par].SetFillColor(parcolor)
            histos[group_name][par_sq].SetLineColor(parcolor)
            histos[group_name][par].SetLineColor(parcolor)
            histos[group_name][par].SetLineWidth(3)
            histos[group_name][par_sq].SetLineWidth(2)

            if square:
                histos[group_name][par].SetFillColorAlpha(parcolor,0.75)
            
            for bin0, ibin in zip(bins, range(nbins)):
                if "gap" in bin0: continue
                h_frame[group_name].GetXaxis().SetBinLabel(ibin + 1, bin0)
                parval = par_info[1]
                linear_eftval, quadratic_eftval, smval = 0.0, 0.0, 1.0
                width_linear, width_quadratic = 0.0, 0.0
                if par in parmat.xpars:
                    linear_eftval, smval = parmat.getElem(par, bin0), 1.0
                    width_linear = parmat.getElem(par, "HTOT")
                    if fractional_info:
                        sum_info = 0.0
                        for bin in bins:
                            if "gap" in bin: continue
                            sum_info += parmat.getElem(par, bin)*parmat.getElem(par, bin)/(errmat.getElem("error", bin)*errmat.getElem("error", bin))
                                                        
                        linear_eftval = parmat.getElem(par, bin0)*parmat.getElem(par, bin0)/(errmat.getElem("error", bin0)*errmat.getElem("error", bin0))
                        #print(par, bin0, sum_info, linear_eftval, linear_eftval/sum_info)
 
                        linear_eftval = linear_eftval/sum_info
                    if square:
                        if par_sq in parmat.xpars:
                            quadratic_eftval = parmat.getElem(par_sq, bin0)
                            width_quadratic = parmat.getElem(par_sq, "HTOT")

                if "SM" in parmat.xpars:
                    smval = parmat.getElem("SM", bin0)
                    if smval == 0.0:
                        parval, smval = 0.0, 1.0
            
                if not fractional_info:
                    linear_impact = (1 + linear_eftval * parval)/(1 + width_linear * parval) - 1
                    quad_impact = (1 + linear_eftval * parval + quadratic_eftval * parval * parval)/(1 + width_linear * parval + width_quadratic * parval * parval) - 1
                    if scale_by_measurement_uncertainty:
                        err = 1.0
                        if bin0 in errmat.ypars:
                            err = errmat.getElem("error", bin0)
                        linear_impact = linear_impact / err
                        quad_impact = quad_impact / err
                else:
                    linear_impact = linear_eftval
                    quad_impact = quadratic_eftval
                if logscale:
                    linear_impact = abs(linear_impact)

                #finalval = finalval  # /err #mat.getElem("error",bin0)
                #print(finalval)
                if linear_impact > ranges[group_name]["hi"]:
                    ranges[group_name]["hi"] = linear_impact
                if linear_impact < ranges[group_name]["lo"]:
                    ranges[group_name]["lo"] = linear_impact
                
                if quad_impact > ranges[group_name]["hi"]:
                    ranges[group_name]["hi"] = quad_impact
                if quad_impact < ranges[group_name]["lo"]:
                    ranges[group_name]["lo"] = quad_impact

                histos[group_name][par].SetBinContent(spac * ibin + (ipar), linear_impact/scales[group_name])
                histos[group_name][par_sq].SetBinContent(spac * ibin +(ipar), quad_impact/scales[group_name])
                histos[group_name][par].SetLineWidth(2)
                h_frame[group_name].GetXaxis().SetLabelSize(0)
                h_frame[group_name].GetXaxis().SetNdivisions(nbins, 0)

                scale, err_ymin, err_ymax = (
                    2.14,
                    abs(ymin) * 1.1 * 1e-4,
                    abs(ymax) * 2 * 1e-0,
                )

            if logscale:
                legend[group_name][par] = ROOT.TLatex(
                    spac * (nbins + 0.1),
                    (ymax * 0.8) * pow((0.5), (ipar)),
                    " " + label + "=" + str(parval),
                )
            else:
                if not fractional_info:
                    legend[group_name][par] = ROOT.TLatex(
                        spac * (nbins + 0.1),
                        -0.2 + ymax - ymax * scaling * (ipar), # 3 * 0.082 for warsaw
                        " " + label + "=" + str(parval),
                    )  # spac*(nbins+0.1), -0.2 + ymax - ymax*10.0*0.075*(ipar)," "+label +"=" + str(parval))
                else:
                    legend[group_name][par] = ROOT.TLatex(
                        spac * (nbins + 0.1),
                        -0.12 + ymax - ymax * 2.8 * 0.07 * (ipar), # 3 * 0.082 for warsaw
                        " " + label,
                    )  # spac*(nbins+0.1), -0.2 + ymax - ymax*10.0*0.075*(ipar)," "+label +"=" + str(parval))
            legend[group_name][par].SetTextFont(72)
            # legend[group_name][par].SetIndiceSize(0.25)
#            legend[group_name][par].SetTextSize(0.07) warsaw
           # if "cZH" in par:
           #     legend[group_name][par].SetTextSize(0.045)
            legend[group_name][par].SetTextSize(0.125)
            # lege[group_name][par]r].SetTextSizePixels(45)
            legend[group_name][par].SetTextColor(parcolor)
            legend[group_name][par].SetTextAlign(10)
        
        #if scales[group_name] != 1.0:
        legend[group_name]["scaling"] = ROOT.TLatex(spac * (-2.0),  ymax*0.9275, " " + str(scales[group_name]) + "x")
        legend[group_name]["scaling"].SetTextFont(42)
        legend[group_name]["scaling"].SetTextSize(0.11*1.5)
        legend[group_name]["scaling"].SetTextAlign(30)

    # filling in the histogram
    # prop*nbins,50*nplots)
    # nbins = 10
    # nplots = 50
    # nspace = 0.9 * 150
    # 100, 150
    ## common pars for pads
    cst0.SetTickx()
    cst0.SetLeftMargin(canvas_margin["l"])
    cst0.SetBottomMargin(canvas_margin["b"])
    cst0.SetTopMargin(canvas_margin["t"])
    cst0.Divide(1, nplots, 0, 0)

    # first pad for the error bars
    if errmat != None:
        pad = cst0.cd(1)
        xlow, yhigh = l_left / (1.0 * l_canv), b_top / (1.0 * b_canv)
        xhigh, ylow = l_right / (1.0 * l_canv), yhigh - b_pad / (2.0 * (b_canv))
        #print(xlow, ylow, xhigh, yhigh)
        pad.SetPad(xlow, ylow, xhigh, yhigh)
        pad.SetTopMargin(35.0 / l_canv)
        pad.SetBottomMargin(30.0 / l_canv)
        pad.SetLeftMargin(pad_margin["l"])
        pad.SetRightMargin(pad_margin["r"])
        ROOT.gStyle.SetPadRightMargin(0.1)
        if logscale:
            pad.SetLogy()
        # histogram for showing unc uncertainities
        errhisto = ROOT.TH1F("errhisto", "", nbins, 0, nbins)
        for bin0, ibin in zip(bins, range(nbins)):
            if bin0 in errmat.ypars:
                errhisto.Fill(ibin, errmat.getElem("error", bin0))
                #errhisto.Fill(ibin, 1e-06)
            else:
                print(
                    "measurement error not provided for {0}, plotting with placeholder error (1e-6)".format(
                        bin0
                    )
                )
                errhisto.Fill(ibin, 1e-06)
            errhisto.SetBinError(ibin + 1, 1e-06)

        if logscale:
            scale, err_ymin, err_ymax = (
                2.14,
                (1.1 * 1e-4) * abs(ymin),
                (2 * 1e-0) * abs(ymax),
            )
            errhisto.GetYaxis().SetLabelOffset(-0.0125)
            errhisto.GetYaxis().SetTickSize(5*scale / (l_canv))
            errhisto.GetYaxis().SetTitleOffset(ytitleoffset * 0.4)
            errhisto.GetYaxis().SetTitleSize(ytitlesize * scale)
        else:
            scale, err_ymin, err_ymax = 2.14, (1.1 * 1e-4) * abs(ymin), 1.2*abs(ymax)
            errhisto.GetYaxis().SetLabelOffset(0.005)
            errhisto.GetYaxis().SetTickLength(0.005)
            errhisto.GetYaxis().SetNdivisions(404)
            errhisto.GetYaxis().SetTitleOffset(ytitleoffset * 0.2)
            errhisto.GetYaxis().SetTitleSize(ytitlesize * scale)

        errhisto.SetFillColor(ROOT.kGray + 1)
        errhisto.SetFillStyle(3345)
        errhisto.SetYTitle("unc.")

        # errhisto.GetYaxis().SetTickSize(2./l_canv)#*math.sqrt(x_modifier*y_modifier)))
        errhisto.SetMinimum(err_ymin)
        errhisto.SetMaximum(err_ymax)
        errhisto.SetMarkerSize(0)
        errhisto.GetXaxis().SetNdivisions(nbins, 0)
        errhisto.GetXaxis().SetLabelSize(0)

        errhisto.GetYaxis().SetLabelSize(ticklabelsize * scale)
        # errhisto.GetYaxis().SetTextAlign(21)
        errhisto.SetLineWidth(4)
        errhisto.SetLineColor(ROOT.kGray+1)
        errhisto.SetMarkerColor(ROOT.kGray+1)
        errhisto.Draw("hist same")
        errhisto.Draw("e same")

        ibox = 0
        for bin0 in bins:
            if "gap" in bin0:
                ibox = ibox + 1
                name = "_".join(["err_hist", str(ibox)])
                graybox[name] = ROOT.TBox(
                    bins.index(bin0), err_ymin, bins.index(bin0) + 1, err_ymax
                )
                graybox[name].SetFillColor(19)
                graybox[name].Draw()
        ## draw solid lin3e at the top to avoid a thin boundary
        l["errhisto_top"] = ROOT.TLine(0.0, err_ymax, nbins, err_ymax)
        l["errhisto_top"].SetLineColor(ROOT.kBlack)
        l["errhisto_top"].SetLineWidth(linewidth)
        l["errhisto_top"].Draw("same")
        ## draw solid line at the bottom to avoid a thin boundary
        l["errhisto_bottom"] = ROOT.TLine(0.0, err_ymin, nbins, err_ymin)
        l["errhisto_bottom"].SetLineColor(ROOT.kBlack)
        l["errhisto_bottom"].SetLineWidth(linewidth)
        l["errhisto_bottom"].Draw("same")
        ## draw solid line at the left to avoid a thin boundary
        l["errhisto_left"] = ROOT.TLine(0.0, err_ymin, 0.0, err_ymax)
        l["errhisto_left"].SetLineColor(ROOT.kBlack)
        l["errhisto_left"].SetLineWidth(linewidth)
        l["errhisto_left"].Draw("same")
        ## draw solid line at the right to avoid a thin boundary
        l["errhisto_right"] = ROOT.TLine(nbins, err_ymin, nbins, err_ymax)
        l["errhisto_right"].SetLineColor(ROOT.kBlack)
        l["errhisto_right"].SetLineWidth(linewidth)
        l["errhisto_right"].Draw("same")


        # pad.Draw()
    # fill in the remaining pads with the eft impact plots
    ylow = ylow * 0.998
    ierr = nplots - ngroups
    for i in range(ngroups):
        pad = cst0.cd(i + 1 + ierr)
        if ierr:
            yhigh = ylow 
            #ierr = 0
        else:
            yhigh = (b_top - i * b_pad) / (1.0 * b_canv)
        xlow = l_left / (1.0 * l_canv)
        xhigh, ylow = l_right / (1.0 * l_canv), yhigh - b_pad / (1.0 * (b_canv))
        print("xlow : {0:.2f}, ylow:{1:.2f}, xhigh:{2:.2f}, yhigh:{3:.2f}".format(xlow, ylow, xhigh, yhigh))
        pad.SetPad(xlow, ylow, xhigh, yhigh)
        pad.SetTopMargin(pad_margin["t"])
        pad.SetBottomMargin(pad_margin["b"])
        pad.SetLeftMargin(pad_margin["l"])
        pad.SetRightMargin(pad_margin["r"])
        ROOT.gStyle.SetPadRightMargin(0.1)



        if logscale:
            pad.SetLogy()
        group_name = "group_{0}".format(i)
        # h_frame[group_name].Draw("nostackbhist")
        h_frame[group_name].GetXaxis().SetLabelSize(0)
        h_frame[group_name].GetXaxis().SetNdivisions(nbins, 0)

#       ymin = h_frame[group_name].GetMinimum()
#        ymax = h_frame[group_name].GetMaximum()
        
#        print(ymin, ymax)
        
        #import sys
        #sys.exit()
        #ymax = ranges[group_name]["hi"] * 1.1
        #ymin = ranges[group_name]["lo"] * 1.1
        #h_frame[group_name].SetMinimum(ranges[group_name]["lo"] * 1.1)
        #h_frame[group_name].SetMaximum(ranges[group_name]["hi"] * 1.1)
        h_frame[group_name].SetMinimum(ymin*1.1)
        h_frame[group_name].SetMaximum(ymax*1.1)

        ylabel = "#Delta(#sigma #times B)(c_{i})/SM     "
        if scale_by_measurement_uncertainty:
            ylabel = "#Delta(#sigma #times B)(c_{i})/(SM #times #sigma^{unc.}_{meas.})"
        if fractional_info:
            ylabel = "1/n contr. to info."
        h_frame[group_name].GetYaxis().SetTitleSize(ytitlesize)
        h_frame[group_name].GetYaxis().SetTitle(ylabel)
        h_frame[group_name].GetYaxis().SetTitleOffset(ytitleoffset)
        h_frame[group_name].GetYaxis().SetLabelSize(ticklabelsize)
        h_frame[group_name].GetYaxis().SetTickSize(5.0/l_canv)
        h_frame[group_name].Draw("hist")
#        for par in histos[group_name]:
#            histos[group_name][par].Draw("hist same")
            
        #if square:
        #    for case in histos[group_name]:
        #        histos[group_name][case].Draw("hist same")
            
        h_frame[group_name].Draw("axis same")
        h_frame[group_name].Draw("hist same")

        if logscale:
            ylabel = "|" + ylabel + "|"
        ## draw dashed lines to segregate bins


        ROOT.gStyle.SetLineStyleString(26,"4 4");

        l1[group_name] = { "{0}_{1}".format(group_name, k): ROOT.TLine(k * spac, ymin * 1.1, k * spac, ymax * 1.1) for k in range(1, nbins)}
        for tline in l1[group_name].values():
            tline.SetLineWidth(dashedlinewidth)
            tline.SetLineColor(ROOT.kGray)
            tline.SetLineStyle(26)
            tline.Draw("same")

        for par in histos[group_name]:
            histos[group_name][par].Draw("hist same")
            
#        if square:
#            for case in histos[group_name]:
#                histos[group_name][case].Draw("hist same")
            
        h_frame[group_name].Draw("axis same")
        h_frame[group_name].Draw("hist same")


        ## draw solid line at the middle
        l["{0}_{1}".format(group_name, "mid")] = ROOT.TLine(0.0, 0.0, spac * nbins, 0.0)
        l["{0}_{1}".format(group_name, "mid")].SetLineColor(ROOT.kBlack)
        l["{0}_{1}".format(group_name, "mid")].SetLineWidth(linewidth)
        l["{0}_{1}".format(group_name, "mid")].Draw("same")
        ibox = 0
        for bin0 in bins:
            if "gap" in bin0:
                ibox = ibox + 1
                name = "_".join([group_name, str(ibox)])
                graybox[name] = ROOT.TBox(
                    spac * bins.index(bin0) + spac*0.07,
                    ymin * 1.1,
                    spac * bins.index(bin0) + spac*0.93,
                    ymax * 1.1,
                )
                graybox[name].SetFillColor(19)
                graybox[name].Draw()
        ## draw solid line at the top to avoid a thin boundary
        l["{0}_{1}".format(group_name, "up")] = ROOT.TLine(
            0.0, ymin * 1.1, spac * nbins, ymin * 1.1
        )
        l["{0}_{1}".format(group_name, "up")].SetLineColor(ROOT.kBlack)
        l["{0}_{1}".format(group_name, "up")].SetLineWidth(linewidth)  
        l["{0}_{1}".format(group_name, "up")].Draw("same")

        ## draw solid line at the top to avoid a thin boundary
        l["{0}_{1}".format(group_name, "dn")] = ROOT.TLine(
            0.0, ymax * 1.1, spac * nbins, ymax * 1.1
        )
        l["{0}_{1}".format(group_name, "dn")].SetLineColor(ROOT.kBlack)
        l["{0}_{1}".format(group_name, "dn")].SetLineWidth(linewidth) 
        l["{0}_{1}".format(group_name, "dn")].Draw("same")

        l["{0}_{1}".format(group_name, "left")] = ROOT.TLine(
            0.0, ymin * 1.1, 0.0, ymax * 1.1
        )
        l["{0}_{1}".format(group_name, "left")].SetLineColor(ROOT.kBlack)
        l["{0}_{1}".format(group_name, "left")].SetLineWidth(linewidth)  # *math.sqrt(x_modifier*y_modifier)))
        l["{0}_{1}".format(group_name, "left")].Draw("same")

        l["{0}_{1}".format(group_name, "right")] = ROOT.TLine(
            spac*nbins, ymin * 1.1, spac*nbins, ymax * 1.1
        )
        l["{0}_{1}".format(group_name, "right")].SetLineColor(ROOT.kBlack)
        l["{0}_{1}".format(group_name, "right")].SetLineWidth(linewidth)
        l["{0}_{1}".format(group_name, "right")].Draw("same")

        for par, ipar in zip(legend[group_name], range(len(legend[group_name]))):
            legend[group_name][par].Draw()

    for i in range(ngroups,ngroups+1):
        ierr = nplots - ngroups
        pad = cst0.cd(i + 1 + ierr)
        if ierr:
            yhigh = ylow
        else:
            yhigh = (b_top - i * b_pad) / (1.0 * b_canv)
        xlow = l_left / (1.0 * l_canv)
        xhigh, ylow = l_right / (1.0 * l_canv), yhigh - b_pad / (1.0 * (b_canv))
        print("xlow : {0:.2f}, ylow:{1:.2f}, xhigh:{2:.2f}, yhigh:{3:.2f}".format(xlow, ylow, xhigh, yhigh))
    
    cst0.cd()
    t3 = ROOT.TLatex()
    t3.SetTextFont(73)
    t3.SetTextColor(ROOT.kBlack)
    t3.SetTextSize(22)
    t3.DrawLatexNDC((0.08 * l_canv) / l_canv, 0.97, "ATLAS")
    t3.SetTextFont(43)
    t3.SetTextSize(20)
    t3.DrawLatexNDC(( 80 + 0.08 * l_canv) / l_canv, 0.97, "Internal")
    t3.SetTextSize(11)
    t3.DrawLatexNDC((0.08 * l_canv) / l_canv, 0.958, "#sqrt{s} = 13 TeV, 139 fb^{-1}")

    t3.SetTextSize(18)
    t3.SetTextFont(45)
    t3.DrawLatexNDC(0.22,0.975,"H#rightarrow #gamma#gamma")
    t3.DrawLatexNDC(0.35,0.975,"H#rightarrow Z#gamma")
    t3.DrawLatexNDC(0.40,0.975,"H#rightarrow WW*#rightarrow l#nu l#nu")
    t3.DrawLatexNDC(0.53,0.975,"H#rightarrow ZZ*#rightarrow 4l")
    t3.DrawLatexNDC(0.68,0.975,"H#rightarrow b#bar{b}")
    #t3.DrawLatexNDC(0.75,0.965,"H#rightarrow c#bar{c}")
    t3.DrawLatexNDC(0.775,0.975,"H#rightarrow #mu#mu")
    t3.DrawLatexNDC(0.84,0.975,"H#rightarrow #tau#tau")
    #t3.DrawLatexNDC(0.885,0.965,"#Gamma_{H}")
    # Now draw the xlabels on the bottom

    tex2 = ROOT.TLatex()
    tex2.SetTextFont(42)
    tex2.SetTextAlign(11)
    for ibin, binlabel in zip(range(nbins), bins):
        if "gap" in binlabel:
            continue
        angle, ylabel_shift, label_size, xshift = xticks_angle, -0.69*b_pad, 0.0075, 0.072 * l_canv 
        xpos = (l_left + xshift + ibin * 1.025 * 0.945 * (l_right - l_left - 0.14 * l_canv) / (nbins))/ l_canv
        ypos = (b_bottom - ylabel_shift) / b_canv
        tex2.SetTextAngle(angle)
        tex2.SetTextSize(label_size)
        tex2.SetTextColor(ROOT.kBlack)
        tex2.DrawLatexNDC(xpos, ypos, ylabels[binlabel])

    cst0.ForceUpdate()
    cst0.Draw()

    cst0.SaveAs(outfile)


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser("plot EFT parameterisation")
    parser.add_argument(
        "-i",
        "--input",
        type=str,
        dest="parmat_loc",
        help="input json file with parameterisation matrix",
        required=True,
        metavar="path/to/file",
    )
    parser.add_argument(
        "--errors",
        type=str,
        dest="errors_loc",
        help="input json file with measurement errors (to be used for showing measurement unc.)",
        required=False,
        metavar="path/to/file",
        default=None,
    )
    parser.add_argument(
        "--observables",
        type=str,
        dest="observables_regex",
        help="regular expression to match observables to plot",
        required=False,
        default=".*"
    )
    parser.add_argument(
        "--scale-by-measurement-uncertainty",
        action="store_true",
        dest="scale_by_measurement_uncertainty",
        help="scale impact by measurement uncertainty",
        required=False,
        default=False
    )
    parser.add_argument(
        "--fractional-info",
        action="store_true",
        dest="fractional_info",
        help="plot fractional information of each bin",
        required=False,
        default=False
    )
    parser.add_argument(
        "--group",
        nargs="+",
        action="append",
        type=str,
        dest="pargroups",
        help="list of parameter groups",
        default=[],
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="out",
        help="path to save parameterisation plot",
        required=False,
        default="out.eps",
    )
    parser.add_argument(
        "--exclude",
        type=str,
        dest="excludepars",
        help="pars to exclude.",
        metavar="POI",
        nargs="+",
        default=[],
    )
    parser.add_argument(
        "--covariance",
        type=str,
        dest="covariance",
        help="covariance matrix of measurement bins",
        default="",
    )
    parser.add_argument("--rotation", type=str, dest="rotation", default="")
    parser.add_argument(
        "--logscale",
        action="store_true",
        dest="logscale",
        help="log scale for the yaxis",
        default=False,
    )
    parser.add_argument(
        "--matrix-only",
        type=str,
        dest="matrix",
        default="",
    )
    parser.add_argument(
        "--xticks-angle",
        type=float,
        dest="xticks_angle",
        help="angle to rotate xticks.",
        default=-45,
    )
    parser.add_argument(
        "--ymax", type=float, dest="ymax", help="maximum range of yaxis", default=False
    )
    parser.add_argument(
        "--ymin", type=float, dest="ymin", help="minimum range of yaxis", default=False
    )
    parser.add_argument(
        "--k", type=float, dest="scale", help="scaling parameter", default=1.0
    )
    parser.add_argument(
        "--square", action="store_true", dest="square", help="show quad contribution", default=False
    )

    args = parser.parse_args()
    pargroups = parse_pargroups(args.pargroups)
    parmat = load_parameterisation(args.parmat_loc)
    if parmat == None:
        print("ERROR: could not load parameterisation matrix")
        exit(1)
    
    import re
    observables = parmat.ypars
    regex_observables = re.compile(args.observables_regex)
    for observable in observables:
        if not regex_observables.match(observable):
            observables.remove(observable)
    
    parmat.reduce_matrix(xpars=parmat.xpars, ypars=observables)

    def dict_to_mat(dict0,name=""):
        xpars = []
        ypars = []
        for obs in dict0:
            ypars.append(obs)
            for par in dict0[obs]:
                xpars.append(par)

        mat = []           
        xpars = list(set(xpars))
        for ypar in ypars:
            tmp = []
            for xpar in xpars:
                tmp.append(dict0[ypar][xpar])
            mat.append(tmp)
        return matrix(name, mat, xpars=xpars, ypars=ypars)

    def rotate_quad_param(rotmat, parmat, no_cross=True):
        # it may be more simpler in sympy but let's give it a try with RooFit
        from senstiEFT.param_helpers import rotation_expr, prepare_expr, print_expr
        ws = ROOT.RooWorkspace("ws")
        for ypar in rotmat.ypars:
            ws.factory("{0}[0,-1e6,1e6]".format(ypar))
            ws.var(ypar).setError(0.0001)

        print("Created POIs for rotated basis")
        for xpar in rotmat.xpars:
            ws.factory(rotation_expr(rotmat,xpar))

        for ypar in parmat.ypars:
            
            ws.factory(prepare_expr(parmat,ypar))

        pars = rotmat.ypars 
        square_terms = [ "{0}*{1}".format(par, par) for par in rotmat.ypars]
        cross_terms = [ "{0}*{1}".format(par1, par2) for par1 in rotmat.ypars for par2 in rotmat.ypars[rotmat.ypars.index(par1)+1:]]
        all_terms = rotmat.ypars + square_terms
        if not no_cross:
            all_terms = all_terms + cross_terms # important to evaluate the contributions in this order
        new_ypars = [ ypar + "_new" for ypar in parmat.ypars]
        pardict = {obs:{} for obs in new_ypars}   

        for poi in new_ypars:
            for term in all_terms:
                if "*" not in term:
                    par = term
                    ws.var(par).setVal(1.0)
                    valp1 = ws.obj(poi).getValV()
                    ws.var(par).setVal(-1.0)
                    valm1 = ws.obj(poi).getValV()
                    ws.var(par).setVal(0.0)
                    int_val = (valp1 - valm1)*0.5
                    pardict[poi][par] = int_val     

                if "*" in term:
                    par1, par2 = term.split("*")
                    if par1 == par2:
                        ws.var(par1).setVal(1.0)
                        valp1 = ws.obj(poi).getValV()
                        ws.var(par1).setVal(1.0)
                        valm1 = ws.obj(poi).getValV()
                        ws.var(par1).setVal(0.0)
                        sq_val = (valp1 + valm1)*0.5 - 1.0
                        pardict[poi][term] = sq_val

                    else:
                        ws.var(par1).setVal(1.0)
                        ws.var(par2).setVal(1.0)
                        val_inc_cross = ws.obj(poi).getValV()
                        val_cross = val_inc_cross - 1.0 - pardict[poi][par1] - pardict[poi][par2] - pardict[poi]["{0}*{0}".format(par1)] - pardict[poi]["{0}*{0}".format(par2)]
                        ws.var(par1).setVal(0.0)
                        ws.var(par2).setVal(0.0)
                        pardict[poi][term] = val_cross

        labels = {**parmat.labels, **rotmat.labels}
        rotated_parmat = dict_to_mat(pardict,"rotated parmat")
        rotated_parmat.labels = labels
        #print(labels)
        new_ypars = [ypar.replace("_new", "") for ypar in rotated_parmat.ypars]
        rotated_parmat.ypars = new_ypars
        return rotated_parmat
                                 
    if len(args.rotation):
        rotation = load_matrix(args.rotation)
        for par in parmat.xpars:
            if "*" not in par:
                if par not in rotation.xpars:
                    print("ERROR: rotation matrix does not contain all parameters")
                    print(par)
                
        for par in rotation.xpars:
            if par not in parmat.xpars:
                print("ERROR: rotation matrix does not contain all parameters")
                print(par)

        if "SM" in parmat.xpars:
            rotmat = get_padded_matrix(
                rotation,
                pad_name=rotation.name,
                pad_xpars=["SM"] + rotation.xpars,
                pad_ypars=["SM"] + rotation.ypars,
                pad_val=0.0,
            )
            rotmat.setElem("SM", "SM", 1.0)
            rotation = rotmat

        if args.square:
            print("Rotating quadratics")
            parmat   = rotate_quad_param(rotation, parmat, no_cross=False)

        else:
            linpars = [par for par in parmat.xpars if "*" not in par]
            parmat.reduce_matrix(xpars=linpars, ypars=parmat.ypars)
            parmat = get_rotated_matrix(parmat, rotation)

    if args.matrix != "":
        maxval = max(parmat.matrix.max(), abs(parmat.matrix.min()))
        plotmatrix(
            parmat, savepathfile=args.matrix, vmax=maxval, vmin=-maxval, fontsize=5, ticklabelsize=8
        )
        print("Plotting matrix only, exiting")
        print("Saved matrix plot to {0}".format(args.matrix))   
        print("To plot the impact plot : remove --matrix-only flag\n")
        sys.exit(0)

    if args.errors_loc != None and args.covariance != "":
        print("ERROR: cannot specify both errors and covariance")
        exit(1)

    elif args.errors_loc != None:
        print("Loading errors {0}".format(args.errors_loc))
        errors = load_matrix(args.errors_loc)
        
    elif args.covariance != "":
        covariance = load_covariance(args.covariance)   
        errors_arr = [ [ math.sqrt(covariance.getElem(ypar,ypar))] for ypar in covariance.ypars]
        errors = matrix("errors", errors_arr, ["error"], covariance.ypars)

    else:
        errors = None
        
#    includes_quadratic = False
#    for x in parmat.xpars:
#        if "*" in x:  
#            includes_quadratic = True
#            break
#    print(parmat.xpars)
    plot_parameterisation(
        parmat,
        errors,
        pargroups,
        args.out,
        square=args.square,
        exclude_pars=args.excludepars,
        xticks_angle=args.xticks_angle,
        logscale=args.logscale,
        fractional_info=args.fractional_info,
        setymax=args.ymax,
        setymin=args.ymin,
        scale_by_measurement_uncertainty=args.scale_by_measurement_uncertainty,
        scaling=args.scale
    )

