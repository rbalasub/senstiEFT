#!/bin/env python
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import json
import os
if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(description="barplot")
    parser.add_argument( "--input", type=str, help="POI ranges to scan the Nll.", metavar=("case","input_dir"), default=None,nargs=2,action="append")
    args = parser.parse_args()

def autolabel(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

group1=[]
group2=[]
group3=[]
group4=[]
results={}
for i in args.input:
    pardict={}
    for file in os.listdir(i[1]):
        with open(i[1]+"/"+file) as f:
            param=json.load(f)["MLE"]["parameters"]
        for j in param:
            name=j["name"]
            err=j["err"]
            if err < 0.01: group1.append(name)
            if ((err< 0.2) and (err >= 0.01)): group2.append(name)
            if ((err< 4) and (err >= 0.2)): group3.append(name)
            if ((err >= 4)): group4.append(name)
            pardict[name]=err
    results[i[0]]=pardict

#results={ "case1" : pardict}


groups = [group1,group2,group3,group4 ]

cases = list(results.keys())
heights=[]
heights.append(np.sqrt(len(group1)))
heights.append(np.sqrt(len(group2)))
heights.append(np.sqrt(len(group3)))
heights.append(np.sqrt(len(group4)))
f, axs = plt.subplots(len(groups), 1,figsize=(10,25),sharex='row',height_ratios=heights )
plt.rcdefaults()

i = 0
for group in groups:
    pars = group
    vals = {}
    rects = {}
    x = np.arange(len(pars))  
    width = 0.5  # the width of the bars

    icase = 0
    maxval = 0
    for case in cases:
        vals[case] = [results[case][par] for par in group]
        rects[case] = axs[i].barh(x - width/(0.9*(2./3.))+ (icase)*width*(2./3.), vals[case], width*(2./3.),label=case)
        if max(vals[case]) > maxval: maxval = max(vals[case])
        icase = icase + 1
        
    # the label locations
    #ax.barh(y_pos, performance, xerr=error, align='center')
    axs[i].set_yticks(x-width/(0.9*len(cases)*(2./3.)))
    axs[i].set_yticklabels(pars)
    axs.flat[i].set_xlim(0,1.5*maxval)# labels read top-to-bottom
    i = i+1
    if i == len(group)-1: axs[i].set_xlabel('Parameter Sensitivity '+r'($\sigma$)')

l4 = plt.legend( loc="upper right")
plt.subplots_adjust(hspace=0.3)
plt.savefig("test.pdf")


