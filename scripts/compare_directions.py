#!/bin/env python
#!/usr/bin/env python

import numpy as np
from senstiEFT.matrix import matrix, load_matrix
from senstiEFT.plotter import plotmatrix


def transform(mat1, mat2):
    if mat1.xpars == mat2.xpars:
        mat2.transpose()
    name = "{0} in terms of {1}".format(mat1.name, mat2.name)
    mat3 = matrix(
        name, np.matmul(mat1.matrix, mat2.matrix), ypars=mat1.ypars, xpars=mat2.xpars
    )
    mat3.save_matrix(args.out)
    return mat3


def compare_bases(args):
    print(args.inputs)
    if len(args.inputs) != 2:
        print("Please provide two transformation matrices !")
        exit(0)

    else:
        matrices = [load_matrix(file0[0]) for file0 in args.inputs]
        new_transmat = transform(matrices[0], matrices[1])
        if len(args.plt) != 0:
            plotmatrix(
                new_transmat,
                savepathfile=args.plt,
                vmax=1.0,
                vmin=-1.0,
                vals=True,
                zero=False,
                fontsize=10,
            )


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(
        "perform comparing different basis definition starting from the transformation matrix"
    )
    parser.add_argument(
        "-i",
        "--input",
        "--inputs",
        nargs="+",
        action="append",
        type=str,
        dest="inputs",
        help="input json files with the transformation matrix",
        required=True,
        metavar="path/to/file.json",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="out",
        help="save transformation matrix",
        required=True,
        default="out.json",
    )
    parser.add_argument(
        "--plot-matrix",
        type=str,
        dest="plt",
        help="plot transformation matrix",
        required=False,
        default="",
    )

    args = parser.parse_args()
    compare_bases(args)
