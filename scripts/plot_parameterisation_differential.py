#!/bin/env python
import numpy as np
import math
import pprint

from senstiEFT.matrix import load_matrix, load_parameterisation, load_covariance, matrix
from senstiEFT.helpers import get_rotated_matrix
from senstiEFT.matrix_helpers import get_padded_matrix

## lines to make pyroot in batch mode
import sys

import ROOT

ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)


def parse_pargroups(pargroup_inputs):
    def get_name(par_input):
        if "," in par_input:
            return par_input.split(",")[0].split("=")[0]
        else:
            return par_input.split("=")[0]

    def get_val(par_input):
        if "," in par_input:
            return float(par_input.split(",")[0].split("=")[1])
        else:
            return float(par_input.split("=")[1])

    def get_color(par_input):
        if "," in par_input:
            return eval(str(par_input.split(",")[-1]))
        else:
            return None

    return [
        [
            (get_name(par_input), get_val(par_input), get_color(par_input))
            for par_input in pargroup
        ]
        for pargroup in pargroup_inputs
    ]


def plot_parameterisation(
    parmat,
    errmat=None,
    pargroups={},
    outfile="out.eps",
    square=False,
    logscale=False,
    exclude_pars=[],
    xticks_angle=-45,
    setymax=False,
    setymin=False,
):
    xticks, yticks = parmat.get_parlabels()
    xticks = [label.replace("$", "") for label in xticks]
    yticks = [label.replace("$", "") for label in yticks]
    xlabels = {
        par: xticks[ipar] for par, ipar in zip(parmat.xpars, range(len(parmat.xpars)))
    }
    ylabels = {
        bin0: yticks[ibin] for bin0, ibin in zip(parmat.ypars, range(len(parmat.ypars)))
    }

    colors = [
        ROOT.kAzure - 4,
        ROOT.kRed,
        ROOT.kGreen + 1,
        ROOT.kGray + 2,
        ROOT.kOrange + 1,
        ROOT.kMagenta - 6,
        ROOT.kCyan + 1,
        ROOT.kViolet - 6,
        ROOT.kOrange - 6,
        ROOT.kYellow - 3,
        ROOT.kGray,
        ROOT.kYellow + 2,
        ROOT.kOrange + 3,
    ]
    bins = [ypar for ypar in parmat.ypars if ypar not in exclude_pars]
    pars = [xpar for xpar in parmat.xpars if xpar not in exclude_pars]
    nbins, ngroups = len(bins), len(pargroups)
    if "HTOT" in bins: nbins = len(bins) - 3

    if errors != None:
        nplots, ypads_scale = ngroups + 1, ngroups + 1.25
    else:
        nplots, ypads_scale = ngroups, ngroups + 1
    print(
        "number of bin : {0}\nnumber of parameters {1}\nnumber of parameter groups {2}".format(
            nbins, len(pars), ngroups
        )
    )

    # l_canv,b_canv = int(10*(1+math.pow(10.0/nbins,0.25)) * 10),int(nplots * 50)

    # l_canv,b_canv = int(6*nbins+10), int(ypads_scale * 30)
    l_canv, b_canv = 4 * int(6 * nbins + 10), 4 * int(
        ypads_scale * 60
    )  ## one at a time
    # l_canv,b_canv = int(12*nbins+10), int(ypads_scale * 60)
    ##l_canv,b_canv = 700, 400
    
    l_canv,b_canv = 900, 350
    b_top, b_bottom = b_canv - 35, -60  
    l_left, l_right = 12, l_canv - 40
    l_pad, b_pad = l_canv, (b_top - b_bottom) / (1.0 * ypads_scale)

    print(" (l,b) of canvas : ({0},{1}) ".format(l_canv, b_canv))
    print(" (top,bottom) of canvas : ({0},{1}) ".format(b_top, b_bottom))
    print(" (left,right) of canvas : ({0},{1}) ".format(l_left, l_right))
    print(" (length of pad, b_pad) of canvas : ({0},{1}) ".format(l_pad, b_pad))

    # (l,b) of canvas : (316,120)
    # (top,bottom) of canvas : (115,20)
    # (left,right) of canvas : (12,304)
    # (length of pad, b_pad) of canvas : (316,47.5)
    #    xlow,yhigh = l_left/(1.*l_canv), (b_top - (i+1)*b_pad)/(1.0*(b_canv))
    #    xhigh, ylow = l_right/(1.*l_canv), yhigh - b_pad/(1.0*(b_canv))

    if logscale:
        ymin, ymax = 1e-5, 0.9
    else:
        ymin, ymax = -1.0, 1.0
    if type(setymax)== float:
        ymax = setymax
    if type(setymin)== float:
        ymin = setymin

#    ymax=1.0
#    ymin=-0.0
    print("ymin,ymax : ", ymin, ymax)
    ROOT.gStyle.SetPaperSize(l_canv * 0.1, b_canv * 0.1)
    cst0 = ROOT.TCanvas("cst0", "cst0", l_canv, b_canv)
    cst0.SetCanvasSize(l_canv, b_canv)

    ytitlesize = 0.1  # * y_modifier
    ticklabelsize = 0.075  # * y_modifier
    ticklabeloffset = -0.009
    ytitleoffset = 0.225  # * y_modifier
    canvas_margin = {"r": 0.0, "l": 0.0, "b": 0.0, "t": -5.0}
    pad_margin = {"r":-50.0 / l_canv, "l": 80.0 / l_canv, "b": 25.0 / b_canv, "t": 0.0}
    # pad_margin = {"r":0.0,"l":0.0,"b":0.0,"t":0.0}
    l, l1 = {}, {}
    h_frame = {}
    histos = {}
    legend = {}
    graybox = {}
    spac = 5
    if len(pargroups[0]) > 6:
        spac = len(pargroups[0])
    #print(spac)
    # stack histogram per group
    for group, group_tag in zip(pargroups, range(len(pargroups))):
        group_name = "{0}_{1}".format("group", group_tag)
        # if len(group) > 9 : spac = len(group)
        h_frame[group_name] = ROOT.TH1D(
            "hs_{0}".format(group_tag), "", int(nbins), 0, int(spac * nbins)
        )
        histos[group_name], legend[group_name] = {}, {}

        for par_info, ipar in zip(group, range(len(group))):
            par = par_info[0]
            if par_info[2] == None:
                if ipar >= len(colors):
                    parcolor = ROOT.kGray + 2
                else:
                    parcolor = colors[ipar]
            else:
                parcolor = par_info[2]

            par_sq = "*".join([par,par])
            histos[group_name][par] = ROOT.TH1F(par, par, nbins * spac, 0, nbins * spac)
            histos[group_name][par_sq] = ROOT.TH1F(par_sq, par_sq, nbins * spac, 0, nbins * spac)

            histos[group_name][par_sq].SetLineColor(parcolor)
            histos[group_name][par_sq].SetLineWidth(3)
            histos[group_name][par].SetLineColor(parcolor)
            histos[group_name][par].SetLineWidth(1)
            histos[group_name][par].SetFillColor(parcolor)


            if square:
                histos[group_name][par].SetFillColorAlpha(parcolor,0.5)

            if par in xlabels.keys():
                label = xlabels[par]
            else:
                label = par


                
            for bin0, ibin in zip(bins, range(nbins)):
                if bin0 in ["HTOT","HAA","H4L"]:
                    continue
                h_frame[group_name].GetXaxis().SetBinLabel(ibin + 1, bin0)
                parval = par_info[1]
                linear_eftval, quadratic_eftval, smval = 0.0, 0.0, 1.0
                width_linear, width_quadratic = 0.0, 0.0
                if par in parmat.xpars:
                    if "Hyy" in bin0:
                        linear_eftval, smval = parmat.getElem(par, bin0) + parmat.getElem(par, "HAA"), 1.0
                    if "HZZ" in bin0:
                        linear_eftval, smval = parmat.getElem(par, bin0) + parmat.getElem(par, "H4L"), 1.0
                    width_linear = parmat.getElem(par, "HTOT")
                    if square:
                        if par_sq in parmat.xpars:
                            quadratic_eftval = parmat.getElem(par_sq, bin0)
                            width_quadratic = parmat.getElem(par_sq, "HTOT")
                        else:
                            print("no quadratic term for ", par)
                            
                if "SM" in parmat.xpars:
                    smval = parmat.getElem("SM", bin0)
                    if smval == 0.0:
                        parval, smval = 0.0, 1.0
                

                linear_impact = (1 + linear_eftval * parval)/(1 + width_linear * parval) - 1
                quad_impact = (1 + linear_eftval * parval + quadratic_eftval* parval * parval) / (1 + width_linear * parval + width_quadratic * parval * parval) - 1

                if logscale:
                    linear_impact = abs(linear_impact)

                #finalval = finalval  # /err #mat.getElem("error",bin0)
                #print(finalval)

                histos[group_name][par].SetBinContent(spac * ibin + ipar + 2 , linear_impact)
                histos[group_name][par_sq].SetBinContent(spac * ibin + ipar+ 2, quad_impact)
                
                h_frame[group_name].GetXaxis().SetLabelSize(0)
                h_frame[group_name].GetXaxis().SetNdivisions(nbins, 0)
                # hnew_frame.GetYaxis().SetLabelSize(0.09);
                # hnew_frame.GetYaxis().SetTitleSize(0.12);
                # hnew_frame.GetYaxis().SetTitleOffset(0.35);
                # hnew_frame.GetYaxis().SetTickSize(0.02) ;
                scale, err_ymin, err_ymax = (
                    2.14,
                    1e-3+abs(ymin) * 1.1 * 1e-4,
                    abs(ymax) * 2 * 1e-0,
                )
                # if ipar == 0:
                #     h_frame[group_name].Draw("hist")
                #     ## draw dashed lines to segregate bins
                # l1["errhisto"] = {"{0}_{1}".format("errhisto",k) : ROOT.TLine() for k in range(1,nbins)}
                # for line0, tline in l1["errhisto"].items():
                #     tline.SetLineWidth(1)
                #     tline.SetLineColor(ROOT.kGray)
                #     tline.SetLineStyle(ROOT.kDotted)
                #     for i in range(1,nbins-4):
                #         tline.DrawLine(ipar*spac,-1*1.14*0.02,ipar*spac,1.14-0.02)
            # h_frame[group_name].Add(histos[group_name][par])

            if logscale:
                legend[group_name][par] = ROOT.TLatex(
                    spac * (nbins + 0.1),
                    (ymax * 0.8) * pow((0.5), (ipar)),
                    " " + label + "=" + str(parval),
                )
            else:
                legend[group_name][par] = ROOT.TLatex(
                    spac *0.995*(nbins + 0.1),
                    -0.05 + ymax - ymax * 0.2 * (ipar),
                    " " + label + "=" + str(parval),
                )  # spac*(nbins+0.1), -0.2 + ymax - ymax*10.0*0.075*(ipar)," "+label +"=" + str(parval))
            legend[group_name][par].SetTextFont(72)
            # legend[group_name][par].SetIndiceSize(0.25)
            legend[group_name][par].SetTextSize(0.075)
            # lege[group_name][par]r].SetTextSizePixels(45)
            legend[group_name][par].SetTextColor(parcolor)
            legend[group_name][par].SetTextAlign(10)
    # filling in the histogram
    # prop*nbins,50*nplots)
    # nbins = 10
    # nplots = 50
    # nspace = 0.9 * 150
    # 100, 150
    ## common pars for pads
    cst0.SetTickx()
    cst0.SetLeftMargin(canvas_margin["l"])
    cst0.SetRightMargin(-100.0)
    cst0.SetBottomMargin(canvas_margin["b"])
    cst0.SetTopMargin(canvas_margin["t"])
    cst0.Divide(1, nplots, 0, 0)

    # first pad for the error bars
    if errmat != None:
        pad = cst0.cd(1)
        xlow, yhigh = l_left / (1.0 * l_canv), b_top / (1.0 * b_canv)
        xhigh, ylow = l_right / (1.0 * l_canv), yhigh - b_pad / (2.54 * (b_canv))
        pad.SetPad(xlow, ylow, xhigh, yhigh)
        pad.SetTopMargin(35.0 / l_canv)
        pad.SetBottomMargin(30.0 / l_canv)
        pad.SetLeftMargin(pad_margin["l"])
        pad.SetRightMargin(pad_margin["r"])
        if logscale:
            pad.SetLogy()
        # histogram for showing unc uncertainities
        errhisto = ROOT.TH1F("errhisto", "", nbins, 0, nbins)
        for bin0, ibin in zip(bins, range(nbins)):
            if bin0 in errmat.ypars:
                errhisto.Fill(ibin, errmat.getElem("error", bin0))
                #errhisto.Fill(ibin, 1e-06)
            else:
                print(
                    "measurement error not provided for {0}, plotting with placeholder error (0.1)".format(
                        bin0
                    )
                )
                errhisto.Fill(ibin, 1e-06)
            errhisto.SetBinError(ibin + 1, 1e-06)

        if logscale:
            scale, err_ymin, err_ymax = (
                2.14,
                (1.1 * 1e-4) * abs(ymin),
                (2 * 1e-0) * abs(ymax),
            )
            errhisto.GetYaxis().SetLabelOffset(-0.0125)
            errhisto.GetYaxis().SetTickSize(5*scale / (l_canv))
            errhisto.GetYaxis().SetTitleOffset(ytitleoffset * 0.4)
            errhisto.GetYaxis().SetTitleSize(ytitlesize * scale)
        else:
            scale, err_ymin, err_ymax = 2.14, -0.01, 1.1*abs(ymax)
            err_ymax = 1.1
            errhisto.GetYaxis().SetLabelOffset(0.01)
            # errhisto.GetYaxis().SetTickSize(0.05)
            errhisto.GetYaxis().SetTickLength(0.005)
            errhisto.GetYaxis().SetNdivisions(410)
            errhisto.GetYaxis().SetTitleOffset(ytitleoffset * 0.8)
            errhisto.GetYaxis().SetTitleSize(ytitlesize * scale)

        errhisto.SetFillColor(ROOT.kGray + 1)
        errhisto.SetFillStyle(3345)
        errhisto.SetYTitle("unc.")

        # errhisto.GetYaxis().SetTickSize(2./l_canv)#*math.sqrt(x_modifier*y_modifier)))
        errhisto.SetMinimum(err_ymin)
        errhisto.SetMaximum(err_ymax)
        errhisto.SetMarkerSize(0)
        errhisto.GetXaxis().SetNdivisions(nbins, 0)
        errhisto.GetXaxis().SetLabelSize(0)

        errhisto.GetYaxis().SetLabelSize(ticklabelsize * scale)
        # errhisto.GetYaxis().SetTextAlign(21)
        errhisto.SetLineWidth(5)
        errhisto.SetLineColor(ROOT.kWhite)
        errhisto.SetLineColor(ROOT.kGray + 1)
        errhisto.SetMarkerColor(ROOT.kGray + 1)
        errhisto.Draw("hist same")
        errhisto.Draw("e same")

        ibox = 0
        for bin0 in bins:
            if "gap" in bin0:
                ibox = ibox + 1
                name = "_".join(["err_hist", str(ibox)])
                graybox[name] = ROOT.TBox(
                    bins.index(bin0), err_ymin, bins.index(bin0) + 1, err_ymax
                )
                graybox[name].SetFillColor(19)
                graybox[name].Draw()

        ## draw solid lin3e at the top to avoid a thin boundary
        l["errhisto_top"] = ROOT.TLine(0.0, err_ymax, nbins, err_ymax)
        l["errhisto_top"].SetLineColor(ROOT.kBlack)
        l["errhisto_top"].SetLineWidth(4)
        l["errhisto_top"].Draw("same")
        ## draw solid line at the bottom to avoid a thin boundary
        l["errhisto_bottom"] = ROOT.TLine(0.0, err_ymin, nbins, err_ymin)
        l["errhisto_bottom"].SetLineColor(ROOT.kBlack)
        l["errhisto_bottom"].SetLineWidth(4)
        l["errhisto_bottom"].Draw("same")
        ## draw solid line at the left to avoid a thin boundary
        l["errhisto_left"] = ROOT.TLine(0.0, err_ymin, 0.0, err_ymax)
        l["errhisto_left"].SetLineColor(ROOT.kBlack)
        l["errhisto_left"].SetLineWidth(4)
        l["errhisto_left"].Draw("same")
        ## draw solid line at the right to avoid a thin boundary
        l["errhisto_right"] = ROOT.TLine(nbins, err_ymin, nbins, err_ymax)
        l["errhisto_right"].SetLineColor(ROOT.kBlack)
        l["errhisto_right"].SetLineWidth(4)
        l["errhisto_right"].Draw("same")


        # pad.Draw()
    # fill in the remaining pads with the eft impact plots

    for i in range(ngroups):
        ierr = nplots - ngroups
        pad = cst0.cd(i + 1 + ierr)
        if ierr:
            yhigh = ylow
        else:
            yhigh = (b_top - i  * b_pad) / (1.0 * b_canv)
        xlow = l_left / (1.0 * l_canv)
        xhigh, ylow = l_right / (1.0 * l_canv), 0.15 #yhigh - b_pad / (1.0 * (b_canv))
        print("xlow : {0:.2f}, ylow:{1:.2f}, xhigh:{2:.2f}, yhigh:{3:.2f}".format(xlow, ylow, xhigh, yhigh))
        pad.SetPad(xlow, ylow, xhigh, yhigh)
        #pad.SetMargin(0.02, 0.04,0.0,0.0)
        pad.SetTopMargin(pad_margin["t"])
        pad.SetBottomMargin(pad_margin["b"])
        pad.SetLeftMargin(pad_margin["l"])
        pad.SetRightMargin(pad_margin["r"])

        if logscale:
            pad.SetLogy()
        group_name = "group_{0}".format(i)
        # h_frame[group_name].Draw("nostackbhist")
        h_frame[group_name].GetXaxis().SetLabelSize(0)
        h_frame[group_name].GetXaxis().SetNdivisions(nbins, 0)

        h_frame[group_name].SetMinimum(ymin * 1.1)
        h_frame[group_name].SetMaximum(ymax * 1.1)

        ylabel = "#Delta #sigma(c_{i})/#sigma_{SM}"
        h_frame[group_name].GetYaxis().SetTitleSize(ytitlesize)
        h_frame[group_name].GetYaxis().SetTitle(ylabel)
        h_frame[group_name].GetYaxis().SetTitleOffset(1.75*ytitleoffset)
        h_frame[group_name].GetYaxis().SetTickLength(0.005)
        h_frame[group_name].GetYaxis().SetLabelSize(ticklabelsize)
#        h_frame[group_name].GetYaxis().SetLabelOffset(-0.0005)
        h_frame[group_name].GetYaxis().SetTickSize(
            5.0 / l_canv
        )  # *math.sqrt(x_modifier*y_modifier)))
        h_frame[group_name].Draw("hist")
#        for par in histos[group_name]:
#            histos[group_name][par].Draw("same hist ")

        for case in histos[group_name]:
            if not square and "*" in case:
                continue
            else: histos[group_name][case].Draw("same hist ")

        h_frame[group_name].Draw("axis same")
        h_frame[group_name].Draw("hist same")

        if logscale:
            ylabel = "|" + ylabel + "|"
        ## draw dashed lines to segregate bins
        l1[group_name] = {
            "{0}_{1}".format(group_name, k): ROOT.TLine(
                k * spac, ymin * 1.1, k * spac, ymax * 1.1
            )
            for k in range(1, nbins)
        }
        for tline in l1[group_name].values():
            tline.SetLineWidth(2)
            tline.SetLineColor(ROOT.kGray)
            tline.SetLineStyle(ROOT.kDotted)
            tline.Draw("same")
        ##
        ## draw solid line at the middle
        if ymin !=0:
            l["{0}_{1}".format(group_name, "mid")] = ROOT.TLine(0.0, 0.0, spac * nbins, 0.0)
            l["{0}_{1}".format(group_name, "mid")].SetLineColor(ROOT.kBlack)
            l["{0}_{1}".format(group_name, "mid")].SetLineWidth(4)
            #l["{0}_{1}".format(group_name, "mid")].Draw("same")
        ibox = 0
        for bin0 in bins:
            if "gap" in bin0:
                ibox = ibox + 1
                name = "_".join([group_name, str(ibox)])
                graybox[name] = ROOT.TBox(
                    spac * bins.index(bin0),
                    ymin * 1.1,
                    spac * bins.index(bin0) + spac,
                    ymax * 1.1,
                )
                graybox[name].SetFillColor(19)
                graybox[name].Draw()
        ## draw solid line at the top to avoid a thin boundary
        l["{0}_{1}".format(group_name, "up")] = ROOT.TLine(
            0.0, ymax * 1.1, spac * nbins, ymax * 1.1
        )
        l["{0}_{1}".format(group_name, "up")].SetLineColor(ROOT.kBlack)
        l["{0}_{1}".format(group_name, "up")].SetLineWidth(4)  
        l["{0}_{1}".format(group_name, "up")].Draw("same")

        ## draw solid line at the top to avoid a thin boundary
        l["{0}_{1}".format(group_name, "dn")] = ROOT.TLine(
            0.0, ymin * 1.1, spac * nbins, ymin * 1.1
        )
        l["{0}_{1}".format(group_name, "dn")].SetLineColor(ROOT.kBlack)
        l["{0}_{1}".format(group_name, "dn")].SetLineWidth(4) 
        l["{0}_{1}".format(group_name, "dn")].Draw("same")

        l["{0}_{1}".format(group_name, "left")] = ROOT.TLine(
            0.0, ymin * 1.1, 0.0, ymax * 1.1
        )
        l["{0}_{1}".format(group_name, "left")].SetLineColor(ROOT.kBlack)
        l["{0}_{1}".format(group_name, "left")].SetLineWidth(4)  # *math.sqrt(x_modifier*y_modifier)))
        l["{0}_{1}".format(group_name, "left")].Draw("same")

        l["{0}_{1}".format(group_name, "right")] = ROOT.TLine(
            spac*nbins, ymin * 1.1, spac*nbins, ymax * 1.1
        )
        l["{0}_{1}".format(group_name, "right")].SetLineColor(ROOT.kBlack)
        l["{0}_{1}".format(group_name, "right")].SetLineWidth(4)
        l["{0}_{1}".format(group_name, "right")].Draw("same")

        for par, ipar in zip(legend[group_name], range(len(legend[group_name]))):
            # legend[group_name][par].SetIndiceSize(0.01)
            legend[group_name][par].Draw()

#    for i in range(ngroups,ngroups+1):
#        ierr = nplots - ngroups
#        pad = cst0.cd(i + 1 + ierr)
#        if ierr:
#            yhigh = ylow
#        else:
#            yhigh = (b_top - i * b_pad) / (1.0 * b_canv)
#        xlow = l_left / (1.0 * l_canv)
#        xhigh, ylow = l_right / (1.0 * l_canv), yhigh - b_pad / (1.0 * (b_canv))
#        print("xlow : {0:.2f}, ylow:{1:.2f}, xhigh:{2:.2f}, yhigh:{3:.2f}".format(xlow, ylow, xhigh, yhigh))
    
    cst0.cd()
    t3 = ROOT.TLatex()
    t3.SetTextFont(73)
    t3.SetTextColor(ROOT.kBlack)
    t3.SetTextSize(20)
    t3.DrawLatexNDC((0.105 * l_canv) / l_canv, 0.94, "ATLAS")
    t3.SetTextFont(43)
    t3.SetTextSize(16)
    t3.DrawLatexNDC((60 + 0.115 * l_canv) / l_canv, 0.94, "Internal")
    t3.SetTextSize(10)
    t3.DrawLatexNDC((0.105 * l_canv) / l_canv, 0.90, "#sqrt{s} = 13 TeV, 139 fb^{-1}")

    t3.SetTextSize(12)
    t3.SetTextFont(45)


    # Now draw the xlabels on the bottom

    tex2 = ROOT.TLatex()
    tex2.SetTextFont(42)
    tex2.SetTextAlign(11)
    for ibin, binlabel in zip(range(nbins), bins):
        angle, ylabel_shift, label_size, xshift = (
            xticks_angle,
            -0.7*b_pad,
            # 0.02
            # 0.035
            0.025,
            0.09 * l_canv,
        )
        tex2.SetTextAngle(angle)
        tex2.SetTextSize(label_size)
        tex2.SetTextColor(ROOT.kBlack)
        #print(ylabels[binlabel])
        if "YY" in outfile.upper():
            xshift=0.12 * l_canv
        elif "ZZ" in outfile.upper():
            xshift=0.13 * l_canv
        tex2.DrawLatexNDC(
            (
                l_left
                + xshift
                + ibin * 1.0675 *0.94 * 0.96 * (l_right - l_left - 0.14 * l_canv) / (nbins)
#                + ibin * 1.025 *0.94 * 0.96 * (l_right - l_left - 0.14 * l_canv) / (nbins)
            )
            / l_canv,
            (b_bottom - ylabel_shift) / b_canv,
            ylabels[binlabel],
        )
 
    
    for axislabel,pos,ypos in zip(["p_{T}^{#gamma#gamma}", "p_{T}^{4l}"], [9, 23], [0.93,0.93]):
        print("axis label :{0}\n".format(axislabel))
        tex3 = ROOT.TLatex()
        tex3.SetTextFont(42)
        tex3.SetTextAlign(11)
        tex3.SetTextSize(0.045)
        tex3.SetTextColor(ROOT.kBlack)
        tex3.DrawLatexNDC(
            (l_left + xshift + pos * (l_right - l_left - 0.115 * l_canv) / (nbins))
            / l_canv,
            ypos,
            axislabel,)

    cst0.ForceUpdate()
    cst0.Draw()

    cst0.SaveAs(outfile)


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser("plot EFT parameterisation")
    parser.add_argument(
        "-i",
        "--input",
        type=str,
        dest="parmat_loc",
        help="input json file with parameterisation matrix",
        required=True,
        metavar="path/to/file",
    )
    parser.add_argument(
        "--errors",
        type=str,
        dest="errors_loc",
        help="input json file with measurement errors (to be used for showing measurement unc.)",
        required=False,
        metavar="path/to/file",
        default=None,
    )
    parser.add_argument(
        "--group",
        nargs="+",
        action="append",
        type=str,
        dest="pargroups",
        help="list of parame?er groups",
        default=[],
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="out",
        help="path to save parameterisation plot",
        required=False,
        default="out.eps",
    )
    parser.add_argument(
        "--exclude",
        type=str,
        dest="excludepars",
        help="pars to exclude.",
        metavar="POI",
        nargs="+",
        default=[],
    )
    parser.add_argument("--rotation", type=str, dest="rotation", default="")
    parser.add_argument(
        "--covariance",
        type=str,
        dest="covariance",
        help="covariance matrix of measurement bins",
        default="",
    )
    parser.add_argument(
        "--logscale",
        action="store_true",
        dest="logscale",
        help="log scale for the yaxis",
        default=False,
    )
    parser.add_argument(
        "--xticks-angle",
        type=float,
        dest="xticks_angle",
        help="angle to rotate xticks.",
        default=-45,
    )
    parser.add_argument(
        "--ymax", type=float, dest="ymax", help="maximum range of yaxis", default=False
    )
    parser.add_argument(
        "--ymin", type=float, dest="ymin", help="minimum range of yaxis", default=False
    )
    parser.add_argument(
        "--square", action="store_true", dest="square", help="show quad contribution", default=False
    )

    args = parser.parse_args()
    pargroups = parse_pargroups(args.pargroups)
    parmat = load_parameterisation(args.parmat_loc)
    if len(args.rotation):
        rotation = load_matrix(args.rotation)
        if "SM" in parmat.xpars:
            rotmat = get_padded_matrix(
                rotation,
                pad_name=rotation.name,
                pad_xpars=["SM"] + rotation.xpars,
                pad_ypars=["SM"] + rotation.ypars,
                pad_val=0.0,
            )
            rotmat.setElem("SM", "SM", 1.0)
            rotation = rotmat
        parmat = get_rotated_matrix(parmat, rotation)
    if args.errors_loc != None and args.covariance != "":
        print("ERROR: cannot specify both errors and covariance")
        exit(1)

    elif args.errors_loc != None:
        print("Loading errors {0}".format(args.errors_loc))
        errors = load_matrix(args.errors_loc)
        
    elif args.covariance != "":
        covariance = load_covariance(args.covariance)   
        errors_arr = [ [ math.sqrt(covariance.getElem(ypar,ypar))] for ypar in covariance.ypars]
        errors = matrix("errors", errors_arr, ["error"], covariance.ypars)

    else:
        errors = None

    plot_parameterisation(
        parmat,
        errors,
        pargroups,
        args.out,
        square=args.square,
        exclude_pars=args.excludepars,
        xticks_angle=args.xticks_angle,
        logscale=args.logscale,
        setymax=args.ymax,
        setymin=args.ymin,
    )
