#!/bin/env python
import numpy as np
import math
import pprint

from senstiEFT.matrix import load_matrix, load_parameterisation, load_covariance, matrix, load_central_values
from senstiEFT.helpers import get_rotated_matrix, get_matrix_in_newbasis
from senstiEFT.matrix_helpers import get_padded_matrix

## lines to make pyroot in batch mode
import sys
import yaml
import ROOT

ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)

def prepare_postfit_result(parmat, fitresultfile, output):
    eft_bestfit = load_central_values(fitresultfile)
    eft_covariance = load_covariance(fitresultfile)
    
    
    output_fitresult = {}
    measured, sm = [], []

    for observable in parmat.ypars:
        if "SM" in parmat.xpars:
            postfit_value = parmat["SM"]
            
        else:
            postfit_value = 1.0
            
        sm.append(float(postfit_value))
        for coefficient_term in eft_bestfit.ypars:
            if "*" not in coefficient_term: 
                par = coefficient_term
                postfit_value = postfit_value + parmat.getElem(par,observable) * eft_bestfit.getElem("measured",par)

            else:
                if len(par.split("*")) == 2:
                    par1, par2 = par.split("*")[0], par.split("*")[1]
                    postfit_value = postfit_value + parmat.getElem(par,observable) * eft_bestfit.getElem("measured",par1) * eft_bestfit.getElem("measured",par2)
                else: 
                    raise ValueError("Invalid coefficient term {0}".format(par))
        
        measured.append(float(postfit_value))
        
        # remove coefficient terms from parmat which are not in eft_bestfit
        parmat.reduce_matrix(xpars=eft_bestfit.ypars, ypars=parmat.ypars)
        covariance = get_matrix_in_newbasis(eft_covariance, parmat)
        output_fitresult["sm"] = sm
        output_fitresult["covariance"] = covariance.matrix.tolist()
        output_fitresult["measured"] = measured
        output_fitresult["names"] = parmat.ypars
        output_fitresult["measurement"] = "postfit of {0} propagating {1}".format(parmat.name, eft_covariance.name)

    with open(output, "w") as f:
        yaml.dump(output_fitresult, f, default_flow_style=None)

    print("Saved postfit result to {0}".format(output))
    
if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser("prepare back-propagated result of EFT parameterisation")
    parser.add_argument(
        "--parameterisation",
        type=str,
        dest="parmat",
        help="input yaml file with parameterisation",
        required=True,
        metavar="path/to/file",
    )
    parser.add_argument(
        "--eft-fitresult",
        type=str,
        dest="fitresult",
        help="input yaml file with EFT fit result",
        required=True,
        metavar="path/to/file",
        default=None,
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="out",
        help="path to save output fitresult",
        required=False,
        default="out.yaml",
    )
    parser.add_argument("--rotation", type=str, dest="rotation", default="")

    args = parser.parse_args()
    parmat = load_parameterisation(args.parmat)
    if len(args.rotation):
        rotation = load_matrix(args.rotation)
        if "SM" in parmat.xpars:
            rotmat = get_padded_matrix(
                rotation,
                pad_name=rotation.name,
                pad_xpars=["SM"] + rotation.xpars,
                pad_ypars=["SM"] + rotation.ypars,
                pad_val=0.0,
            )
            rotmat.setElem("SM", "SM", 1.0)
            rotation = rotmat
        parmat = get_rotated_matrix(parmat, rotation)

    prepare_postfit_result(
        parmat,
        args.fitresult,
        args.out
    )
