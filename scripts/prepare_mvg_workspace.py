#!/bin/env python
import itertools

from senstiEFT.matrix import (
    load_parameterisation,
    load_covariance,
    load_central_values,
    matrix,
    load_matrix,
)
from senstiEFT.senstivity_helpers import make_infomat
from senstiEFT.helpers import get_rotated_matrix
from senstiEFT.param_helpers import print_expr, prepare_expr, rotation_expr
from senstiEFT.matrix_helpers import (
    add_list_of_matrices,
    get_padded_matrix,
    print_stddev,
    make_combined_mat,
)
import numpy as np
import math
import ROOT

_nodel = []




def nodel(something):
    """mark an object as undeletable for the garbage collector"""
    _nodel.append(something)

def rotation_expr(mat, xpar):
    expr = ""
    for ypar,i in zip(mat.ypars,range(len(mat.ypars))):
        val = mat.getElem(xpar,ypar)
        absval = abs(val)
        if val >= 0.0 : sign = " + "
        else: sign = " - "
        expr = expr + " {0} {1}*@{2}".format(sign,absval,i)

    parlist = ",".join(mat.ypars)
    return "expr::{0}('{1}',{2})".format(xpar, expr, parlist)



def make_mvg(covmat,cenval,parmat=None,outfile="out.root",rotation="",dataname="combData",wsname="combWS",verbose=False,add_smpdf=False):
    # prepare covariance in ROOT
    # print(cenval.xpars)
    # define basis here : do rotation if given, else identity is used

    

#        rotmat.printMeta()

#        parmat_rot = get_rotated_matrix(parmat, rotmat, "rotated_param")
#        parmat = parmat_rot

    ws = ROOT.RooWorkspace(wsname)
    poisset = ROOT.RooArgSet()

    # rotated basis as POIs

    args.exclude = list(itertools.chain(*args.exclude))    
    if verbose: print("Created POIs for rotated basis")
    if parmat != None:
        if args.rotation != "":
            rotmat = load_matrix(args.rotation)
        else:
            xpars = [x for x in parmat.xpars if "*" not in x if x != "SM"]
            n = len(xpars) 
            identity = np.zeros((n,n), int)
            np.fill_diagonal(identity, 1)
            rotmat = matrix("rotation",identity, xpars=xpars, ypars=xpars)
            
        for ypar in rotmat.ypars:
            if verbose: print(ypar)
            ws.factory("{0}[0,-1e6,1e6]".format(ypar))
            ws.var(ypar).setError(0.0001)
            poisset.add(ws.var(ypar))

        if len(args.exclude) > 0:
            reduced_ypars = [ypar for ypar in parmat.ypars if ypar not in args.exclude]
            print(parmat.ypars)
            parmat.reduce_matrix(xpars=parmat.xpars, ypars=reduced_ypars)
            covmat.reduce_matrix(xpars=reduced_ypars, ypars=reduced_ypars)
            cenval.reduce_matrix(xpars=cenval.xpars, ypars=reduced_ypars)
            print(parmat.ypars)
            
        for xpar in rotmat.xpars:
            ws.factory(rotation_expr(rotmat,xpar))
            if verbose:
                print("Creating POI : {0}".format(xpar))

        if verbose: print("Created Rotated basis in observabls of warsaw basis")

        for ypar in parmat.ypars:
            ws.factory(prepare_expr(parmat,ypar))

            if verbose:
                print("Creating expression for {0}".format(ypar))
                print_expr(parmat, ypar)
        if verbose: print("Created parameterisation in observabls of warsaw basis")

    else:
        if len(args.exclude) > 0:
            reduced_ypars = [ypar for ypar in covmat.ypars if ypar not in args.exclude]
            covmat.reduce_matrix(xpars=reduced_ypars, ypars=reduced_ypars)
            cenval.reduce_matrix(xpars=cenval.xpars, ypars=reduced_ypars)

        for ypar in covmat.ypars:
            yname = "{0}_new".format(ypar)
            ws.factory("{0}[{1},-1e6,1e6]".format(yname,cenval.getElem("measured",ypar)))
            if verbose: print(yname)
            ws.var(yname).setError(0.1)
            poisset.add(ws.var(yname))

            if verbose:
                print("Creating POI :{0}".format(ypar))

    n = len(covmat.matrix)
    mat = ROOT.TMatrixDSym(n)
    for poi1, i in zip(covmat.xpars, range(n)):
        for poi2, j in zip(covmat.xpars, range(n)):
            mat[i][j] = covmat.getElem(poi1, poi2)

    nodel(mat)

    measpois_ral, valpois_ral, eftpois_ral = ROOT.RooArgList(),ROOT.RooArgList(),ROOT.RooArgList()
    measpois_ras, valpois_ras, eftpois_ras = ROOT.RooArgSet(),ROOT.RooArgSet(),ROOT.RooArgSet()

    # prepare POIs
    for poipar, cenvalpar in zip(covmat.ypars, cenval.ypars):
        if cenvalpar == poipar:
            cenvalparname = "cenval_{0}".format(poipar)
        else:
            cenvalparname = cenvalpar

        if poipar not in covmat.ypars:
            print("{0} not in parmat".format(poipar))
            yname = "{0}_new".format(poipar)
            ws.factory("{0}[1]".format(yname))
            ws.var(yname).setConstant(True)

        cenvar = ROOT.RooRealVar(cenvalparname, cenvalparname, cenval.getElem("measured", cenvalpar))
        cenvar.setConstant(True)

        nodel(cenvar)
        if verbose: print(poipar)
        measpois_ral.add(ws.obj(poipar+"_new"))
        measpois_ras.add(ws.obj(poipar+"_new"))
        valpois_ral.add(cenvar)
        valpois_ras.add(cenvar)

    mvgpdf = ROOT.RooMultiVarGaussian("mvg", "mvg", measpois_ral, valpois_ral, mat)
    pdf = mvgpdf
    #nodel(mvgpdf)
    #nodel(pdf)

    if add_smpdf:
        mat_smpdf = ROOT.TMatrixDSym(n)
        for poi1, i in zip(covmat.xpars, range(n)):
            for poi2, j in zip(covmat.xpars, range(n)):
                if i==j: 
                    mat_smpdf[i][j] = 10000*covmat.getElem(poi1, poi2)
                else: 
                    mat_smpdf[i][j] = 0.0

        nodel(mat_smpdf)

        measpois_ral_smpdf, valpois_ral_smpdf, eftpois_ral_smpdf = ROOT.RooArgList(),ROOT.RooArgList(),ROOT.RooArgList()

        for poipar in covmat.ypars:
            cenvalparname = "cenval_{0}_smpdf".format(poipar)
            cenvar_smpdf = ROOT.RooRealVar(cenvalparname, cenvalparname, 1.0)
            cenvar_smpdf.setConstant(True)
            nodel(cenvar_smpdf)
            valpois_ral_smpdf.add(cenvar_smpdf)
            valpois_ras.add(cenvar_smpdf)
                       
        mvgpdf_smpdf = ROOT.RooMultiVarGaussian("mvg_smpdf", "mvg_smpdf", measpois_ral, valpois_ral_smpdf,mat_smpdf)
        nodel(mvgpdf_smpdf)
        nodel(mvgpdf)
        pdflist = ROOT.RooArgList()
        pdflist.add(mvgpdf_smpdf)
        pdflist.add(mvgpdf)
        nodel(pdflist)
        const = ROOT.RooRealVar("const", "const", 0.5)
        pdf = ROOT.RooRealSumPdf("sum_pdf", "sum_pdf", mvgpdf_smpdf, mvgpdf, const)      
    
    dat = ROOT.RooDataSet(dataname, dataname, valpois_ras)
    dat.add(valpois_ras)

    mc = ROOT.RooStats.ModelConfig("ModelConfig", ws)
    mc.SetPdf(pdf)
    mc.SetParametersOfInterest(poisset)
    mc.SetSnapshot(measpois_ras)
    mc.SetObservables(valpois_ras)
    mc.SetGlobalObservables(valpois_ras)
    getattr(ws, "import")(mc)
    getattr(ws, "import")(dat)

    print("writing workspace to " + outfile)
    ws.writeToFile(outfile, True)


def isMeasurement(input):
    n_input = len(input)
    if n_input == 2:
        return "measurement"
    elif n_input == 3:
        return "eft"


def make_combined_mvg(args):
    input_types = [isMeasurement(x) for x in args.input]
    if input_types == ["measurement" for x in args.input]:
        print("Preparing measurement MVG ")
        parmat = None

    elif input_types == ["eft" for x in args.input]:
        print("Preparing EFT MVG")
        parmats = [load_parameterisation(x[1]) for x in args.input]
        parmat = make_combined_mat(parmats)

    covmats = [load_covariance(x[-1]) for x in args.input]
    cenvals = [load_central_values(x[-1], args.expected) for x in args.input]
    covmat, cenval = make_combined_mat(covmats), make_combined_mat(cenvals)

    if args.exclude_cross_terms:
        if parmat is not None:
            cross_terms = [x for x in parmat.xpars if "*" in x and x.split("*")[0] != x.split("*")[1]]
            reduced_terms = [x for x in parmat.xpars if x not in cross_terms]
            parmat.reduce_matrix(xpars=reduced_terms, ypars=parmat.ypars)
            
    if args.exclude_square_terms:
        if parmat is not None:
            square_terms = [x for x in parmat.xpars if "*" in x and x.split("*")[0] == x.split("*")[1]]
            reduced_terms = [x for x in parmat.xpars if x not in square_terms]
            parmat.reduce_matrix(xpars=reduced_terms, ypars=parmat.ypars)
        
    make_mvg(
        covmat,
        cenval,
        parmat,
        args.output,
        rotation=args.rotation,
        dataname=args.data,
        wsname=args.ws,
        verbose=args.verbose,
        add_smpdf=args.smpdf
    )


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser("create multi-variate gaussian RooWorkspace")
    parser.add_argument(
        "-i",
        "--input",
        action="append",
        nargs="+",
        help="files with input information",
        required=True,
    )
    parser.add_argument("-o", "--output", help="output ROOT file", required=True)
    parser.add_argument(
        "--pois", nargs="+", type=str, dest="pois", help="list of POI names", default=[]
    )
    parser.add_argument(
        "--workspace",
        type=str,
        dest="ws",
        help="name of output workspace",
        required=False,
        default="combWS",
    )
    parser.add_argument(
        "--expected",
        action="store_true",
        help="create an expected workspace",
        required=False,
    )
    parser.add_argument(
        "--data",
        type=str,
        help="name of dataset in RooFit workspace",
        required=False,
        default="combData",
    )
    parser.add_argument(
        "--rotation",
        type=str,
        help="name of dataset in RooFit workspace",
        required=False,
        default="",
    )
    parser.add_argument(
        "--verbose",
        action="store_true",
        dest="verbose",
        help="verbose mode for workspace preparation",
        required=False,
        default=False,
    )
    parser.add_argument(
        "--add-sm-pdf",
        action="store_true",
        dest="smpdf",
        help="add a small non-zero value for co-ordinate of the pdf at SM, avoids numerical issues",
        required=False,
        default=False,
    )
    parser.add_argument(
        "--exclude-observables",
        nargs="+",
        dest="exclude",
        action="append",
        help="exclude observables from the fit",
        required=False,
        default=[]
    )
    parser.add_argument(
        "--exclude-square-terms",
        action="store_true",
        dest="exclude_square_terms",
        help="exclude square term in the parameterisation",
        required=False,
        default=False
    )
    parser.add_argument(
        "--exclude-cross-terms",
        action="store_true",
        dest="exclude_cross_terms",
        help="exclude cross term in the parameterisation",
        required=False,
        default=False
    )

    args = parser.parse_args()
    make_combined_mvg(args)
