#!/bin/env python
import math
from senstiEFT.matrix import load_matrix
from senstiEFT.plotter import plotmatrix
from senstiEFT.helpers import getSensitivity, correlation_from_covariance
import json
import numpy as np

def write_json(covmat,nameout):

    json_dict={
    "MLE": {
        "minimizer": 0,
        "nll":0,
        "parameters": []
        },
    "scans": 0,
    "stamp": 0
}

    for xpar in covmat.xpars:
        variance = covmat.getElem(xpar, xpar)
        par_dict={
            "err":np.sqrt(variance),
            "name":xpar,
            "val":0
        }
        json_dict["MLE"]["parameters"].append(par_dict)
    print("Saving json file with uncertainty of the fit basis.")
    with open(nameout,"w") as f:
        json.dump(json_dict,f,indent=5)

def estimate_covariance(args):
    infomat = load_matrix(args.info)
    transmat = load_matrix(args.transmat)
    print(
        "\nEstimating covariance using information matrix ({0}) and applying basis transformation ({1})".format(
            infomat.name, transmat.name
        )
    )
    print("The parameters in the rotated space are\n")
    for par in transmat.ypars:
        if par in args.pois:
            print("---> {0}".format(par))
        else:
            print("     {0}".format(par))
    if len(args.pois) == 0:
        pars = load_matrix(args.transmat).ypars
    else:
        pars = args.pois
    covmat = getSensitivity(infomat, transmat, pars,diag_info=False)
    covmat_drop_corr = getSensitivity(infomat, transmat, pars,diag_info=True)

    maxlen = max([len(xpar) for xpar in covmat.xpars])
    print(
        "\n These are the unc. estimates for the {0} parameters with correlations (w.o. correlations),\n".format(
            len(args.pois)
        )
    )
    for xpar in covmat.xpars:
        variance = covmat.getElem(xpar, xpar)
        variance_wo_correlation = covmat_drop_corr.getElem(xpar, xpar)
        print("{0} : {1:.4f} ({2:.4f})".format(xpar.ljust(maxlen + 2), math.sqrt(variance), math.sqrt(variance_wo_correlation)))
    if args.result:
        write_json(covmat,args.result)

    corrmat = covmat
    if len(args.savecorr):
#        covmat.save_matrix("covariance_{0}.yaml".format(args.savecorr.replace(".pdf", "")))
        covmat.matrix = np.linalg.inv(covmat.matrix)
#        covmat.save_matrix("info_{0}.yaml".format(args.savecorr.replace(".pdf", "")))
        covmat.matrix = np.linalg.inv(covmat.matrix)

    corrmat.matrix = correlation_from_covariance(covmat.matrix)

    xpars = corrmat.xpars
    max0, elems = 0, []
    for xpar1 in xpars:
        for xpar2 in xpars[xpars.index(xpar1) + 1 :]:
            corr = abs(corrmat.getElem(xpar1, xpar2))
            if corr >= max0:
                max0 = corr
                elems = [xpar1, xpar2]

    print(
        "\n The largest correlation is {0:.4f} between {1} and {2}".format(
            max0, elems[0], elems[1]
        )
    )
    
    if len(args.save_reduced_basis):
        transmat.reduce_matrix(xpars=transmat.xpars, ypars=args.pois)
        transmat.save_matrix(args.save_reduced_basis)
        
    if len(args.savecorr):
        #corrmat.save_matrix("correlation_{0}.yaml".format(args.savecorr.replace(".pdf", "")))
        if len(xpars) > 25:
            fontsize = 7
        else:
            fontsize = 10
        plotmatrix(
            corrmat, savepathfile=args.savecorr, vmax=1.0, vmin=-1.0, fontsize=fontsize
        )

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(
        "estimate covariance in different bases starting from information matrix"
    )
    parser.add_argument(
        "-i",
        "--info-matrix",
        type=str,
        dest="info",
        help="input json file with info in one basis",
        required=True,
        metavar="path/to/file.json",
    )
    parser.add_argument(
        "--transformation-matrix",
        type=str,
        dest="transmat",
        help="transformation matrix for basis",
        required=True,
        default="out.json",
    )
    parser.add_argument(
        "--uncertainity-threshold",
        type=float,
        dest="threshold",
        help="thereshold for unc.",
        required=False,
        default=1e5,
    )
    parser.add_argument(
        "--save-correlation",
        type=str,
        dest="savecorr",
        help="filename to plot the correlation",
        required=False,
        default="",
    )
    parser.add_argument(
        "--pois",
        type=str,
        dest="pois",
        help="POIs to include for covariance estimate",
        metavar="POI",
        nargs="+",
        default=[],
    )
    parser.add_argument(
        "--write-result",
        type=str,
        dest="result",
        help="output name of a json with expected covariance of the pois (to be fed to the plotpois script)",

    )
    parser.add_argument(
        "--diag-info",
        action="store_true",
        dest="diag_info",
        help="drop diagonal elements in the information matrix",

    )
    parser.add_argument(
        "--save-reduced-basis",
        dest="save_reduced_basis",
        default="",
        type=str,
        help="save the reduced basis in a yaml file"
    )
    

    args = parser.parse_args()
    estimate_covariance(args)
