#!/bin/env python
import os
warsaw=['cQu8', 'cQu1', 'ceHRe22', 'cQj38', 'cG', 'cHj1', 'cHG', 'cHd', 'cQj11', 'cuHRe', 'ctu1', 'cQd8', 
'cHe22', 'cHWB', 'cHt', 'cHe11', 'cQj18', 'ctu8', 'cbHRe', 'cHl322', 'cHl122', 'cHu', 'ceHRe33', 'cHj3', 
'cHbox', 'cHl111', 'ctBRe', 'ctGRe', 'ctd8', 'cQd1', 'ctHRe', 'cQj31', 'cHl333', 'ctj1', 'ctj8', 'ctWRe', 
'cHQ1', 'cHB', 'cHl133', 'cHDD', 'cll1221', 'cHQ3', 'cHl311', 'cHW', 'cjj31']

for i in warsaw:
    os.system("python scripts/estimate_covariance.py --info-matrix data/information_matrix/info_with_VHcc.json --transformation-matrix data/parameterisation/rotation_warsaw.json --pois {0}   --write-fake-fitresults json_warsaw/exp_cov_warsaw_{0}.json &".format(i))