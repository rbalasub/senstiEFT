import ROOT
import math
import sys
import numpy as np
from senstiEFT.matrix import matrix
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import inv

def dict_to_mat(dict0,name=""):
    xpars = []
    ypars = []
    for obs in dict0:
        ypars.append(obs)
        for par in dict0[obs]:
            xpars.append(par)

    mat = []           
    xpars = list(set(xpars))
    for ypar in ypars:
        tmp = []
        for xpar in xpars:
            if xpar in dict0[ypar]:
                tmp.append(dict0[ypar][xpar])
            else:
                tmp.append(0.0)
        mat.append(tmp)
    return matrix(name, mat, xpars=xpars, ypars=ypars)


def invert_arr_scipy(inarr):
    if len(inarr) == 1:
        return [1 / inarr[0]]
    invarr = inv(csc_matrix(inarr))
    return invarr.todense()


def invert_arr_numpy(inarr):
    if len(inarr) == 1:
        return [1 / inarr[0]]
    invarr = np.linalg.inv(inarr)
    return invarr


# helper functions
def flatten(t):
    return [item for sublist in t for item in sublist]


def get_number_of_digits(n):
    if n > 0:
        digits = int(math.log10(n)) + 1
    elif n == 0:
        digits = 1
    else:
        digits = int(math.log10(-n)) + 2  # +1 if you don't count the '-'
    return digits


# given a TMatrix return an array
def tmat2arr(inmat):
    arr = []
    for icol in range(0, inmat.GetNcols()):
        tmp = []
        for irow in range(0, inmat.GetNrows()):
            tmp.append(inmat[irow][icol])
        arr.append(tmp)
    return arr


def correlation_from_covariance(covariance):
    import numpy as np

    v = np.sqrt(np.diag(covariance))
    outer_v = np.outer(v, v)
    correlation = covariance / outer_v
    correlation[covariance == 0] = 0
    return correlation


def getdiagmat(inarr):
    dim = len(inarr)
    diagmat = []
    for i1 in range(0, dim):
        tmparr = []
        for i2 in range(0, dim):
            if i1 == i2:
                tmparr.append(inarr[i1])
            else:
                tmparr.append(0.0)
        diagmat.append(tmparr)
    return diagmat

def getbestfit(file0, fitresname, pois):
    from ROOT import TFile, RooArgList

    file0 = TFile.Open(file0)
    fitres = file0.Get(fitresname)
    poiList = ROOT.RooArgList()

    poivals = {}
    for poi in pois:
        poifound = fitres.floatParsFinal().find(poi)
        if poifound:
            poivals[poifound.GetName()] = poifound.getVal()
            poiList.add(poifound)
        else:
            print(poi + "- Not found in RooFitResult !")

    file0.Close()

    return poivals

def get_conditional_covariance(fitres, pois):
    covmat = fitres.covarianceMatrix()
    covarr = [[ covmat[irow][icol] for icol in range(covmat.GetNcols())] for irow in range(covmat.GetNrows())] 
    pars = [par.GetName() for par in fitres.floatParsFinal()]
    corarr = correlation_from_covariance(covarr)
    idxs = range(len(corarr))
    high_corr = [(i,j, pars[i], pars[j]) for i in idxs for j in idxs if abs(corarr[i][j])>=0.95 and i!=j ]
    high_corr_pars = list(set([res[2] for res in high_corr]))
    print(high_corr_pars)
    redpars_for_inversion = ROOT.RooArgList()
    for par in fitres.floatParsFinal():
        if par.GetName() in high_corr_pars:
            print("skipping {0}".format(par))
            continue
            
        else:
            redpars_for_inversion.add(par)

    covmat_red = fitres.reducedCovarianceMatrix(redpars_for_inversion) 
    covarr_red = [[covmat_red[irow][icol] for icol in range(covmat_red.GetNcols())] for irow in range(covmat_red.GetNrows())] 
    hessearr = invert_arr_scipy(covarr_red)
    poisidx = [pars.index(x) for x in parsidx if x in pois]
    red_hesse = [[ hessearr[idx1][idx2] for idx1 in poisidx] for idx2 in poisidx]
    red_cov = invert_arr_scipy(red_hesse)

    return red_cov

def getcovmat(file0, fitresname, pois, no_systematics):
    from ROOT import TFile, RooArgList

    file0 = TFile.Open(file0)
    fitres = file0.Get(fitresname)
    poiList = ROOT.RooArgList()

    for poi in pois:
        poifound = fitres.floatParsFinal().find(poi)
        if poifound:
            poiList.add(poifound)
        else:
            print(poi + "- Not found in RooFitResult !")
    if no_systematics:
        mat = get_conditional_covariance(fitres,pois)
        covarr = mat
    else: 
        mat = fitres.reducedCovarianceMatrix(poiList)
        covarr = [[ mat[irow][icol] for icol in range(mat.GetNcols())] for irow in range(mat.GetNrows())]
    file0.Close()

    cov = matrix("covariance", covarr, xpars = pois, ypars = pois)

    return cov



def getpois(file0, fitresname, poi_expr):
    from ROOT import TFile, RooArgList
    import re

    file0 = TFile.Open(file0)
    fitres = file0.Get(fitresname)
    pois = []
    poi_expr_regex = re.compile(poi_expr)

    for par in fitres.floatParsFinal():
        if poi_expr_regex.search(par.GetName()):
            pois.append(par.GetName())
    print("Found {0} Parameters of Interest".format(len(pois)))
    for poi in pois: print(poi)
    return pois



def getfitresult(measurement_name, file0, fitresname, pois = [], poi_expr = "", smvals = [], no_systematics = False):

   if len(pois) == 0: 
      pois = getpois(file0, fitresname, poi_expr)

   cov = getcovmat(file0, fitresname, pois, no_systematics)
   bestfit = getbestfit(file0, fitresname, pois)
   if len(smvals) == 0 :
      smvals = [1.0 for x in cov.xpars]

   measvals = [float(bestfit[poi]) for poi in cov.xpars]
   fitres = {"measurement" : measurement_name,
             "covariance"  : [[float(cov.getElem(x,y)) for x in cov.xpars] for y in cov.ypars],
             "sm"          : smvals,
             "names"       : [x for x in cov.xpars],
             "measured"    : measvals}

   return fitres
   


def getarr(mat):
    ncols, nrows = mat.GetNcols(), mat.GetNrows()
    return [[mat[i][j] for i in range(nrows)] for j in range(ncols)]


def get_rotated_matrix(inimat, rotmat, name=""):
    import numpy as np
    if name == "":
        name = "{0} in {1} basis".format(inimat.name, rotmat.name)
    inimat.reorder_matrix(xpars=rotmat.xpars, ypars=inimat.ypars)
    matarr = np.matmul(inimat.matrix, np.transpose(rotmat.matrix))
    rotated_matrix = matrix(name, matarr, ypars=inimat.ypars, xpars=rotmat.ypars)
    rotated_matrix.labels = {}
    rotated_matrix.labels.update(rotmat.labels)
    rotated_matrix.labels.update(inimat.labels)
    return rotated_matrix


def get_matrix_in_newbasis(inimat, rotmat, name=""):
    if name == "":
        name = "{0} in {1} basis".format(inimat.name, rotmat.name)
    inimat.reorder_matrix(xpars=rotmat.xpars, ypars=rotmat.xpars)
    matarr = np.matmul(
        np.matmul(rotmat.matrix, inimat.matrix), np.transpose(rotmat.matrix)
    )
    rotated_matrix = matrix(name, matarr, ypars=rotmat.ypars, xpars=rotmat.ypars)
    return rotated_matrix


def diagmat(inmat):
    diagmat = []
    for i in range(0, len(inmat)):
        tmparr = []
        for j in range(0, len(inmat)):
            if i == j:
                tmparr.append(inmat[i][j])
            else:
                tmparr.append(0.0)
        diagmat.append(tmparr)
    return diagmat


def shrink_matrix(mat, redxpars=[], redypars=[], name=""):
    if name == "":
        name = "shrunk_{0}".format(mat.name)
    shrunk_arr = [[mat.getElem(xpar, ypar) for xpar in redxpars] for ypar in redypars]
    shrunk_mat = matrix(name, shrunk_arr, xpars=redxpars, ypars=redypars)
    return shrunk_mat


def getSensitivity(infomat, transmat=None, reduced_pars=[], verbose=False, name="",diag_info=False):
    if name == "":
        name = "reduced_covariance_of_{0}_for_{1}".format(
            infomat.name, ",".join(reduced_pars)
        )
        
    heft_newpar = infomat
    
    if transmat != None:
        if not set(reduced_pars).issubset(transmat.ypars):
            print("Please provide parameters that are reducable")
            exit
        heft_newpar = get_matrix_in_newbasis(infomat, transmat)
        if diag_info:
            heft_newpar.matrix = np.diag(np.diag(heft_newpar.matrix))
    shrunk_mat = shrink_matrix(heft_newpar, reduced_pars, reduced_pars)
    shrunk_pars = shrunk_mat.xpars
    try:
        covarr = invert_arr_scipy(shrunk_mat.matrix)

    except:
        print("Oops!", sys.exc_info()[0], "occurred.")
        print(
            "determinant of matrix {0} is {1}".format(
                shrunk_mat.name, np.linalg.det(shrunk_mat.matrix)
            )
        )
        sys.exit()

    covmat = matrix(name, covarr, xpars=reduced_pars)

    return covmat


# dot product of two vectors
def dotprod(vec1, vec2, norm=False):
    import math

    if len(vec1) == len(vec2):
        val, norm1, norm2 = 0.0, 0.0, 0.0
        for i in range(0, len(vec1)):
            val = val + vec1[i] * vec2[i]
            norm1 = norm1 + vec1[i] * vec1[i]
            norm2 = norm2 + vec2[i] * vec2[i]
        if norm:
            return val / math.sqrt(abs(norm1 * norm2))
        else:
            return val


# dot product of two vectors
def getProximMat(inMat, threshold=0.0):
    proximMat = []
    dim = len(inMat)
    for i1 in range(0, dim):
        arr = []
        for i2 in range(0, dim):
            val = dotprod(inMat[i1], inMat[i2], True)
            # required thresholding
            if threshold > 0.0:
                if abs(val) > threshold:
                    val = 1.0
                else:
                    val = 0.0
            arr.append(val)
        proximMat.append(arr)
    return proximMat


# dot product of two vectors
def halfDiag(inMat):
    halfDiag = []
    dim = len(inMat)
    for i1 in range(0, dim):
        arr = []
        for i2 in range(0, dim):
            if i2 >= i1:
                arr.append(inMat[i1][i2])
            else:
                arr.append(0.0)
        halfDiag.append(arr)
    return halfDiag


def getSubmatrix(inmat, directions, namelist):
    red_infoarr = []
    for ipar1 in namelist:
        tmparr = []
        for ipar2 in namelist:
            tmparr.append(inmat[directions.index(ipar1)][directions.index(ipar2)])
        red_infoarr.append(tmparr)
    return np.array(red_infoarr)


def dictToMat(valdict, allpars):
    transmat = []
    for par1 in sorted(valdict.keys()):
        tmparr = []
        for par2 in allpars:
            tmparr.append(valdict[par1][par2])
        transmat.append(tmparr)
    return transmat


def multmats(mat1, mat2):
    import numpy as np

    return np.matmul(np.matmul(np.transpose(mat2), mat1), mat2)


def getProximMat(inMat):
    proximMat = []
    dim = len(inMat)
    for i1 in range(0, dim):
        arr = []
        for i2 in range(0, dim):
            val = dotprod(inMat[i1], inMat[i2], True)
            arr.append(val)
        proximMat.append(arr)
    return proximMat


def printproximpars(mat, threshold):
    proximpars = getProximPars(mat, mat.xpars, threshold)
    for i in range(1, len(proximpars) + 1):
        print(sorted(proximpars["group_" + str(i)]))


# def getParDirs(mat, pars, threshold,plotProximMat=False):
#        pardirections,indipars = [],[]
#        proximpars = getProximPars(getProximMat(mat),pars,threshold)
#        if plotProximMat:
#            plotmatrix(getProximMat(mat),pars,pars,vmax=1.0,vmin=-1.0,vals=True,zero=False)
#        rempars = set(eftpars)
#        for igroup in proximpars:
#            rempars.difference_update(proximpars[igroup])
#            pardirections.append(sorted(proximpars[igroup]))
#        for par in rempars:
#            pardirections.append([par])
#        return pardirections


def getProximPars(proximmat, pars, threshold=0.9):
    proximpars = {}
    proximpars["group_1"], igroup = set(), 1
    proximpars["no_group"] = set()

    # loop over the pars
    for i1 in range(0, len(pars)):
        for i2 in range(i1 + 1, len(pars)):
            if abs(proximmat[i1][i2]) >= threshold:
                group = "group_" + str(igroup)
                if len(proximpars[group]) == 0:
                    proximpars[group].add(pars[i1])
                    proximpars[group].add(pars[i2])
                else:
                    if (pars[i1] in proximpars[group]) or (
                        pars[i2] in proximpars[group]
                    ):
                        proximpars[group].add(pars[i2])
                        proximpars[group].add(pars[i1])
                    elif (pars[i1] not in proximpars[group]) and (
                        pars[i2] not in proximpars[group]
                    ):
                        createNew = 0
                        for groupi in proximpars:
                            if (pars[i1] in proximpars[groupi]) or (
                                pars[i2] in proximpars[groupi]
                            ):
                                proximpars[groupi].add(pars[i2])
                                proximpars[groupi].add(pars[i1])
                            else:
                                createNew = createNew + 1
                        if createNew == len(proximpars):
                            igroup = igroup + 1
                            group = "group_" + str(igroup)
                            proximpars[group] = set()
                            proximpars[group].add(pars[i1])
                            proximpars[group].add(pars[i2])

        # else:
        #     proximpars["no_group"].add(pars[i1])
        #     proximpars["no_group"].add(pars[i2])

    return proximpars


def norm(mat, rows=True, cols=False):
    import numpy as np

    if cols:
        mat = np.transpose(mat)
    norm_mat = [[val / np.amax([abs(x) for x in row]) for val in row] for row in mat]
    if cols:
        norm_mat = np.transpose(norm_mat)
    return norm_mat


def remove_char(string, charlist):
    for char in charlist:
        string = string.replace(char, "")
    return string


def replace_elem(list0, inival, finalval):
    return [finalval if val == inival else val for val in list0]


def parse_formula(formula_string):

    formula_dict = {}
    formula_string = remove_char(formula_string, ["(", ")"])
    formula_parts = formula_string.split("+")  #  for part in term.split("*")]
    for term in formula_parts:
        term = str(term).split("*")
        if len(term) == 1:
            formula_dict["SM"] = float(term[0])
        else:
            term[0] = remove_char(term[0], ["[", "]"])
            formula_dict[term[0]] = float(term[1])

    return formula_dict


def norm_mat(mat):
    normmat = []
    for par in range(len(mat)):
        norm = 0.0
        for direc in range(len(mat[par])):
            norm = norm + mat[par][direc] * mat[par][direc]
        tmp_dir = [1.0 * direc / norm for direc in mat[par]]
        normmat.append(tmp_dir)

    return normmat
