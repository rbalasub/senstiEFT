import scipy
from scipy.sparse.linalg import eigs
import numpy as np
import math, re
from senstiEFT.matrix_helpers import (
    get_padded_matrix,
    add_list_of_matrices,
    get_const_pars,
)
from senstiEFT.matrix import matrix, listdiff
from senstiEFT.helpers import (
    get_rotated_matrix,
    getSensitivity,
    dictToMat,
    getSubmatrix,
    invert_arr_scipy,
    correlation_from_covariance,
)


def get_sign_direction(vec):
    maxval, minval = max(vec), min(vec)
    if abs(maxval) >= abs(minval):
        sign = 1.0
    else:
        sign = -1.0
    return sign


# get the transformation matrix as a dictionary
def getTransDict(evdir, allpars, pois, pardirections, threshold=0.0):
    transdict = {}
    for direc in evdir:
        print("{0}\n".format(direc))
        for ev in sorted(evdir[direc].keys()):
            if len(pardirections[direc]) == 1:
                evname = direc
            else:
                evname = direc + "_" + "{:02d}".format(int(ev))
            egval = evdir[direc][ev]["eigenval"]
            if egval != 0.0: errstr = "{0:.5f}".format(1/math.sqrt(abs(egval)))
            else: errstr = "N.A."
            print(" |-> {0} EV={1:.3f}, err={2}".format(evname,egval,errstr))
            parstring = ""
            transdict[evname] = {}
            total_sign = get_sign_direction(
                [val for par, val in evdir[direc][ev].items() if par != "eigenval"]
            )
            for par in evdir[direc][ev]:
                if par != "eigenval":
                    evdir[direc][ev][par] = total_sign * evdir[direc][ev][par]
                    sign = " + "
                    if evdir[direc][ev][par] < 0.0:
                        sign = " - "
                    if abs(evdir[direc][ev][par]) < threshold : continue
                    parstring = (
                        sign
                        + "{0:.3f}".format(abs(evdir[direc][ev][par]))
                        + "*"
                        + par
                        + parstring
                    )
                transdict[evname][par] = evdir[direc][ev][par]
            print("".join(["   ", parstring, "\n"]))
        print("\n")

    # flatten the par directions
    parsindirections = [par for pardir, pars in pardirections.items() for par in pars]
    rempars = [par for par in allpars if par not in parsindirections]
    for idir in transdict:
        for par in allpars:
            if par not in transdict[idir].keys():
                transdict[idir][par] = 0.0

    for par1 in rempars:
        transdict[par1] = {}
        for par2 in allpars:
            if par1 == par2:
                transdict[par1][par2] = 1.0
            else:
                transdict[par1][par2] = 0.0
    return transdict


# given the parameters, eigenvectors return a dictionarised form
def getEV(pars, eigenvals, eigenvecs, threshold=0.0, evprint=False):
    evdict, iev = {}, 0
    for ieigenval in range(len(eigenvals)):
        h = ""
        iev = iev + 1
        evdict["{:02d}".format(iev)] = {}
        parwgt = eigenvecs[:, ieigenval]

        # calculate normalisation for the thresholded column
        norm = 0.0

        for iparwgt in range(len(parwgt)):
            if abs(parwgt[iparwgt].real) > threshold:
                sign = " + "
                evdict["{:02d}".format(iev)][pars[iparwgt]] = parwgt[iparwgt].real
                evdict["{:02d}".format(iev)]["eigenval"] = eigenvals[ieigenval].real
                # normalisation defined as squared sum of non-zero contribution
                norm = norm + parwgt[iparwgt].real * parwgt[iparwgt].real

                if parwgt[iparwgt].real < 0:
                    sign = " - "
                h = (
                    h
                    + sign
                    + "{0:.4f}".format(abs(parwgt[iparwgt].real))
                    + " "
                    + pars[iparwgt]
                )
        if evprint:
            print(
                "{0:.4f}".format(eigenvals[ieigenval].real),
                " ",
                "{0:.4f}".format(1 / math.sqrt(abs(eigenvals[ieigenval].real))),
                "\n",
                h,
                "\n",
            )
    return evdict


def makedircEVs(heft, inidirections, pardirections, evthreshold=0.0, evprint=False):
    evdir = {}
    for gname, group in pardirections.items():
        redinfomat = getSubmatrix(heft, inidirections, group)
        egvals, egvecs = eigs(redinfomat, len(redinfomat))
        idx = egvals.argsort()[::-1]
        egvals = egvals[idx]
        egvecs = egvecs[:, idx]
        evdir[gname] = getEV(group, egvals, egvecs, evthreshold, evprint)
    return evdir


def print_evs(transdict):
    print(" # par , # 1/sqrt(EV)")
    print("[")
    for par in sorted(transdict.keys()):
        if transdict[par]["eigenval"] != 0.0:
            print(
                '"'
                + par
                + '", # {0:.3f}'.format(1 / math.sqrt(abs(transdict[par]["eigenval"])))
            )
    print("]")


def get_subgroup_evs(name, parameter_groups, eftpars, infomat):
    evdir = makedircEVs(np.array(infomat.matrix), eftpars, parameter_groups)
    ev_dict = getTransDict(evdir, eftpars, [], parameter_groups)
    ev_transmat = dictToMat(ev_dict, eftpars)
    signed_transmat = []
    for yparam in ev_transmat:
        sign = get_sign_direction(yparam)
        signed_transmat.append([sign * x for x in yparam])
    transmat = matrix(
        name, signed_transmat, xpars=eftpars, ypars=sorted(ev_dict.keys())
    )
    egvals = [
        ev_dict[par]["eigenval"]
        for par in transmat.ypars
        if "eigenval" in ev_dict[par].keys()
    ]
    return transmat, egvals


def make_infomat(name, covmat, parmat):

    parmat.reorder_matrix(parmat.xpars, covmat.ypars)
    try:
        invmat = invert_arr_scipy(covmat.matrix)
    except:
        print("Oops!", sys.exc_info()[0], "occurred.")
        print(
            "determinant of matrix {0} is {1}".format(
                mat.name, np.linalg.det(covmat.matrix)
            )
        )
        sys.exit()

    infoarr = np.array(np.matmul(np.transpose(parmat.matrix),np.matmul(invmat, parmat.matrix)))

    infomat = matrix(name, infoarr, xpars=parmat.xpars)

    return infomat


def get_new_info(mat_dicts, eftpars_to_split, transmat=None, debug=False):
    info_dict = {}
    for tag, mat in mat_dicts.items():
        parmat, covmat, group_meas = (
            mat["parameterisation"],
            mat["covariance"],
            mat["group_meas"],
        )

        if transmat != None:
            parmat = get_rotated_matrix(parmat, transmat)

        parts = {tag: re.compile(part) for tag, part in group_meas.items()}
        if debug:
            for par in parmat.ypars:
                found = False
                for partag, part in parts.items():
                    if part.search(par):
                        found = True
                        print(partag, par)
                if not found:
                    print("others", par)
        new_xpars = [
            "_".join([eftpar, partag]) if part.search(par) else eftpar
            for eftpar in eftpars_to_split
            for par in parmat.ypars
            for partag, part in parts.items()
        ]
#        print(new_xpars)
        padded_parmat = get_padded_matrix(
            parmat,
            "_".join(["pad", parmat.name] + list(set(new_xpars))),
            pad_xpars=list(set(new_xpars + parmat.xpars)),
            pad_ypars=parmat.ypars,
        )
        for partag, part in parts.items():
            for par in parmat.ypars:
                for eftpar in eftpars_to_split:
                    if part.search(par):
                        padded_parmat.setElem(
                            "_".join([eftpar, partag]), par, parmat.getElem(eftpar, par)
                        )
                        padded_parmat.setElem(
                            eftpar,
                            par,
                            padded_parmat.getElem(eftpar, par)
                            - padded_parmat.getElem("_".join([eftpar, partag]), par),
                        )

        new_parmat = padded_parmat
        #       if debug: new_parmat.save_matrix("{0}.json".format(new_parmat.name).replace(" ",""))
        info_dict[tag] = make_infomat(
            "_".join(["info", new_parmat.name]), covmat, new_parmat
        )

    # let's make the combined info
    all_infomats = list(info_dict.values())
    sum_infomats = add_list_of_matrices(all_infomats)

    return sum_infomats


def match_cases(parlist, caselist):
    parcase = {}
    for par in parlist:
        case0 = "others"
        for case in caselist:
            if case in par:
                case0 = case

        parcase[par] = case0

    return parcase


def get_fractional_contribution(
    infomat, redpars, par, cases, info_based=False, debug=False
):
    zero_pars = get_const_pars(infomat, xpars=True, ypars=False)
    keep_pars = []
    for evalpar in infomat.xpars:
        if par in evalpar:
            keep_pars.append(evalpar)

    parcase = match_cases(keep_pars, cases)

    redpars = redpars + keep_pars
    redpars = list(set(redpars))
    redpars = listdiff(redpars, zero_pars)
    if not info_based:
#        for par in redpars:
#            print(infomat.getElem(par, par))

        covmat = getSensitivity(infomat, reduced_pars=redpars)
        if np.linalg.det(correlation_from_covariance(covmat.matrix)) == 0:
            print(
                "{0} covariance matrix is not well defined, try changing the grouping".format(
                    covmat.name
                )
            )
            exit()
        #        if debug : covmat.save_matrix("covmat_{0}.json".format(infomat.name).replace(" ",""))
        result = {
            parcase[par]: 0.0 if par in zero_pars else 1 / covmat.getElem(par, par)
            for par in keep_pars
        }
    else:
        result = {parcase[par]: infomat.getElem(par, par) for par in keep_pars}
    sumval = np.sum(list(result.values()))
    result = {par: val / sumval for par, val in result.items()}

    return result
