import matplotlib
from matplotlib.colors import LogNorm
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.colors as mcolors
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import numpy as np
from senstiEFT.matrix import matrix
from senstiEFT.helpers import get_number_of_digits


def get_color_map(cmap_tag):

    Nlevs = 10000
    colors_rgb = {
        "blue": (34.0 / 256.0, 95.0 / 256.0, 204.0 / 256.0),
        "white": (1, 1, 1),
        "yellow": (245.0 / 256.0, 184.0 / 256.0, 66.0 / 256.0),
    }

    cmaps = {
        "blue_white": mcolors.LinearSegmentedColormap.from_list(
            name="blue_white", colors=[colors_rgb["white"], colors_rgb["blue"]], N=Nlevs
        ),
        "blue_white_yellow": mcolors.LinearSegmentedColormap.from_list(
            name="blue_white_yellow",
            colors=[colors_rgb["yellow"], colors_rgb["white"], colors_rgb["blue"]],
            N=Nlevs,
        ),
    }

    if cmap_tag not in cmaps.keys():
        print(
            "{0} option not available in {1}, passing the option over to matplotlib defaults".format(
                cmap_tag
            ),
            ",".join(list(cmaps.keys())),
        )
        return cmap_tag
    else:
        return cmaps[cmap_tag]


def prepare_colors(f, cases):
    colors = [
        "FF8C00",
        "B22222",
        "FF0000",
        "83CEEE",
        "541ae8",
        "838aee",
        "008000",
        "E6194B",
        "3CB44B",
        "FFE119",
        "4363D8",
        "F58231",
        "911EB4",
        "42D4F4",
        "F032E6",
        "BFEF45",
        "FABED4",
        "469990",
        "DCBEFF",
        "9A6324",
        "FFFAC8",
        "800000",
        "AAFFC3",
        "808000",
        "FFD8B1",
        "000075",
        "A9A9A9",
        "FFFFFF",
        "000000",
    ]
    for icase, color in zip(range(len(cases)), colors):
        f.write("\\definecolor{{color{0}}}{{HTML}}{{{1}}}\n".format(icase, color))


def write_barplot(outfile, cont_mat, colors=None,rotate=True):

    ytickcoords = [ypar for ypar in cont_mat.ypars]
    npars = len(ytickcoords)
    xticks, yticks = cont_mat.get_parlabels()
    yticklabels_str = ",".join(yticks)
    xticklabels_str = ",".join(xticks)
    with open(outfile, "w") as f:
        f.write("\\documentclass{standalone}\n")
        f.write("\\usepackage{amsmath,amssymb}\n")
        f.write("\\usepackage[dvipsnames]{xcolor}\n")
        f.write("\\usepackage{tikz}\n")
        f.write("\\usepackage{pgfplots}\n")
        f.write("\\usetikzlibrary{patterns}\n")
        f.write("\\usepackage[scaled=1]{helvet}\n")
        f.write("\\usepackage[helvet]{sfmath}\n")
        if not colors:
            prepare_colors(f, sorted(cont_mat.xpars))
        else:
            for icase, color in zip(range(len(cont_mat.xpars)), colors):
                f.write("\\definecolor{{color{0}}}{{HTML}}{{{1}}}\n".format(icase, color))
        f.write("\\begin{document}\n")
        f.write("\\centering\n")
        f.write("\\begin{tikzpicture}[font={\\fontfamily{qhv}\\selectfont}]\n")
        f.write("\\begin{axis}[\n")
        if not rotate:
            f.write("    xbar stacked,\n")
            f.write("    legend style={\n")
            f.write("        legend columns=2,\n")
    #        f.write("        at={{(0.0,-{0:.3f})}},\n".format(npars * 0.12 / 32))
            f.write("        at={{(0.0,0.9)}},\n")
            f.write("        anchor=west,\n")
            f.write("        cells={anchor=west},\n")
            f.write("        draw=none,\n")
            f.write("%        legend columns=4,\n")
            f.write("%        at={(0,1)},\n")
            f.write("        row sep=0.5pt, % increase spacing a bit\n")
            f.write(
                "%        anchor=south west, % sets the anchor of the legend to the bottom left\n"
            )
            f.write("%        nodes={scale=0.75, transform shape},\n")
            f.write(
                "%        cells={anchor=west},%font=\sffamily}, % left align legend text, change font\n"
            )
            f.write("        },\n")
            f.write("/pgf/bar width=10pt,\n")
            f.write("enlarge y limits={value=0.34,upper},\n")
            f.write("    ytick=data,\n")
            f.write("    axis on top,\n")
            f.write("    tick label style={font=\\footnotesize},\n")
            f.write("    legend style={font=\\footnotesize},\n")
            f.write("    label style={font=\\footnotesize},\n")
            f.write("    width=0.6\\textwidth,\n")
    #        f.write("    height={0:.3f}\\textwidth,\n".format(0.045 * npars))
            f.write("    height=1.939\\textwidth,\n")
            f.write("    xlabel={\\large{\\textsf{expected contribution}}},\n")
            f.write(
                "    symbolic y coords={{{0}}},\n".format(",".join(ytickcoords)).replace(
                    "_", ""
                )
            )
            f.write("    yticklabels={{{0}}},\n".format(yticklabels_str))
            f.write("    xmin=0.0,\n")
            f.write("    xmax=1.0,\n")
            f.write("%    ymax={0},ymin={1}\n".format(ytickcoords[1], ytickcoords[0]))
            f.write("]\n")
        else:
            f.write("    ybar stacked,\n")
            f.write("        yshift=+0.1cm,\n")
            f.write("    yticklabels={,,$0.2$,$0.4$,$0.6$,$0.8$,$1$},")
            f.write("    legend pos=outer north east,\n")
            f.write("    legend style={\n")
            f.write("        legend columns=1,\n")
    #        f.write("        at={{(0.0,-{0:.3f})}},\n".format(npars * 0.12 / 32))
            f.write("        at={{(1.0,1.0)}},\n")
            f.write("%        anchor=west,\n")
            f.write("        cells={anchor=west},\n")
            f.write("        draw=none,\n")
            f.write("%        legend columns=4,\n")
            f.write("%        at={(0,1)},\n")
            f.write("        row sep=0.5pt, % increase spacing a bit\n")
            f.write(
                "%        anchor=south west, % sets the anchor of the legend to the bottom left\n"
            )
            f.write("%        nodes={scale=0.75, transform shape},\n")
            f.write(
                "%        cells={anchor=west},%font=\sffamily}, % left align legend text, change font\n"
            )
            f.write("        },\n")
            f.write("/pgf/bar width=10pt,\n")
            f.write("enlarge x limits={value=0.04,upper},\n")
            f.write("    xtick=data,\n")
            f.write("    axis on top,\n")
            f.write("    tick label style={font=\\footnotesize},\n")
            f.write("    legend style={font=\\Large},\n")
            f.write("    label style={font=\\footnotesize},\n")
            f.write("    height=0.6\\textwidth,\n")
    #        f.write("    height={0:.3f}\\textwidth,\n".format(0.045 * npars))
            f.write("    width={0}\\textwidth,\n".format(0.045 * npars))
            f.write("    ylabel={\\large{\\textsf{expected contribution}}},\n")
            f.write(
                "    symbolic x coords={{{0}}},\n".format(",".join(ytickcoords)).replace(
                    "_", ""
                )
            )
            f.write("    xticklabels={\empty},%"+",{{{0}}},\n".format(xticklabels_str))
            f.write("    ymin=0.0,\n")
            f.write("    ymax=1.0,\n")
            f.write("%    xmax={0},xmin={1}\n".format(ytickcoords[1], ytickcoords[0]))
            f.write("]\n")

        for case, icase in zip(sorted(cont_mat.xpars), range(len(cont_mat.xpars))):
            if not rotate:
                coordstr = " ".join(
                [
                    "({0:.4f},{1})".format(
                        cont_mat.getElem(case, par), par.replace("_", "")
                    )
                    for par in cont_mat.ypars
                ]
            )
            else:
                coordstr = " ".join(
                [
                    "({1},{0:.4f})".format(
                        cont_mat.getElem(case, par), par.replace("_", "")
                    )
                    for par in cont_mat.ypars
                ]
            )

                

            coordstr = "{" + coordstr + "};\n"
            f.write("%{0}\n".format(case))
            icase = str(icase)
            f.write("\\addplot[color"+icase+",fill opacity=0.7,opacity=0.7,preaction={fill=color"+icase+"}] coordinates\n") # pattern={north west lines},pattern color=color"+icase+"
            f.write("{0}\n".format(coordstr))

        for ypar in cont_mat.ypars:
            if "gap" in ypar:
                if rotate:
                    f.write("\\draw[color=gray,thick,dotted] (axis cs:{0},0.0)--(axis cs:{0},1.0);\n".format(ypar))
                else:
                    f.write("\\draw[color=gray,thick,dotted] (axis cs:0.0,{0})--(axis cs:1.0,{0});\n".format(ypar))        
        f.write(
            "\\legend{{{0}}}\n".format(
                ",".join(sorted(cont_mat.xpars)).replace("_", "\_")
            )
        )
        f.write("\\end{axis}\n")
        f.write("\\end{tikzpicture}\n")
        f.write("\\end{document}\n")


# visualising a 2D matrix
def plotmatrix(
    mat,
    vmax=0,
    vmin=0,
    vtextmin=0.0,
    vtextmax=0.0,
    fontsize=15,
    ticklabelsize=20,
    logscale=False,
    cmap="blue_white_yellow",
    hide_zeros=True,
    savepathfile="",
):

    # matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams["text.latex.preamble"] = "\n".join(
        [r"\usepackage{amssymb}", r"\usepackage{amsmath}", r"\usepackage{xcolor}"]
    )
    matarr = mat.matrix
    xticks, yticks = mat.get_parlabels()
    len_arr = np.array(matarr).shape

    kwargs = {}
    if vmax != 0 and vmin != 0:
        kwargs["vmax"], kwargs["vmin"] = vmax, vmin
    if logscale:
        kwargs["norm"] = LogNorm(vmin, vmax)
    # args["cmap"] = get_color_map(cmap)
    fig, ax = plt.subplots(figsize=(16, 16))
    matarr = matarr

    im = ax.imshow(matarr, cmap=get_color_map(cmap), **kwargs)

    if len(xticks) > 0:
        plt.xticks(range(0, len_arr[1]), xticks, fontsize=ticklabelsize)
        plt.xticks(rotation=90)
    if len(yticks) > 0:
        plt.yticks(range(0, len_arr[0]), yticks, fontsize=ticklabelsize)

    im_ratio = len_arr[0] / len_arr[1]
    cbar = plt.colorbar(im, fraction=0.046 * im_ratio, pad=0.04)
    cbar.ax.tick_params(labelsize=ticklabelsize)
    for i in range(len(matarr[0])):
        for j in range(len(matarr)):
            c = matarr[j][i]
            str_format = "{0:.2f}"
            # two values after comma only for small numbers (also option to reduce text printing only in some bins)
            if vtextmin == 0.0 and vtextmax == 0.0:
                vtextmin, vtextmax = -1e-14, 1e14
            if abs(c) >= vtextmin and abs(c) <= vtextmax:
                if get_number_of_digits(c) > 2:
                    str_format = "{0:.0f}"
            if hide_zeros and abs(c) < 1e-2:
                continue
            ax.text(
                i, j, str_format.format(c), va="center", ha="center", fontsize=fontsize
            )

    plt.tick_params(
        axis="x",  # changes apply to the x-axis
        which="both",  # both major and minor ticks are affected
        bottom=False,  # ticks along the bottom edge are off
        top=False,  # ticks along the top edge are off
        labelbottom=True,  # labels along the bottom edge are off
    )
    if len(savepathfile):
        plt.savefig(savepathfile, bbox_inches="tight")
    plt.cla()
    plt.close(fig)
