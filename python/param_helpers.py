import math
import matplotlib
import numpy as np
import ROOT
import yaml
from senstiEFT.helpers import flatten, dict_to_mat
from senstiEFT.matrix import matrix


def rotate_quad_param(rotmat, parmat):
    # it may be more simpler in sympy but let's give it a try with RooFit
    from senstiEFT.param_helpers import rotation_expr, prepare_expr, print_expr
    ws = ROOT.RooWorkspace("ws")
    for ypar in rotmat.ypars:
        ws.factory("{0}[0,-1e6,1e6]".format(ypar))
        ws.var(ypar).setError(0.0001)

    print("Created POIs for rotated basis")
    for xpar in rotmat.xpars:
        ws.factory(rotation_expr(rotmat,xpar))

    for ypar in parmat.ypars:
        ws.factory(prepare_expr(parmat,ypar))

    pars = rotmat.ypars 
    square_terms = [ "{0}*{1}".format(par, par) for par in sorted(rotmat.ypars)]
    cross_terms = [ "{0}*{1}".format(par1, par2) for par1 in sorted(rotmat.ypars) for par2 in sorted(rotmat.ypars[rotmat.ypars.index(par1)+1:])]
    all_terms = rotmat.ypars + square_terms + cross_terms
    new_ypars = [ ypar + "_new" for ypar in parmat.ypars]
    pardict = {obs:{} for obs in new_ypars}

    for poi in new_ypars:
        for term in all_terms:
            # Extract linear and square terms first
            if "*" not in term:
                # interference term = (xsec(1.0) - xsec(0.0))/2
                # interference term = (xsec(1.0) + xsec(0.0))/2 - 1
                par = term
                ws.var(par).setVal(1.0)
                valp1 = ws.obj(poi).getValV()
                ws.var(par).setVal(-1.0)
                valm1 = ws.obj(poi).getValV()
                # reset to 0
                ws.var(par).setVal(0.0)
                int_val = (valp1 - valm1)*0.5
                pardict[poi][par] = int_val     
                sq_val = (valp1 + valm1)*0.5 - 1.0
                pardict[poi]["{0}*{0}".format(term)] = sq_val

        for term in all_terms:
            # Extract cross terms 
            if "*" in term:
                par1, par2 = term.split("*")
                if par1 != par2:
                    #print("corss term: {0}".format(term))
                    # cross term = (xsec(1,1) - 1 - linear1 - linear2 - quad1 - quad2)/2
                    ws.var(par1).setVal(1.0)
                    ws.var(par2).setVal(1.0)
                    val_inc_cross = ws.obj(poi).getValV()
                    val_cross = val_inc_cross - 1.0 - pardict[poi][par1] - pardict[poi][par2] - pardict[poi]["{0}*{0}".format(par1)] - pardict[poi]["{0}*{0}".format(par2)]
                    ws.var(par1).setVal(0.0)
                    ws.var(par2).setVal(0.0)
                    pardict[poi][term] = val_cross
    
    labels = {**parmat.labels, **rotmat.labels}
    rotated_parmat = dict_to_mat(pardict,"rotated parmat")

    rotated_parmat.labels = labels
    new_ypars = [ypar.replace("_new", "") for ypar in rotated_parmat.ypars]
    rotated_parmat.ypars = new_ypars
    return rotated_parmat

def rotation_expr(mat, xpar):
    expr = ""
    for ypar,i in zip(mat.ypars,range(len(mat.ypars))):
        val = mat.getElem(xpar,ypar)
        absval = abs(val)
        if val >= 0.0 : sign = " + "
        else: sign = " - "
        expr = expr + " {0} {1}*@{2}".format(sign,absval,i)

    parlist = ",".join(mat.ypars)
    return "expr::{0}('{1}',{2})".format(xpar, expr, parlist)

def prepare_expr(mat, ypar_fin, is_rotation=False, threshold=1e-6, exclude_cross_terms=False):
    pars = [par for par in mat.xpars if "*" not in par and par != "SM"]
    str0 = ""
    pars_in_form = []
    i = 0
    for par1 in pars:
        val = mat.getElem(par1, ypar_fin)#/mat.getElem("SM",ypar)
        if abs(val) >= threshold:
            if val > 0.0: sign = " + "
            else: sign = " - "
            str0 = " {0} {1:.5f}*@{2} ".format(sign,abs(val),i) + str0  
            if par1 not in pars_in_form:
                pars_in_form.append(par1)
                i = i+1
    
    for par1 in pars:
        for par2 in pars[pars.index(par1):]:    
            if exclude_cross_terms and par1 != par2: continue
            term = "{0}*{1}".format(par1,par2)
            if term not in mat.xpars: continue
            val = mat.getElem(term, ypar_fin)#/mat.getElem("SM",ypar)
            if abs(val) >= 1e-5:
                if par2 not in pars_in_form:
                    pars_in_form.append(par2)
                    i = i+1
                if par1 not in pars_in_form:
                    pars_in_form.append(par1)
                    i = i+1
                if val > 0.0: sign = " + "
                else: sign = " - "
                str0 = " {0} {1:.5f}*@{2}*@{3}".format(sign,abs(val),pars_in_form.index(par1),pars_in_form.index(par2)) + str0

    if "SM" in mat.xpars: smval = mat.getElem("SM",ypar_fin)
    else: smval = 1.0 
    if is_rotation:
        smval = ""
    #print("Prepared expression of {0}".format(ypar_fin))
    return "expr::{0}_new('{3} {1}',{2})".format(ypar_fin,str0,",".join(pars_in_form),smval)


def print_expr(parmat, ypar, print_threshold=1e-4):
    expr = "1.0  "
    for xpar, i in zip(parmat.xpars, range(len(parmat.xpars))):
        val = parmat.getElem(xpar, ypar)
        absval = abs(val)
        if abs(val) <= print_threshold:
            continue
        if val >= 0.0:
            sign = " + "
        else:
            sign = " - "
        expr = expr + " {0} {1:.4f}*{2}".format(sign, absval, xpar)

    print("{0} --> {1}".format(ypar, expr))


def printpar(parmat):
    for poi in parmat.ypars:
        string, sign = ""," + "
        for par in parmat.xpars :
            val = parmat.getElem(par,poi)
            if val < 0: sign = " - "
            if val == 0.0: continue
            string = string + sign + "{0:.3f}".format(abs(val)) + " " + par
        print(poi.ljust(50-11), ":", string)


def load_EFT_param(json_file_loc, verbose=False, threshold=0.001):

    f = open(
        json_file_loc,
    )
    # read the json file
    eft_param = yaml.safe_load(f)
    f.close()
    eftpars = sorted(
        list(set(flatten([param.keys() for bin0, param in eft_param.items()])))
    )

    sm_norm = True
    if "SM" in eftpars:
        sm_norm = False

    # set the param. threshold
    for bin0, param in eft_param.items():
        for par in eftpars:
            val = param[par]
            if not sm_norm:
                val = param[par] / param["SM"]

            # pad with parameters that don't appear
            if par not in param.keys():
                eft_param[bin0][par] = 0.0
            # set param. below threshold to 0.0
            if abs(val) < threshold:
                eft_param[bin0][par] = 0.0
            # round of to reduce digits
            else:
                eft_param[bin0][par] = round(param[par], 4)

    if verbose:
        print("Bins : ")
        for bin0 in eft_param:
            print(
                "{0} is affected by {1}".format(
                    bin0.ljust(42),
                    ", ".join(
                        [par for par, val in eft_param[bin0].items() if abs(val) > 0.01]
                    ),
                )
            )

        printpar(eft_param)

    print(
        "Loaded EFT parameterisation for {0} bins, impact of {1} operators available".format(
            len(eft_param), len(eftpars)
        )
    )

    return eft_param, eftpars


def printpar_dict(poi_par_dict):
    for poi in sorted(poi_par_dict.keys()):
        string, sign = "", " + "
        for par in sorted(poi_par_dict[poi]):
            if poi_par_dict[poi][par] < 0:
                sign = " - "
            if poi_par_dict[poi][par] == 0.0:
                continue
            string = (
                string
                + sign
                + "{0:.3f}".format(abs(poi_par_dict[poi][par]))
                + " "
                + par
            )
        print(poi.ljust(50 - 11), ":", string)


def get_ordered_pois(pois, suffix=None, prefix=None, exclude="None"):
    pois = sorted([par for par in pois if exclude not in par], key=last_letter)
    dec_tags = list(set([poi.split("_")[-1] for poi in pois]))
    ordered_pois = flatten(
        [sorted([poi for poi in pois if tag in poi]) for tag in dec_tags]
    )

    if suffix:
        ordered_pois = ["".join([suffix, poi]) for poi in ordered_pois]
    if prefix:
        ordered_pois = ["".join([poi, prefix]) for poi in ordered_pois]

    return ordered_pois


def make_parammat(name, eftparam, pois, eftpars):
    paramarr = [[eftparam[poi][par] for par in eftpars] for poi in pois]
    parammat = matrix(name, paramarr, xpars=pois, ypars=eftpars)

    return parammat


# subtracting two central val, error tuple
def tuplesub(a, b):
    import math

    if len(a) == 0 and len(b) > 0:
        return b
    elif len(b) == 0 and len(a) > 0:
        return a
    return (a[0] - b[0], math.sqrt(a[1] * a[1] + b[1] * b[1]))


# adding two central val, error tuples
def tupleadd(a, b):
    import math

    if len(a) == 0 and len(b) > 0:
        return b
    elif len(b) == 0 and len(a) > 0:
        return a
    c = (a[0] + b[0], math.sqrt(a[1] * a[1] + b[1] * b[1]))
    return c


# dividing two central val, error tuples
def tupledivide(a, b):
    import math

    if b[0] == 0:
        return b
    return (
        a[0] / b[0],
        math.sqrt(
            (a[1] * a[1]) / (b[0] * b[0])
            + (b[1] * b[1] * a[0] * a[0]) / (b[0] * b[0] * b[0] * b[0])
        ),
    )


# adding the predictions for two production modes
def addpars(vals1, vals2):
    samples = []
    for input0 in [vals1.keys(), vals2.keys()]:
        for sample in input0:
            if sample not in samples:
                samples.append(sample)
    vals = {}
    for sample in samples:
        vals[sample] = {}
        par_keys = vals1.keys()
        for stxsbin in vals1[par_keys[0]].keys():
            if sample in vals1.keys() and sample in vals2.keys():
                vals[sample][stxsbin] = tupleadd(
                    vals1[sample][stxsbin], vals2[sample][stxsbin]
                )
            elif sample in vals1.keys():
                vals[sample][stxsbin] = vals1[sample][stxsbin]
            elif sample in vals2.keys():
                vals[sample][stxsbin] = vals2[sample][stxsbin]
    return vals


# pad the parameterisation with zeros if the stxs doesnt depend on the wilson coeff.
def paddedpar(poipar, allpars):
    import copy

    poipar_padded = copy.deepcopy(poipar)
    for poi in sorted(poipar_padded.keys()):
        for par in allpars:
            if par not in poipar_padded[poi].keys():
                poipar_padded[poi][par] = 0.0
    return poipar_padded


# loop over the parameterisation and collection the parameterisation
# matrix and return the matrix as (numpy array, tmatrix)
# parameterisation matrix - npois x nparameters
# npois and nparameters ordered using sorted
def par2mat(poipar):
    nparmat = []
    pois = poipar.keys()
    tparmat = ROOT.TMatrixD(len(poipar), len(poipar[pois[0]]))
    ipoi = 0
    for poi in sorted(poipar.keys(), key=last_letter):
        ipar, vals = 0, []
        for par in sorted(allpars):
            if par not in [
                "cHWBtil",
                "cGtil",
                "SM",
                "cHWtil",
                "cHGtil",
                "cHBtil",
                "cH",
            ]:
                vals.append(poipar[poi][par][0])
                tparmat[ipoi][ipar] = poipar[poi][par][0]
                ipar = ipar + 1
        ipoi = ipoi + 1
        nparmat.append(vals)
    return nparmat, tparmat


def getinfomat(fitres, pars):
    import numpy as np

    npars = len(pars)
    covmat = ROOT.TMatrixD(npars, npars)
    covarr = []

    fullcovmat = fitres.covarianceMatrix()

    i, parpos = 0, {}
    for par in fitres.floatParsFinal():
        if isPoi(par):
            parpos[par.GetName()] = i
        i = i + 1

    i = 0
    for poi1 in sorted(parpos.keys()):
        j, arr = 0, []
        for poi2 in sorted(parpos.keys()):
            arr.append(fullcovmat(parpos[poi1], parpos[poi2]))
            covmat[i][j] = fullcovmat(parpos[poi1], parpos[poi2])
            j = j + 1
        covarr.append(arr)
        i = i + 1
    infoarr = np.linalg.inv(covarr)
    infomat = covmat.Invert()

    return infoarr, infomat


# return fitresult from ROOT file
def getfitres(filename, fitresname="fitresult_minimizer_asimovData_1"):
    fres = ROOT.TFile.Open(filename)
    fitres = fres.Get(fitresname)
    return fitres


# given a TMatrix return an array
def tmat2arr(inmat):
    arr = []
    for icol in range(0, inmat.GetNcols()):
        tmp = []
        for irow in range(0, inmat.GetNrows()):
            tmp.append(inmat[irow][icol])
        arr.append(tmp)
    return arr


def getdiagmat(inarr):
    dim = len(inarr)
    diagmat = []
    for i1 in range(0, dim):
        tmparr = []
        for i2 in range(0, dim):
            if i1 == i2:
                tmparr.append(inarr[i1])
            else:
                tmparr.append(0.0)
        diagmat.append(tmparr)
    return diagmat


def getpaddmat(arrdict):
    paddmat = []
    for par in sorted(allpars):
        tmparr = []
        zerocount = 0
        for iev in range(0, len(allpars)):
            key = str(iev + 1)
            if par not in arrdict[key].keys():
                zerocount = zerocount + 1
                arrdict[key][par] = 0.0
            tmparr.append(arrdict[key][par])
        paddmat.append(tmparr)
    return paddmat


def last_letter(word):
    return word[::-2]


def print_evs(transdict):
    print(" # par , # 1/sqrt(EV)")
    print("[")
    for par in sorted(transdict.keys()):
        if transdict[par]["eigenval"] != 0.0:
            print(
                '"'
                + par
                + '", # {0:.3f}'.format(1 / math.sqrt(abs(transdict[par]["eigenval"])))
            )
    print("]")


def norm(mat, rows=True, cols=False):
    import numpy as np

    if cols:
        mat = np.transpose(mat)
    norm_mat = [[val / np.amax([abs(x) for x in row]) for val in row] for row in mat]
    if cols:
        norm_mat = np.transpose(norm_mat)
    return norm_mat


def geterr(val):
    print(1 / math.sqrt(val))


def getmat(pattofitres, fitresname, pois):
    fitres = ROOT.TFile.Open(pattofitres).Get(fitresname)
    poi_list = ROOT.RooArgList()
    allpars = fitres.floatParsFinal()
    for poi in pois:
        poi_list.add(allpars.find(poi))

    mat = fitres.reducedCovarianceMatrix(poi_list)

    matarr = np.array(
        [[mat[i][j] for i in range(mat.GetNcols())] for j in range(mat.GetNrows())]
    )

    return matarr


def check_symmetric(a, rtol=1e-05, atol=1e-08):
    return np.allclose(a, a.T, rtol=rtol, atol=atol)
