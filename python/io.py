import ROOT
from senstiEFT.helpers import getarr
from senstiEFT.matrix import matrix


def make_covmat_from_fitresult(name, path_to_fitres, fitres_name, pois):
    fitres = ROOT.TFile.Open(path_to_fitres).Get(fitres_name)
    poi_list = ROOT.RooArgList()
    allpars = fitres.floatParsFinal()
    for poi in pois:
        poi_list.add(allpars.find(poi))

    cov = fitres.reducedCovarianceMatrix(poi_list)
    covarr = np.array(
        [[cov[i][j] for i in range(cov.GetNcols())] for j in range(cov.GetNrows())]
    )
    covmat = matrix(name, covarr, xpars=pois, ypars=pois)

    print(
        "Asked for covariance of {0} bins, found {1} pois in the fit result".format(
            len(pois), len(covarr)
        )
    )

    return covmat


def make_covmat_from_mvg(name, path_to_ws, ws_name, pdf_name, set_name):
    file0 = ROOT.TFile.Open(path_to_ws)
    x = ROOT.RooMultiVarGaussian()
    ws = file0.Get(ws_name)
    x = ws.pdf(pdf_name)
    pois = [par.GetName() for par in ws.set(set_name)]
    covmat = x.covarianceMatrix()
    covarr = getarr(covmat)
    covmat = matrix(name, covarr, xpars=pois)

    return covmat
