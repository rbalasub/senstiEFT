from senstiEFT.matrix import matrix
import numpy as np
import json


def print_stddev(covmat):
    for x, i in zip(covmat.xpars, range(len(covmat.xpars))):
        print("{0} : {1:.4f}".format(x, math.sqrt(covmat.matrix[i][i])))


def make_combined_mat(list_of_mats):
    xpars, ypars = [], []
    # prepare the axes of the matrix
    for mat in list_of_mats:
        for xpar in mat.xpars:
            if xpar not in xpars:
                xpars.append(xpar)
        for ypar in mat.ypars:
            if ypar not in ypars:
                ypars.append(ypar)
    # create empty matrix
    matarr = np.zeros((len(ypars), len(xpars)))
    name = "combined_{0}".format("_".join([mat.name for mat in list_of_mats]))
    combined_mat = matrix(name, matarr, xpars=xpars, ypars=ypars)

    # collect the matrix
    for mat in list_of_mats:
        for xpar in xpars:
            for ypar in ypars:
                if xpar in mat.xpars and ypar in mat.ypars:
                    combined_mat.setElem(xpar, ypar, mat.getElem(xpar, ypar))

    return combined_mat


def merge_pars(pars1, pars2):
    merge_pars = pars1 + [par for par in pars2 if par not in pars1]
    return merge_pars


def merge_matrix_pars(matrix1, matrix2):
    merge_xpars = merge_pars(matrix1.xpars, matrix2.xpars)
    merge_ypars = merge_pars(matrix1.ypars, matrix2.ypars)
    return merge_xpars, merge_ypars


def get_padded_matrix(in_matrix, pad_name="", pad_xpars=[], pad_ypars=[], pad_val=0.0):
    if not len(pad_name):
        pad_name = "_".join(["padded", str(pad_val), in_matrix.name])
    padded_xpars = merge_pars(in_matrix.xpars, pad_xpars)
    padded_ypars = merge_pars(in_matrix.ypars, pad_ypars)
    padded_arr = []

    for ypar in padded_ypars:
        tmp_arr = []
        if ypar not in in_matrix.ypars:
            tmp_arr = [pad_val for xpar in padded_xpars]
        else:
            for xpar in padded_xpars:
                if xpar in in_matrix.xpars:
                    tmp_arr.append(in_matrix.getElem(xpar, ypar))
                else:
                    tmp_arr.append(pad_val)
        padded_arr.append(tmp_arr)

    padded_matrix = matrix(pad_name, padded_arr, xpars=padded_xpars, ypars=padded_ypars)
    return padded_matrix


def norm(mat, rows=True, cols=False):
    if cols:
        mat = np.transpose(mat)
    norm_mat = [[val / np.amax([abs(x) for x in row]) for val in row] for row in mat]
    if cols:
        norm_mat = np.transpose(norm_mat)
    return norm_mat


def add_matrices(matrix1, matrix2, sum_name=""):
    if not len(sum_name):
        sum_name = "_".join(["sum", matrix1.name, matrix2.name])
    sum_xpars, sum_ypars = merge_matrix_pars(matrix1, matrix2)

    pad_matrix1 = get_padded_matrix(matrix1, pad_xpars=sum_xpars, pad_ypars=sum_ypars)
    pad_matrix2 = get_padded_matrix(matrix2, pad_xpars=sum_xpars, pad_ypars=sum_ypars)

    sum_arr = [
        [
            float(pad_matrix1.getElem(xpar, ypar))
            + float(pad_matrix2.getElem(xpar, ypar))
            for xpar in sum_xpars
        ]
        for ypar in sum_ypars
    ]
    sum_matrix = matrix(sum_name, sum_arr, xpars=sum_xpars, ypars=sum_ypars)
    return sum_matrix


def add_list_of_matrices(matlist):
    if len(matlist) == 1:
        addmat = matlist[0]

    else:
        addmat = add_matrices(matlist[0], matlist[1])

    if len(matlist) > 2:
        for mat in matlist[2:]:
            addmat = add_matrices(addmat, mat)

    return addmat


def get_const_pars(mat, val=0.0, xpars=True, ypars=True):
    zero_pars = []
    if xpars:
        zeros = np.zeros(len(mat.xpars))
        transpose_mat = np.transpose(np.copy(mat.matrix))
        for mat_xpar, par in zip(transpose_mat, mat.xpars):
            if np.array_equal(zeros, mat_xpar):
                zero_pars.append(par)

    if ypars:
        zeros = np.zeros(len(mat.ypars))
        for mat_ypar, par in zip(mat.matrix, mat.ypars):
            if np.array_equal(zeros, mat_ypar):
                zero_pars.append(par)

    return zero_pars
