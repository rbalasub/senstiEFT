import numpy as np
import json, yaml, sys
from numpy import append
from collections import OrderedDict


def dict_to_mat(name,indict,xpars=[],ypars=[]):
    """Convert dictionary map to matrix"""

    if len(xpars) == 0:
        for key, val in indict.items():
            xpars = xpars + list(val.keys())
        xpars = list(set(xpars))

    if len(ypars) == 0: ypars = list(indict.keys())

    mat_arr = []
    for ypar in ypars:
        tmp_arr = []
        for xpar in xpars:
            if xpar not in indict[ypar].keys():
                tmp_arr.append(0.0)
            else:
                if isinstance(indict[ypar][xpar], dict):
                   tmp_arr.append(indict[ypar][xpar]["val"])
                if isinstance(indict[ypar][xpar], float):
                   tmp_arr.append(indict[ypar][xpar])
                if isinstance(indict[ypar][xpar], int):
                   tmp_arr.append(indict[ypar][xpar])

        mat_arr.append(tmp_arr)

    mat = matrix(name, mat_arr, xpars, ypars)
    return mat


def setup_yaml():
    """https://stackoverflow.com/a/8661021"""

    represent_dict_order = lambda self, data: self.represent_mapping(
        "tag:yaml.org,2002:map", data.items()
    )
    yaml.add_representer(OrderedDict, represent_dict_order)


setup_yaml()


def listdiff(list0, list1):
    """different of two lists"""

    return list(set(list0).difference(set(list1)))


def is_square_matrix(nparr):
    """check if numpy array is square"""
    return nparr.shape[0] == nparr.shape[1]


def transform_matrix_basis(mat, tfn_mat):
    mat.matrix = transformbasis(mat.matrix, tfn_mat.matrix)
    mat.xpars, mat.ypars = tfn_mat.ypars, tfn_mat.ypars


def load_matrix(infilename, xpars=[], ypars=[]):
    with open(infilename) as f:
        if infilename.endswith(".json"):
            data = json.load(f)
        elif infilename.endswith(".yaml") or infilename.endswith(".yml"):
            data = yaml.safe_load(f)
        else:
            print(
                "Provided input {0} not json or yaml, not compatible".format(infilename)
            )
            sys.exit()

    keys = set(["name", "xpars", "ypars", "matrix"])
    if keys.intersection(data.keys()) != keys:
        missing = [key for key in keys if key not in data.keys()]
        print(
            "cannot open matrix from {0}, does not contain the keys {1}".format(
                infilename, missing
            )
        )

    else:
        # xpars, ypars
        if len(data["xpars"]) == 0:
            data["xpars"] = data["ypars"]
        if len(data["ypars"]) == 0:
            data["ypars"] = data["xpars"]
        labels = {}
        if "labels" in data.keys():
            labels = data["labels"]
        mat = matrix(data["name"], data["matrix"], data["xpars"], data["ypars"], labels)
        if "eigen values" in data.keys():
            mat.eigenvalues = data["eigen values"]            
        if "uncertainities" in data.keys():
            mat.uncertainties = data["uncertainities"]  
        orderpars = {}
        if "order" in data.keys():
            for par in ["xpars", "ypars"]:
                if par in data["order"].keys():
                    orderpars[par] = data["order"][par]
                else:
                    orderpars[par] = data[par]
            if len(orderpars):
                order_arr = [
                    [mat.getElem(x, y) for x in orderpars["xpars"]]
                    for y in orderpars["ypars"]
                ]
                mat_ordered = matrix(
                    data["name"],
                    order_arr,
                    xpars=orderpars["xpars"],
                    ypars=orderpars["ypars"],
                )
                mat_ordered.labels = mat.labels
                mat_ordered.eigenvalues = mat.eigenvalues
                mat_ordered.uncertainties = mat.uncertainties
                return mat_ordered
        return mat

def load_input(infilename):
    with open(infilename) as f:
        if infilename.endswith(".json"):
            data = json.load(f)
        elif infilename.endswith(".yaml") or infilename.endswith(".yml"):            
            data = yaml.safe_load(f)
        else:
            print(
                "Provided input {0} not json or yaml, not compatible".format(infilename)
            )
            sys.exit()
    return data

def load_map(infilename, xpars=[], ypars=[]):

    data = load_input(infilename)
    keys = set(["name", "xpars", "ypars", "map"])
    if keys.intersection(data.keys()) != keys:
        missing = [key for key in keys if key not in data.keys()]
        print(
            "cannot open matrix from {0}, does not contain the keys {1}".format(
                infilename, missing
            )
        )

    else:
        # xpars, ypars
        if len(data["xpars"]) == 0:
            data["xpars"] = data["ypars"]
        if len(data["ypars"]) == 0:
            data["ypars"] = data["xpars"]
        labels = {}
        if "labels" in data.keys():
            labels = data["labels"]
        matarr = dict_to_mat(data["map"],xpars=data["xpars"],ypars=data["ypars"])

        mat = matrix(data["name"], matarr, data["xpars"], data["ypars"], labels)
        orderpars = {}
        if "order" in data.keys():
            for par in ["xpars", "ypars"]:
                if par in data["order"].keys():
                    orderpars[par] = data["order"][par]
                else:
                    orderpars[par] = data[par]
            if len(orderpars):
                order_arr = [
                    [mat.getElem(x, y) for x in orderpars["xpars"]]
                    for y in orderpars["ypars"]
                ]
                mat_ordered = matrix(
                    data["name"],
                    order_arr,
                    xpars=orderpars["xpars"],
                    ypars=orderpars["ypars"],
                )
                mat_ordered.labels = mat.labels

                return mat_ordered

        return mat


def load_parameterisation(infilename, xpars=[], ypars=[]):

    data = load_input(infilename)
    keys = set(["coefficient terms", "observables", "parameterisation"])
    if keys.intersection(data.keys()) != keys:
        missing = [key for key in keys if key not in data.keys()]
        print(
            "cannot open matrix from {0}, does not contain the keys {1}".format(
                infilename, missing
            )
        )
        sys.exit()

    else:
        # xpars, ypars
        labels = {}
        if "labels" in data.keys():
            labels = data["labels"]
        if isinstance(data["parameterisation"], dict) : 
            mat = dict_to_mat(data["name"],data["parameterisation"],xpars=data["coefficient terms"],ypars=data["observables"])
            mat.labels = labels
        elif isinstance(data["parameterisation"], list) : 
            mat = matrix(
                data["name"],
                data["parameterisation"],
                data["coefficient terms"],
                data["observables"],
                labels,
            )
        return mat


def load_covariance(infilename, xpars=[], ypars=[], normalise_sm=True):

    data = load_input(infilename)
    keys = set(["measurement", "names", "covariance", "sm"])
    if keys.intersection(data.keys()) != keys:
        missing = [key for key in keys if key not in data.keys()]
        if missing == ["names"]:
            data["names"] = [
                "{0}_bin{1}".format(data["measurement"], i)
                for i in range(len(data["sm"]))
            ]
        else:
            print(
                "cannot open matrix from {0}, does not contain the keys {1}".format(
                    infilename, missing
                )
            )
            sys.exit()

    labels = {}
    if "labels" in data.keys():
        labels = data["labels"]
    if normalise_sm:
        if 0.0 not in data["sm"]:
            idxpars = range(len(data["sm"]))
            cov_norm = [ [(data["covariance"][par1][par2]) /(sm1 * sm2) for sm1, par1 in zip(data["sm"], idxpars)] for sm2, par2 in zip(data["sm"], idxpars)]
            data["covariance"] = cov_norm
        else:
            idxpars = range(len(data["sm"]))
            cov_norm = [ [(data["covariance"][par1][par2])  for sm1, par1 in zip(data["sm"], idxpars)] for sm2, par2 in zip(data["sm"], idxpars)]
            data["covariance"] = cov_norm
            
    mat = matrix(
        data["measurement"], data["covariance"], data["names"], data["names"], labels
    )
    return mat


def load_central_values(
    infilename, xpars=[], ypars=[], normalise_sm=True, sm_expected=False
):
    data = load_input(infilename)

    keys = set(["measurement", "names", "covariance", "sm"])
    if keys.intersection(data.keys()) != keys:
        missing = [key for key in keys if key not in data.keys()]
        if missing == ["names"]:
            data["names"] = [
                "{0}_bin{1}".format(data["measurement"], i)
                for i in range(len(data["sm"]))
            ]
        else:
            print(
                "cannot open matrix from {0}, does not contain the keys {1}".format(
                    infilename, missing
                )
            )
            sys.exit()

    labels = {}
    if "labels" in data.keys():
        labels = data["labels"]
    if sm_expected:
        data["measured"] = data["sm"]

    if normalise_sm:
        if 0.0 not in data["sm"]:
            idxpars = range(len(data["sm"]))
            cenval_norm = [[meas / sm] for meas, sm in zip(data["measured"], data["sm"])]
            data["measured"] = cenval_norm
        else:
            idxpars = range(len(data["sm"]))
            cenval_norm = [[meas] for meas, sm in zip(data["measured"], data["sm"])]
            data["measured"] = cenval_norm
            
    mat = matrix(
        data["measurement"],
        data["measured"],
        xpars=["measured"],
        ypars=data["names"],
        labels=labels,
    )
    return mat


class matrix:
    def __init__(self, name, matrix, xpars=[], ypars=[], labels={}):
        self.name = name
        self.matrix = np.array(matrix)
        self.xpars = xpars
        if not len(ypars):
            ypars = xpars
        self.ypars = ypars
        self.labels = labels
        self.eigenvalues, self.eigenvectors = {},{}
        self.uncertainties = {}
        self.reduced_matrix = []
        self.shape = self.matrix.shape
        self.pars = self.xpars + self.ypars

    def diagonalise(self):
        mat = self.matrix
        if not is_square_matrix(mat):
            print(" matrix {0} not square ".format(tfn_matrix.name))

        self.egvals, self.egvecs = np.linalg.eig(mat)
        self.diag_matrix = np.diag(self.egvals).real
        self.egvecs = self.egvecs

    def getElem(self, xpar, ypar):
        return self.matrix[self.ypars.index(ypar)][self.xpars.index(xpar)]

    def getDict(self):
        return {
            ypar: {xpar: self.getElem(xpar, ypar) for xpar in self.xpars}
            for ypar in self.ypars
        }

    def get_parlabels(self):
        xticks = [
            self.labels[x] if x in self.labels.keys() else x.replace("_", "\_")
            for x in self.xpars
        ]
        yticks = [
            self.labels[y] if y in self.labels.keys() else y.replace("_", "\_")
            for y in self.ypars
        ]
        return xticks, yticks

    def setElem(self, xpar, ypar, val):
        self.matrix[self.ypars.index(ypar)][self.xpars.index(xpar)] = val

    def reorder_matrix(self, xpars, ypars):
        if set(xpars) == set(self.xpars) and set(ypars) == set(self.ypars):
            new_matrix = [
                [self.getElem(xpar, ypar) for xpar in xpars] for ypar in ypars
            ]
            self.xpars, self.ypars = xpars, ypars
            self.matrix = np.array(new_matrix)
        else:
            print(
                "Not transforming, different parameter provided !\nxpars : {0}\nypars : {1}".format(
                    ",".join(listdiff(self.xpars, xpars)),
                    ",".join(listdiff(self.ypars, ypars)),
                )
            )

    def reduce_matrix(self, xpars, ypars):
        if set(xpars).issubset(set(self.xpars)) and set(ypars).issubset(
            set(self.ypars)
        ):
            red_matrix = np.array(
                [[self.getElem(xpar, ypar) for xpar in xpars] for ypar in ypars]
            )
            self.matrix = red_matrix
            self.xpars, self.ypars = xpars, ypars

        else:
            print(
                "Not transforming, different parameter provided !\nxpars : {0}\nypars : {1}".format(
                    ",".join(listdiff(self.xpars, xpars)),
                    ",".join(listdiff(self.ypars, ypars)),
                )
            )

    def printMeta(self):
        print(
            "matrix {0} ({3}x{4}) with directions \n x: {1} \n y: {2}".format(
                self.name,
                ", ".join(self.xpars),
                ", ".join(self.ypars),
                len(self.xpars),
                len(self.ypars),
            )
        )

    def transform_basis(self, tfn_matrix):
        if not is_square_matrix(tfn_matrix.matrix):
            print(
                "warning: transformation matrix {0} not square ".format(tfn_matrix.name)
            )

        # rotate the matrix and also update the basis
        transform_matrix_basis(self, tfn_matrix)

    def print_dimensions(self):
        print("printing the size of the matrix {0}".format(self.name))
        print(
            "matrix is of size : {0} x {1} ".format(
                len(self.matrix), len(self.matrix[0])
            )
        )
        print(
            "axis is of size : ypars({0}), xpars({1})".format(
                len(self.ypars), len(self.xpars)
            )
        )

    def print_dimensions(self):
        print("printing the size of the matrix {0}".format(self.name))
        print(
            "matrix is of size : {0} x {1} ".format(
                len(self.matrix), len(self.matrix[0])
            )
        )
        print(
            "axis is of size : ypars({0}), xpars({1})".format(
                len(self.ypars), len(self.xpars)
            )
        )

    def transpose(self):
        xpars_tmp = self.xpars.copy()
        self.xpars = self.ypars.copy()
        self.ypars = xpars_tmp
        self.matrix = self.matrix.transpose()

#    def shrink_mat(self, threshold=10.0):
#        self.diagonalise()
#        reduced_egvals = [egval for egval in self.egvals if egval >= threshold]
#        red_diag_egvals = np.diag(reduced_egvals).real
#        n_red_pars = len(reduced_egvals)
#        red_egvecs = [
#            [self.egvecs[i][j] for i in range(n_red_pars)] for j in range(n_red_pars)
#        ]
#        if np.linalg.det(red_egvecs) > 0:
#            self.reduced_matrix = np.transpose(
#                np.absolute((np.linalg.inv(red_egvecs)@red_diag_egvals)@red_egvecs)
#            )
#        else:
#            print(
#                "determinant of reduced transformation matrix is {0}, keeping original matrix !".format(
#                    np.linalg.det(red_egvecs)
#                )
#            )
#            self.reduced_matrix = self.matrix

    def save_matrix(self, outfilename, additional_info={}):
        out = OrderedDict(
            [
                ("name", str(self.name)),
                ("xpars", [str(x) for x in self.xpars]),
                ("ypars", [str(y) for y in self.ypars]),
                ("matrix", self.matrix.tolist()),
                ("labels", self.labels),
            ]
        )
        for tag, info in additional_info.items():
            out[tag] = info
        with open(outfilename, "w") as f:
            if outfilename.endswith(".json"):
                json.dump(out, f, indent=2)
            elif outfilename.endswith(".yaml"):
                yaml.dump(
                    out,
                    f,
                    allow_unicode=True,
                    default_flow_style=None,
                    encoding="utf-8",
                )
            else:
                print("Please provide yaml or json as output")
                sys.exit()

        print("Written matrix {0} to {1}".format(self.name, outfilename))

    def save_parameterisation(self, outfilename, additional_info={}, threshold=1e-10):
        matdict = {
            ypar: {
                xpar: float(self.getElem(xpar, ypar))
                for xpar in self.xpars
                if abs(self.getElem(xpar, ypar)) > threshold
            }
            for ypar in self.ypars
        }
        print("Saving map and applying a threshold of {0}".format(threshold))
        xpars = []
        for ypar, param in matdict.items():
            xpars = xpars + list(param.keys())
        xpars = list(set(xpars))
        out = OrderedDict(
            [
                ("name", self.name),
                ("parameterisation", matdict),
                ("coefficient terms", xpars),
                ("observables", self.ypars),
                #            ("labels",self.labels),
            ]
        )
        for tag, info in additional_info.items():
            out[tag] = info
        with open(outfilename, "w") as f:
            if outfilename.endswith(".json"):
                json.dump(out, f, indent=2)
            elif outfilename.endswith(".yaml"):
                yaml.dump(out, f, allow_unicode=False, default_flow_style=False)
            else:
                print("Please provide yaml or json as output")
                sys.exit()

        print("Written map {0} to {1}".format(self.name, outfilename))
