[[_TOC_]]

## Introduction 

senstiEFT a package to understand the interplay of the EFT effects between measurements based on estimates  
for the sensitivities from the measurement and the corresponding EFT dependence. This package includes ideas  
and scripts developed with Carsten Burgard, Saskia Falke, and  Wouter Verkerke. There are two key ingredients  
that would be required to get things started for the EFT related studies,

- EFT parameterisation for relevant distribution
- Measurement sensitivity corresponding to the distribution

Once these are derived, it is can be serialised in the json format. The files then serve as inputs to the scripts  
provided in the package. If you have any suggestions do not hesitate to get in touch at [andrea.visibile@cern.ch](andrea.visibile@cern.ch) [rabalasu@cern.ch](rabalasu@cern.ch).

## Setup

For setting up the make sure a decent ROOT version and python is setup, this can be done with the following commands,
```
setupATLAS;
ulimit -s 137028;
lsetup "views LCG_101 x86_64-centos7-gcc8-opt"
```
 
To setup the package do the following,
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/rbalasub/senstiEFT.git
mkdir build && cd build && cmake ../senstiEFT && make -j4 && cd ../
source build/setup.sh
```

For all the examples below you can give them a try from the package folder so 
don't forget to change directory before trying it out,
```
cd senstiEFT
```

## sensitivity study

A scheme of how a sensitivity study is done:

![ALT](data/img/sensitivity_study_scheme.jpeg "Sensitivity study scheme")

Note on inputs:
measurements and parameterisation must be in yaml format, containing the following data with the appropriate keys:
measurements: name, names, covariance, measurement, sm, (labels)
parameterisation: name, coefficient terms, observables, parameterisation

#### 1. Preparing Fisher information

The sensitivity study is based on the Fisher information matrix in the EFT space derived from the information matrix in the measurement space. To prepare the information matrix starting from the expected measurement covariances and parameterisation, the following command can be used:
```
python scripts/prepare_fisher_info.py \
       --input Higgs data/parameterisation/stxs_param.yaml data/fit_results/Higgs_STXSxBR_mu_exp.yaml \
       --output data/information_matrix/info_Higgs_STXSxBR.yaml
```
The information matrix (I) is created as $I=P^{T}C^{-1}P$, with P the parameterisation matrix and C the covariance matrix. To add further inputs when under the approximating of no correlation between measurement you can simply provide additional input flags ```--input meas meas_param.yaml meas_data.yaml```.

Note that the you need to provide the fit result of the measurement which contains the expected covariance and the parameterisation. In case you have a RooFitResult of the measurement you can prepare the corresponding yaml file using,

```
python scripts/extract_result_from_fitresult.py -i fitresult.root --poi-expr ^mu_ \
       --outfile outfile.yaml --measurement-name my_meas \
       --fitresult-name fitresult_name
```

#### 2. Finding sensitive directions

To find the most sensitive directions within a group of SMEFT parameters, a principle component analysis is used, for instance,
```
python scripts/get_sensitive_directions.py --input data/information_matrix/info_Higgs_STXSxBR.yaml \
--group name=c_top cG cuu1 cqq3 cqq1 cqq11 cqq31 cqu1 cqu8 cuu cud1 cud8 cqd1 cqd8 \
--group name=c_ggH cHG cuGRe cuHRe --group cll1 cHl3 --group cHq3 --group cHq1 cHd cHu --group cHl1 cHe \
--group name=c_Hyy cHWB cHW cHB cHDD cuWRe cuBRe cW --group ceHRe --group cHbox --group cdHRe \
--group-name combSTXS --output data/transformation_matrix/transmat_stxs_br_info.yaml

```

Here the information matrix is provide to the `--input` flag and the parameters are grouped accordingly to the provided flags. This means for example that for the case of the group name `c_top` a PCA is performed in the reduced infomat with the relevant parameters only. 

#### 3. Estimate covariance

The sensitivity study can also be used to estimate the uncertainity of the parameters in any basis provide no flat directions are given as input. An example is provide for the case of a particular grouping derived from the LEP and STXS Higgs information.
```
python scripts/estimate_covariance.py --info-matrix data/information_matrix/info_Higgs_STXSxBR.yaml \
--transformation-matrix data/transformation_matrix/transmat_stxs_br_info.yaml \
--pois c_Hyy_01 c_Hyy_02 cll1_cHl3_01 c_top_01 cHq1_cHd_cHu_01 cHl1_cHe_01 cHq3 \
c_ggH_01 c_ggH_02 ceHRe cdHRe cHq1_cHd_cHu_02 c_Hyy_03
```

This script is useful to understand if the chosen grouping is a good choice: you can select here a subset of pois with an expected uncertainty under a certain threshold and save the correlation matrix of this fit basis. 

It is useful to be able to tweak the previous two steps and this feature is provided in the `interactive_sensitivity_study` script. With this the parameter groups can be tweaked interactively and also estimating the uncertainity final set of POIs that will be floated.

#### interactive senstivity study

You can be guided in the sensitivity study by the interactive_sens_study.py script. 

You can run an example with:

```
python scripts/interactive_sens_study.py -p data/parameterisation/param_for_test_interactive.yaml  \
--output-ws test_ws.root \
--meas data/fit_results/HiggsData_obs_for_test_interactive.yaml  --fontsize 5 --no-plot-infomat
```

here you can use the Global EFT parameterisation and fit results to make a sensitivity study. 

Since the infomat is given as input you can answer "n" to the first step (creating an infomat from measurements and param). Then you can use the example line for grouping, in particular this will give you the full EV basis. You can estimate the expected uncertainty on the pois and look at the correlation matrix. Finally you can create contribution plots (WIP) and mvg workspace with Higgs data only.

The script saves also a config that can be used to rerun the code (if you run with --config the script prints the commands used in the run related to the config).

## Workspace based on multi-variate gaussian pdf of measurements

A simple way to approximate the statistical model for the measurements is to use the gaussian approximation where the measurement is modelled as a multi-variate gaussian around the best-fit of the measurement with the covariance matrix of the measurement capturing the appropriate correlations. 

These measurements can then be translated to the wilson co-efficient using the parameterisation and the rotation. An example of this is shown below, 
```
python scripts/prepare_mvg_workspace.py \
--input Higgs data/parameterisation/stxs_param.yaml data/fit_results/Higgs_STXSxBR_mu_exp.yaml \
--rotation data/transformation_matrix/transmat_stxs_br_info.yaml \
--output data/workspaces/rotated_basis_Higgs_STXSxBR_mu_exp.root
```

To prepare the MVG can in terms of the measurements without any reparameterisation, you can simply remove all the inputs related to 
EFT. For instance this will give the MVG in terms of the measurements,
```
python scripts/prepare_mvg_workspace.py --input Higgs data/fit_results/Higgs_STXSxBR_mu_exp.yaml \
--output data/workspaces/Higgs_STXSxBR_mu_exp.root
```


N.B., for some observed multi-variate Gaussian models, there can  numerical issues with multivariate gaussian pdf. When the MVG is centered around the observed measurement values, the pdf value corresponding to the SM point which is used as a 
starting point of the reparameterised fit encouters numerical issues. Adding a small flat PDF centred around the SM alleviates 
this issue. This can be done by using the ```--add-sm-pdf``` flag.

### XML making
The inputs generated as part of this study can be easily translated to xml format required by [workspaceCombiner](https://gitlab.cern.ch/atlas_higgs_combination/software/workspaceCombiner) that is used for workspace manipulations.  
As an example, to generate the xml inputs for making the linear EFT reparameterisation the following command prepares the xml,

```
python scripts/make_xml.py -o test.xml  --parameterisation data/parameterisation/stxs_param.yaml \
--in-file pre_param.root --out-file post_param.root 
```

To exclude any parameters in the the xml making the `--exclude` flag can be used as following, `--exclude poi1 poi2`.
If you want to reparameterise an existing EFT workspace from the Warsaw basis to another basis, please use the 
corresponding rotation matrix with the script,

```
python scripts/make_xml.py -o test.xml  --rotation data/transformation_matrix/transmat_stxs_br_info.yaml \
--in-file warsaw_param_ws.root --out-file rotated_basis_param_ws.root 
```

## Storing relevant information
For most of the steps currently storing them as a matrix in a yaml format seems reasonable, this covers the following,

 - covariance matrix
 - linear parameterisation (can be extended to include quadratic terms)
 - information matrix
 - basis transformations

For other format (ROOT, RooFit objects), helper functions exist or can be added to facilitate translations.


## Measurement contribution to EFT
The `plot_measurement_contribution.py` can be used to identify the contribution of measurements to EFT fit directions. While the script is based on using the information matrices, an option is also provided to write out the intermediate parameterisation matrices which can be used to build xmls for a MVG/likelihood level estimate.

For instance to plot the contribution of different Higgs decay modes to the EFT fit directions use, 
```
python scripts/plot_measurement_contribution.py --config data/configs/contribution_plot_higgs_decay.yaml --output out.tex
```  

## Plotting (Ongoing)

Plotting scripts for EFT related plots are to be added shortly
